import buble from 'rollup-plugin-buble'
import uglify from 'rollup-plugin-uglify'

const common = {
  format: 'iife',
  plugins: [
    buble()
  ]
}

const configs = [
  // lib (for tests)
  Object.assign({
    entry: 'lib/qb.js',
    dest: 'dist/qb.js',
    moduleName: 'QB'
  }, common),

  // dash
  Object.assign({
    entry: 'dash/main.js',
    dest: 'dist/dash.bundle.js'
  }, common),

  // hls
  Object.assign({
    entry: 'hls/main.js',
    dest: 'dist/hls.bundle.js'
  }, common)
]

const all = configs.concat(configs.map(config => {
  const minified = Object.assign({}, config)
  minified.plugins = config.plugins.concat([uglify()])
  minified.dest = config.dest.replace('.js', '.min.js')
  return minified
}))

export default all
