------------------------------
Dependencies (for development)
------------------------------

  * node
  * npm
  * sassc
  * jsdoc

-------
Testing
-------

First of all run ``npm install`` to get all the necessary packages to test

Unit tests
----------

To run on continuous integration server:

``npm run test``

To run locally for tdd, first load a chrome instance:

``npm run tdd_start``

Then run the tests

``npm run tdd``

End-to-end tests
----------------

To run first we need a selenium server running. Use ``selenium-standalone``
npm package to install and run the server.

``npm install -g selenium-standalone``
``selenium-standalone install``
``selenium-standalone start``

After we have the selenium server running we must set that server in
``tests/e2e/config.js``::

  module.exports = {
    ...

    test_settings: {
      default: {
        ...
        selenium_port: [PORT],
        selenium_host: [HOST],
        ...
      }
    }

    ...
  }

We also need to have a play api hosted at the machine
running the tests, or to change ``apiUrl`` at
``tests/e2e/config.js``::

  module.exports = {
    ...

    test_settings: {
      default: {
        ...
        globals: {
          ...
          apiUrl: [Play API URL],
          ...
        }
      }
    }

    ...
  }

The tests first make a request to ``apiUrl`` to get the
client url to test.

Tests are working in Firefox, Google Chrome, Internet Explorer and Microsoft Edge.
For Internet Explorer you have to make some special work: https://stackoverflow.com/a/32100197/20601

Known content errors
----------------
* Serie Mad Men -> 0bb57999-9ec9-4d76-8058-5ab9887a443d_MadMenT01E01 ApiVod return serie_chapters_tree empty. In this case next episode is not showed

* Serie Weeds -> d1281fb1-840e-4034-8503-bd0b70f256ce_WeedsS02E01 haven't creditsBegin time. temporal solution 20s before end show next episode to play

* Serie Robotech -> 0b075bb2-4026-4cca-960e-eb46b396a2b7_RobotechMacrossSagaS01E01 service nextEpisode from ApiVod return the actual chapter.





