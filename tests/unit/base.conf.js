/* globals module, require */
module.exports = function (config) {
  return {
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../..',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'sinon-chai', 'sinon', 'chai'],

    // list of files / patterns to load in the browser
    files: [
      {pattern: 'tests/unit/fakes/*', watched: false, included: false, served: true, nocache: false},
      {pattern: 'lib/**/*.js', included: false},
      'node_modules/chance/chance.js',
      'tests/unit/specs/*.js'
    ],

    // when a browser disconnects, wait 30 seconds so it can reconnect
    browserDisconnectTimeout: 30000,

    // let the browser to be "inactive" for 30 seconds
    browserNoActivityTimeout: 30000,

    client: {
      mocha: {
        timeout: 30000
      }
    },

    hostname: 'localhost',

    // list of files to exclude
    exclude: [
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'lib/**/*.js': ['rollup'],
      'tests/unit/specs/*.js': ['rollup']
    },

    rollupPreprocessor: {
      plugins: [
        require('rollup-plugin-buble')(),
      ],
      format: 'iife',
      moduleName: 'QB',
      sourceMap: 'inline'
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['verbose'],

    verboseReporter: {
      immediateLogs: true
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: 1
  }
}
