import Config from '../../../lib/config'
import Observable from '../../../lib/observable'

describe('Observable', function () {

  before(function () {
    Config.debug = false     // disable logs
  })

  beforeEach(function () {
    this.observable = Observable()
    this.observers = [
      sinon.spy(function () {}),
      sinon.spy(function () {}),
      sinon.spy(function () {})
    ]
  })


  describe('.notify()', function () {

    it('should fail with invalid events', function () {
      var self = this

      ;[undefined, null, {}, [], 0, 123, function () {}].forEach(function (invalid) {
        expect(function () { self.observable.notify(invalid) }).to.not.throw
      })
    })

    it('should not fail without observers', function () {
      var self = this
      expect(function () { self.observable.notify(chance.word()) }).to.not.throw
    })

    it('should notify observers', function () {
      var self = this
        , event = chance.word()

      self.observers.forEach(function (observer) {
        self.observable.on(event, observer)
      })

      self.observable.notify(event)

      self.observers.forEach(function (observer) {
        expect(observer).to.have.been.callCount(1)
      })
    })

    it('should notify observers with extra data', function () {
      var self = this
        , event = chance.word()
        , data = {name: chance.word, value: chance.integer()}

      self.observers.forEach(function (observer) {
        self.observable.on(event, observer)
      })

      self.observable.notify(event, data)

      self.observers.forEach(function (observer) {
        expect(observer).to.have.been.calledWith(data)
      })
    })

  })


  describe('.on()', function () {

    it('should fail with an invalid event', function () {
      var self = this

      ;[undefined, null, {}, [], 0, 123, function () {}].forEach(function (invalid) {
        expect(function () { self.observable.on(invalid) }).to.throw
      })
    })

    it('should fail with an invalid function', function () {
      var self = this

      ;[undefined, null, {}, [], 0, 123, '', chance.word()].forEach(function (invalid) {
        expect(function () { self.observable.on(chance.word(), invalid) }).to.throw
      })
    })

    it('should not fail to add the same event or listener multiple times', function () {
      var self = this
        , event = chance.word()
        , listener = function () {}

      expect(function () { self.observable.on(event, listener) }).to.not.throw
      expect(function () { self.observable.on(event, listener) }).to.not.throw
      expect(function () { self.observable.on(event, listener) }).to.not.throw
    })

    it('should add multiple listeners to the same event', function () {
      var self = this
        , event = chance.word()

      self.observers.forEach(function (observer) {
        self.observable.on(event, observer)
      })

      self.observable.notify(event)

      self.observers.forEach(function (observer) {
        expect(observer).to.have.been.callCount(1)
      })
    })

  })


  describe('.off()', function () {

    it('should fail with an invalid event', function () {
      var self = this

      ;[undefined, null, {}, [], 0, 123, function () {}].forEach(function (invalid) {
        expect(function () { self.observable.off(invalid) }).to.throw
      })
    })

    it('should fail with an invalid function', function () {
      var self = this

      ;[undefined, null, {}, [], 0, 123, '', chance.word()].forEach(function (invalid) {
        expect(function () { self.observable.off(chance.word(), invalid) }).to.throw
      })
    })

    it('should not fail if an unknown event or listener are removed', function () {
      var self = this
        , event = chance.word()
        , listener = function () {}

      expect(function () { self.observable.on(event + 'on', listener) }).to.not.throw
      expect(function () { self.observable.off(event, listener) }).to.not.throw
      expect(function () { self.observable.off(event, function () {}) }).to.not.throw
      expect(function () { self.observable.off(event + 'off', function () {}) }).to.not.throw
    })

    it('should remove listeners correctly', function () {
      var self = this
        , event = chance.word()

      self.observers.forEach(function (observer) {
        self.observable.on(event, observer)
      })

      self.observable.notify(event)
      self.observable.notify(event)

      self.observers.forEach(function (observer) {
        expect(observer).to.have.been.callCount(2)
        self.observable.off(event, observer)
      })

      self.observable.notify(event)

      self.observers.forEach(function (observer) {
        expect(observer).to.have.been.callCount(2)
      })
    })

  })


})
