import Config from '../../../lib/config'

describe('Config', function () {

  before(function () {
    Config.debug = false    // disable logs
  })

  var properties = {
    'debug': 'boolean',
    'styles': ['string'],
    'Dash': {
      'lib': 'string',
      'youbora': 'string'
    },
    'HLS': {
      'lib': 'string',
      'youbora': 'string'
    },
    'qubitClsService': 'string',
    'dependencies': [{
      'url': 'string'
    }],
    'languages': ['string']
  }

  it('should not have extra properties', function () {
    expect(Object.keys(Config)).to.deep.equal(Object.keys(properties))
  })

  function isObject(value) {
    return !!value && typeof value === 'object'
  }

  var MAX_SAFE_INTEGER = 9007199254740991 // (2^53) - 1
  function isLength(value) {
    return typeof value == 'number'
        && value > -1
        && value % 1 == 0
        && value <= MAX_SAFE_INTEGER
  }

  function isArray(value) {
    return isObject(value) && isLength(value.length) && Object.prototype.toString.call(value) === '[object Array]'
  }

  function testType(type, value, prefix) {
    prefix = prefix ? prefix + ' ' : ''
    it(prefix + 'should be a ' + type, function () {
      expect(value).to.be.a(type)
    })
  }

  function testProperty(types, object, name) {
    var value = object[name]

    it('should exist', function () {
      expect(value).to.exist
    })

    var type = types[name]
    if (!type) {
      // this property could be of any type, but must exist
      return
    }

    if (isArray(type)) {
      testArray(type[0], value)
    } else if (isObject(type)) {
      testObject(type, value)
    } else {
      testType(type, value)
    }
  }

  function testArray(type, values) {
    expect(values).to.be.an('array')

    if (isObject(type)) {
      describe('should be an array of objectss', function () {
        values.forEach(function (value, index) {
          testObject(type, value, '[' + index + ']')
        })
      })
    } else {
      describe('should be an array of ' + type + 's', function () {
        values.forEach(function (value, index) {
          testType(type, value[index], '[' + index + ']')
        })
      })
    }
  }

  function testObject(types, object, prefix) {
    prefix = prefix || ''
    Object.keys(types).forEach(function (name) {
      describe(prefix + '.' + name, function () {
        testProperty(types, object, name)
      })
    })
  }

  testObject(properties, Config)
})
