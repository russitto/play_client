import Config from '../../../lib/config'
import Options from '../../../lib/options'
import Utils from '../../../lib/utils'
import Dependencies from '../../../lib/dependencies'

describe('Options', function () {

  function createIncomplete() {
    var json = {
      source: [{
        lang: chance.word(),
        url: chance.url()
      }]
    }
    if (chance.bool()) {
      json.user = chance.email()
    }
    if (chance.bool()) {
      json.operator = chance.word()
    }
    if (chance.bool()) {
      json.transaction = chance.natural()
    }
    return json
  }

  function createComplete() {
    var source = chance.url()
    var lang = chance.word()
    return {
      source: [{url: source, lang: lang}],
      operator: chance.word(),
      transaction: chance.natural(),
      user: chance.email(),
      link: chance.url(),
      title: chance.word(),
      subtitleLanguage: null,
      audioLanguage: lang,
      youbora: {
        accountCode: chance.natural(),
        media: {
          source: source,
          isLive: false,
          resource: source
        },
        playerType: chance.word(),
        transactionCode: chance.natural(),
        username: chance.word()
      }
    }
  }

  before(function (done) {
    Config.debug = false      // disable logs
    Dependencies.load(done)   // for LZString
  })


  describe('.fromUriHash()', function () {

    function hashFromObject(object) {
      return '#' + window.LZString.compressToEncodedURIComponent(JSON.stringify(object))
    }

    it('should throw with an invalid hash', function (done) {
      var invalids = [
        undefined, null, [], {}, '', hashFromObject({url: chance.url()})
      ]

      var good = sinon.spy(function (options) {
        expect(options).to.be.null
        complete()
      })

      var error = sinon.spy(function (err) {
        expect(err).to.not.be.null
        complete()
      })

      var count = invalids.length
      function complete(err) {
        count--
        if (err) {
          done(err)
        } else if (count <= 0) {
          try {
            expect(good).to.have.not.been.called
            expect(error).to.have.been.callCount(invalids.length)
            done()
          } catch (e){
            done(e)
          }
        }
      }

      invalids.forEach(function (invalid) {
        Options.fromUriHash(invalid)
          .then(good)
          .catch(error)
      })
    })

    it('should return a complete object with an incomplete hash', function (done) {
      var count = chance.integer({min:5, max:10})
      var incompletes = []
      for (var i = 0; i < count; ++i) {
        incompletes.push(createIncomplete())
      }

      function complete(err) {
        count--
        if (count <= 0 || err) {
          done(err)
        }
      }

      incompletes.forEach(function (incomplete) {
        Options.fromUriHash(hashFromObject(incomplete))
          .then(function (response) {
            try {
              expect(response).to.deep.include(incomplete)
              complete()
            } catch (e) {
              complete(e)
            }
          })
          .catch(complete)
      })
    })

    it('should return a valid object with a valid hash', function (done) {
      var count = chance.integer({min:5, max:10})
      var valids = []
      for (var i = 0; i < count; ++i) {
        valids.push(createComplete())
      }

      function complete(err) {
        count--
        if (count <= 0 || err) {
          done(err)
        }
      }

      valids.forEach(function (valid) {
        Options.fromUriHash(hashFromObject(valid))
          .then(function (response) {
            try {
              expect(response).to.be.deep.equal(valid)
              complete()
            } catch (err) {
              complete(err)
            }
          })
          .catch(complete)
      })
    })

  })


  describe('.fromString()', function () {

    it('should throw with an invalid string', function () {
      var invalids = [
        undefined, null, [], {}, '', JSON.stringify({url: chance.url()})
      ]

      invalids.forEach(function (invalid) {
        expect(function () { Utils.fromString(invalid) }).to.throw
      })
    })

    it('should return a complete object with an incomplete string', function () {
      var count = chance.integer({min:5, max:10})
      for (var i = 0; i < count; ++i) {
        var incomplete = createIncomplete()
        var actual = Options.fromString(JSON.stringify(incomplete))
        expect(actual).to.deep.include(incomplete)
      }
    })

    it('should return a valid object with a valid string', function () {
      var count = chance.integer({min:5, max:10})
      for (var i = 0; i < count; ++i) {
        var valid = createComplete()
        var actual = Options.fromString(JSON.stringify(valid))
        expect(actual).to.deep.equal(valid)
      }
    })
  })


  describe('.getSourceUrl()', function () {

    before(function () {
      this.multiSourceOptions = {
        source: [
          { lang: 'es', url: chance.url() },
          { lang: 'en', url: chance.url() }
        ]
      }
    })

    it('should throw on options with invalid object', function () {
      var invalids = [undefined, null, '', 0, 123, true, [], {}]
      invalids.forEach(function (invalid) {
        expect(function () { Options.getSourceUrl(invalid) }).to.throw
      })
    })

    it('should work with a single source', function () {
      var url = chance.url()
      var options = {
        source: [
          { lang: chance.string(), url: url }
        ]
      }

      expect(Options.getSourceUrl(options)).to.equal(url)
    })

    it('should select the url by language', function () {
      expect(Options.getSourceUrl(this.multiSourceOptions, 'es')).to.equal(this.multiSourceOptions.source[0].url)
      expect(Options.getSourceUrl(this.multiSourceOptions, 'en')).to.equal(this.multiSourceOptions.source[1].url)
    })

    it('should select the first url if the language is invalid', function () {
      var invalids = [undefined, null, '', 0, 123, true, [], {}]
      var options = this.multiSourceOptions
      invalids.forEach(function (invalid) {
        expect(Options.getSourceUrl(options, invalid)).to.equal(options.source[0].url)
      })
    })

  })


})
