import Config from '../../../lib/config'
import Utils from '../../../lib/utils'

describe('Utils', function () {

  function fakeFile(filename) {
    return 'base/tests/unit/fakes/' + filename
  }

  var badUrls = [
    undefined,
    null,
    '',
    chance.string(),
    [],
    [chance.string()],
    {},
    {'url': chance.string()}
  ]

  before(function () {
    Config.debug = false    // disable logs
  })


  describe('.toArray()', function () {

    it('should return an empty array for non-array like objects', function () {
      expect( Utils.toArray({})      ).to.deep.equal([])
      expect( Utils.toArray({'a':1}) ).to.deep.equal([])
    })

    it('should throw an error on null or undefined object', function () {
      expect(function () { Utils.toArray()          }).to.throw()
      expect(function () { Utils.toArray(undefined) }).to.throw()
      expect(function () { Utils.toArray(null)      }).to.throw()
    })

    it('should return an empty array for an empty array-like object', function () {
      expect( Utils.toArray([]) ).to.deep.equal([])
      expect( Utils.toArray({}) ).to.deep.equal([])
      expect( Utils.toArray('') ).to.deep.equal([])
    })

    it('should work with array-like objects', function () {
      expect( Utils.toArray([0])                                 ).to.deep.equal([0])
      expect( Utils.toArray([0, 1])                              ).to.deep.equal([0, 1])
      expect( Utils.toArray([0, 1, 2])                           ).to.deep.equal([0, 1, 2])
      expect( Utils.toArray({0: 'a', 1: 'b', 2: 'c', length: 0}) ).to.deep.equal([])
      expect( Utils.toArray({0: 'a', 1: 'b', 2: 'c', length: 1}) ).to.deep.equal(['a'])
      expect( Utils.toArray({0: 'a', 1: 'b', 2: 'c', length: 2}) ).to.deep.equal(['a', 'b'])

      var args = function () { return Utils.toArray(arguments) }
      expect( args()              ).to.deep.equal([])
      expect( args('a')           ).to.deep.equal(['a'])
      expect( args('a', 'b')      ).to.deep.equal(['a', 'b'])
      expect( args('a', 'b', 'c') ).to.deep.equal(['a', 'b', 'c'])
    })

  })


  describe('.wasPageReloaded()', function () {

    it('should return false the first time a page is loaded', function () {
      expect(Utils.wasPageReloaded()).to.be.false
    })

    it('should return true after reloading the page', function () {
      // can't test this
    })

  })


  describe('.loadStyle()', function () {

    before(function () {
      var c = document.createElement('div')
      c.id = 'color_test'
      document.body.appendChild(c)
    })

    it('should fail to load with invalid urls', function () {
      badUrls.forEach(function (url) {
        expect(function () { Utils.loadStyle(url) }).to.throw
      })
    })

    it('should load a valid style url', function (done) {
      var el = document.getElementById('color_test')
      var style = window.getComputedStyle(el)

      expect(style['border-color']).to.be.equal('rgb(0, 0, 0)')
      expect(style['background-color']).to.be.equal('rgba(0, 0, 0, 0)')

      expect(Utils.loadStyle(fakeFile('style.css'))).to.be.instanceof(HTMLElement)

      setTimeout(function () {
        style = window.getComputedStyle(el)
        expect(style['border-color']).to.be.equal('rgb(1, 2, 3)')
        expect(style['background-color']).to.be.equal('rgb(4, 5, 6)')
        done()
      }, 200)
    })

  })


  describe('.loadScript()', function () {

    it('should fail to load with invalid urls', function (done) {
      var onLoad = sinon.spy(function () {
        loadComplete()
      })

      var onError = sinon.spy(function (err) {
        expect(err).to.be.an('error')
        loadComplete()
      })

      var initialCount = badUrls.length - 1
      var count = initialCount

      function loadComplete() {
        count--
        if (count <= 0) {
          expect(onLoad).to.have.not.been.called
          expect(onError).to.have.been.callCount(initialCount)
          done()
        }
      }

      badUrls.forEach(function (url) {
        var promise = Utils.loadScript(url)
        expect(promise).to.be.instanceof(Promise)
        promise
          .then(onLoad)
          .catch(onError)
      })
    })

    it('should load a valid script url', function (done) {
      expect(window.SCRIPT_0_LOADED).to.be.undefined

      var promise = Utils.loadScript(fakeFile('script_0.js'))

      expect(promise).to.be.instanceof(Promise)

      promise.then(function (script) {
        expect(script).to.be.instanceof(HTMLElement)
        expect(window.SCRIPT_0_LOADED).to.be.true
        done()
      })
    })

  })


  describe('.loadScripts()', function () {

    it('should fail to load with invalid urls', function (done) {
      var onLoad = sinon.spy(function () {
        loadComplete()
      })

      var onError = sinon.spy(function (err) {
        expect(err).to.be.an('error')
        loadComplete()
      })

      var initialCount = badUrls.length
      var count = initialCount

      function loadComplete() {
        count--
        if (count <= 0) {
          expect(onLoad).to.have.not.been.called
          expect(onError).to.have.been.callCount(initialCount)
          done()
        }
      }

      badUrls.forEach(function (url) {
        var promise = Utils.loadScripts(url)
        expect(promise).to.be.instanceof(Promise)
        promise
          .then(onLoad)
          .catch(onError)
      })
    })

    it('should fail to load scripts array with empty element', function (done) {
      var promise = Utils.loadScripts([
        fakeFile('script_0.js'),
        '',
        fakeFile('script_0.js')
      ])

      var onLoad = sinon.spy(function () {
        loadComplete()
      })

      var onError = sinon.spy(function (err) {
        expect(err).to.be.an('error')
        loadComplete()
      })

      function loadComplete() {
        expect(onLoad).to.have.not.been.called
        expect(onError).to.have.been.calledOnce
        done()
      }

      expect(promise).to.be.instanceof(Promise)
      promise
        .then(onLoad)
        .catch(onError)
    })

    it('should load a valid scripts array', function (done) {
      expect(window.SCRIPT_1_LOADED).to.be.undefined
      expect(window.SCRIPT_2_LOADED).to.be.undefined

      var promise = Utils.loadScripts([
        fakeFile('script_1.js'),
        fakeFile('script_2.js')
      ])

      expect(promise).to.be.instanceof(Promise)

      var bad = sinon.spy(loadComplete)
      var good = sinon.spy(function () {
        expect(window.SCRIPT_1_LOADED).to.be.true
        expect(window.SCRIPT_2_LOADED).to.be.true
        loadComplete()
      })

      function loadComplete() {
        expect(bad).to.have.not.been.called
        done()
      }

      promise.then(good).catch(bad)
    })
  })


  describe('.loadScriptAsync()', function () {

    it('should fail to load with invalid urls', function (done) {
      var onLoad = sinon.spy(function () {
        loadComplete()
      })

      var onError = sinon.spy(function (err) {
        expect(err).to.be.an('error')
        loadComplete()
      })

      var initialCount = badUrls.length
      var count = initialCount

      function loadComplete() {
        count--
        if (count <= 0) {
          expect(onLoad).to.have.not.been.called
          expect(onError).to.have.been.callCount(initialCount)
          done()
        }
      }

      badUrls.forEach(function (url) {
        expect(Utils.loadScriptAsync(url, onLoad, onError))
      })
    })

    it('should load a valid script url', function (done) {
      function loadComplete() {
        expect(onLoad).to.have.been.calledOnce
        expect(onError).to.have.not.been.calledWith(script)
        done()
      }

      var onLoad = sinon.spy(loadComplete)
      var onError = sinon.spy(loadComplete)

      var script = Utils.loadScriptAsync(fakeFile('script_0.js'), onLoad, onError)
      expect(script).to.be.instanceof(HTMLElement)
    })

  })


  describe('.bypass()', function () {

    it('should bypass a promise response', function (done) {
      var count = 2
      function complete() {
        count--
        if (count <= 0) {
          done()
        }
      }

      function toBypass() {
        return 'toBypass'
      }

      var data = 'a'

      // not bypassed case
      Promise.resolve(data)
        .then(toBypass)
        .then(function (value) {
          expect(value).to.be.equal(toBypass())
          complete()
        })
        .catch(done)

      // bypassed case
      Promise.resolve(data)
        .then(Utils.bypass(toBypass))
        .then(function (value) {
          expect(value).to.be.equal(data)
          complete()
        })
        .catch(done)
    })

    it('should fail on invalid functions', function () {
      var word = chance.string()
      var invalids = [undefined, null, 0, 123, '', word, [123], [123, 456], [word], [word, word], {'prop': word}]
      invalids.forEach(function (invalid) {
        expect(function () { Utils.bypass(invalid) }).to.throw
      })
    })

  })


  describe('.getBaseUrl()', function() {

    it('should work with valid urls', function () {
      var protocol = chance.pickone(['http', 'https'])
      var domain = chance.domain()
      var folder = chance.word()
      var subfolder = chance.word()
      var file = chance.word() + '.' + chance.pickone(['html', 'gif', 'jpg'])
      var query1 = chance.word() + '=' + chance.string()
      var query2 = chance.word() + '=' + chance.integer({min: 1, max: 100})
      var hash = chance.hash()

      var cases = [
        {
          url: protocol + '://' + domain,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/?' + query1,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/?' + query2,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/#' + hash,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/' + file,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/' + file + '?' + query1,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/' + file + '?' + query2,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/' + file + '#' + hash,
          base: protocol + '://' + domain + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + file,
          base: protocol + '://' + domain + '/' + folder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + file + '?' + query1,
          base: protocol + '://' + domain + '/' + folder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + file + '?' + query2,
          base: protocol + '://' + domain + '/' + folder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + file + '#' + hash,
          base: protocol + '://' + domain + '/' + folder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/' + file,
          base: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/' + file + '?' + query1,
          base: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/' + file + '?' + query2,
          base: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/' + file + '#' + hash,
          base: protocol + '://' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: '//' + domain,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/?' + query1,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/?' + query2,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/#' + hash,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/' + file,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/' + file + '?' + query1,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/' + file + '?' + query2,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/' + file + '#' + hash,
          base: '//' + domain + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + file,
          base: '//' + domain + '/' + folder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + file + '?' + query1,
          base: '//' + domain + '/' + folder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + file + '?' + query2,
          base: '//' + domain + '/' + folder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + file + '#' + hash,
          base: '//' + domain + '/' + folder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + subfolder + '/' + file,
          base: '//' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + subfolder + '/' + file + '?' + query1,
          base: '//' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + subfolder + '/' + file + '?' + query2,
          base: '//' + domain + '/' + folder + '/' + subfolder + '/'
        },
        {
          url: '//' + domain + '/' + folder + '/' + subfolder + '/' + file + '#' + hash,
          base: '//' + domain + '/' + folder + '/' + subfolder + '/'
        }
      ]

      cases.forEach(function (c) {
        expect(Utils.getBaseUrl(c.url)).to.be.equal(c.base)
      })
    })

    it('should throw with invalid urls', function () {
      badUrls.forEach(function (bad) {
        expect(function () { Utils.getBaseUrl(bad) }).to.throw
      })
    })

  })


  describe('.removeAll()', function() {

    beforeEach(function () {
      var body = document.querySelector('body')
      var ids = ['a', 'b', 'c', 'd']
      ids.forEach(function (id) {
        var div = document.createElement('div')
        div.id = id
        body.appendChild(div)
      })

      this.body = body
      this.ids = ids
    })

    it('should remove all elements for a given selector', function () {

      this.ids.forEach(function (id) {
        expect(document.getElementById(id)).to.not.be.null
      })

      Utils.removeAll(this.body, 'div')

      this.ids.forEach(function (id) {
        expect(document.getElementById(id)).to.be.null
      })
    })

    it('should remove nothing if no element is found', function () {
      var originalBody = this.body.outerHTML
      Utils.removeAll(this.body, chance.word())
      expect(originalBody).to.be.equal(this.body.outerHTML)
    })

    it('should throw on invalid element', function () {
      expect(function () { Utils.removeAll()          }).to.throw
      expect(function () { Utils.removeAll(undefined) }).to.throw
      expect(function () { Utils.removeAll(null)      }).to.throw
      expect(function () { Utils.removeAll([])        }).to.throw
      expect(function () { Utils.removeAll({})        }).to.throw
    })

  })


  describe('.get()', function () {
    //TODO
  })


  describe('.post()', function () {
    //TODO
  })


  describe('.querystring()', function () {

    it('should return an empty string with invalid or empty objects', function () {
      var word = chance.string()
      var invalids = [undefined, null, 0, 123, '', word, [], {}, [word], [word, word]]
      invalids.forEach(function (invalid) {
        expect(Utils.querystring(invalid)).to.equal('')
      })
    })

    it('should work with valid objects', function () {
      var query = ''
      var key = null
      var value = null
      var object = {}

      for (var i = 0; i < 5; i++) {
        key = chance.string()
        value = chance.string()
        object[key] = value
        if (query) {
          query += '&'
        }
        query += encodeURIComponent(key) + '=' + encodeURIComponent(value)
        expect(Utils.querystring(object)).to.equal(query)
      }
    })

  })


  describe('.findElementPosition()', function () {

    it('should return {left:0, top:0} for an element that is not in the page', function () {
      var element = document.createElement('div')
      expect(Utils.findElementPosition(element)).to.deep.equal({left: 0, top: 0})
    })

    it('should return the position of an element in the page', function () {
      //TODO: test this
    })

  })

})
