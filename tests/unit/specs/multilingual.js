import Config from '../../../lib/config'
import Multilingual from '../../../lib/multilingual'
import Observable from '../../../lib/observable'

describe('Multilingual', function () {

  before(function () {
    Config.debug = false     // disable logs
  })

  beforeEach(function () {
    var languages = [chance.word(), chance.word()]
    this.languages = languages
    this.derived = Object.create(Multilingual('test' + Math.random()), {
      languages: { value: function () {
        return languages
      }}
    })
  })

  it('should be an observable', function () {
    var multilingual = Multilingual()
      , observable = Observable()

    for (var property in observable) {
      expect(multilingual).to.respondTo(property)
    }
  })


  describe('.CHANGED', function () {

    it('should have the form [name]_LANGUAGE_CHANGED', function () {
      var suffix = '_LANGUAGE_CHANGED'
      var word = chance.word()

      expect(Multilingual().CHANGED).to.be.equal(suffix)
      expect(Multilingual('_').CHANGED).to.be.equal('_' + suffix)
      expect(Multilingual(word).CHANGED).to.be.equal(word + suffix)
    })

    it('should be constant', function () {
      var multi = Multilingual()
        , original = multi.CHANGED

      expect(multi.CHANGED).to.be.equal(original)
      expect(function () { multi.CHANGED = 'new value' }).to.throw()
      expect(multi.CHANGED).to.be.equal(original)
    })

  })


  describe('.languages()', function () {

    it('should be derived', function () {
      var base = Multilingual()
      expect(function () { base.languages() }).to.throw
      expect(this.derived.languages()).to.deep.equal(this.languages)
    })

    it('should return an array of strings', function () {
      var base = Multilingual()
      expect(function () { base.languages() }).to.throw

      var languages = this.derived.languages()
      expect(languages).to.be.an('array')
      languages.forEach(function (language) {
        expect(language).to.be.a('string')
      })
    })
  })


  describe('.currentLanguage() / .selectLanguage()', function () {

    it('should be able to get and set the current language', function () {
      var derived = this.derived
      expect(derived.currentLanguage()).to.be.undefined

      derived.selectLanguage('es')
      expect(derived.currentLanguage()).to.be.equal('es')

      derived.selectLanguage('en')
      expect(derived.currentLanguage()).to.be.equal('en')

      derived.selectLanguage('ja')
      expect(derived.currentLanguage()).to.be.equal('ja')
    })

    it('should notify when language change', function (done) {
      var derived = this.derived

      derived.on(derived.CHANGED, function () {
        expect(derived.currentLanguage()).to.be.equal('es')
        done()
      })

      expect(derived.currentLanguage()).to.be.undefined
      derived.selectLanguage('es')
    })

  })
})
