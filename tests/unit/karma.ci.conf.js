/* globals module, require */
module.exports = function(config) {
  var base = JSON.parse(JSON.stringify(require('./base.conf.js')(config)))

  // start these browsers
  // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
  base.browsers = ['Chrome', 'Edge', 'Firefox']

  // Continuous Integration mode
  // if true, Karma captures browsers, runs the tests and exits
  base.singleRun = true

  config.set(base)
}
