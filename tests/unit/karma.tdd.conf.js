/* globals module, require */
module.exports = function(config) {
  var base = JSON.parse(JSON.stringify(require('./base.conf.js')(config)))

  // start these browsers
  // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
  base.browsers = ['Chrome']

  // Continuous Integration mode
  // if true, Karma captures browsers, runs the tests and exits
  base.singleRun = false

  // test results reporter to use
  // possible values: 'dots', 'progress'
  // available reporters: https://npmjs.org/browse/keyword/karma-reporter
  base.reporters = ['spec']

  config.set(base)
}
