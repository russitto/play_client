module.exports = {
  "env": {
    "mocha": true,
  },
  "globals": {
    "chance": true,
    "chai": true,
    "sinon": true,
    "expect": true, // chai expect
  }
}
