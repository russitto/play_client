module.exports = {
  'player can load': function (browser) {
    browser
      .url(browser.globals.url)
      .waitForElementVisible('video', 5000)
      .waitForElementVisible('.video-js', 5000)
      .waitForElementNotVisible('.vjs-poster', 30000)
  },

  'can play/pause': function (browser) {
    const context = {}
    const control = '.vjs-play-control'
    browser
      .waitForElementPresent(control, 1000)
      .perform(getProperty(browser, context, 'video', 'paused'))
      .getAttribute(control, 'class', function (result) {
        this.assert.notEqual(result.value.indexOf('vjs-playing'), -1, 'play button has vjs-playing class')
        this.assert.ok(!context.paused, 'video is playing')
      })
      .click(control)
      .perform(getProperty(browser, context, 'video', 'paused'))
      .getAttribute(control, 'class', function (result) {
        this.assert.notEqual(result.value.indexOf('vjs-paused'), -1, 'play button has vjs-paused class')
        this.assert.ok(context.paused, 'video is paused')
      })
      .click(control)
      .perform(getProperty(browser, context, 'video', 'paused'))
      .getAttribute(control, 'class', function (result) {
        this.assert.notEqual(result.value.indexOf('vjs-playing'), -1, 'play button has vjs-playing class')
        this.assert.ok(!context.paused, 'video is playing')
      })
  },

  'can mute/unmute': function (browser) {
    const context = {}
    const control = '.vjs-volume-menu-button'
    browser
      .waitForElementPresent(control, 1000)
      .perform(getProperty(browser, context, 'video', 'muted'))
      .getAttribute(control, 'class', function (result) {
        this.assert.equal(result.value.indexOf('vjs-vol-0'), -1, 'volume button has vjs-vol-0 class')
        this.assert.ok(!context.muted, 'video is not muted')
      })
      .click(control)
      .perform(getProperty(browser, context, 'video', 'muted'))
      .getAttribute(control, 'class', function (result) {
        this.assert.notEqual(result.value.indexOf('vjs-vol-0'), -1, 'volume button has other class')
        this.assert.ok(context.muted, 'video is muted')
      })
      .click(control)
      .perform(getProperty(browser, context, 'video', 'muted'))
      .getAttribute(control, 'class', function (result) {
        this.assert.equal(result.value.indexOf('vjs-vol-0'), -1, 'volume button has vjs-vol-0 class')
        this.assert.ok(!context.muted, 'video is not muted')
      })
  },

  'can enter/exit from fullscreen': function (browser) {
    const context = {}
    const control = '.vjs-fullscreen-control'
    browser
      .waitForElementPresent(control, 1000)
      .perform(getProperty(browser, context, 'document', 'fullscreenElement'))
      .getAttribute(control, 'class', function () {
        this.assert.strictEqual(context.fullscreenElement, null, 'video is not in fullscreen')
      })
      .click(control)
      .perform(getProperty(browser, context, 'document', 'fullscreenElement'))
      .getAttribute(control, 'class', function () {
        this.assert.notEqual(context.fullscreenElement, null, 'video is in fullscreen')
      })
      .click(control)
      .perform(getProperty(browser, context, 'document', 'fullscreenElement'))
      .getAttribute(control, 'class', function () {
        this.assert.strictEqual(context.fullscreenElement, null, 'video is not in fullscreen')
      })
  },

  'can change subtitles': function (browser) {
    const context = {}
    const control = '.vjs-subtitles-button'
    browser
      .perform(getPlayerState(browser, context))
      .isVisible(control, function (result) {
        const count = context.Player.Subtitles.languages.length
        if (!result.value) {
          browser.assert.ok(count == 1, 'we have no subtitles, ' + control + ' should be invisible')
        } else {
          browser.assert.ok(count > 1, 'we have ' + count + ' subtitles, ' + control + ' button should be visible')
        }
      })
      .click(control)
      .click(control + ' .vjs-menu ul li:nth-child(1)')
      .perform(getPlayerState(browser, context))
      .getAttribute(control, 'class', function () {
        this.assert.equal(context.Player.Subtitles.currentLanguage, context.Player.Subtitles.languages[0], 'we can select the first subtitle in the list')
      })
      .click(control)
      .click(control + ' .vjs-menu ul li:nth-child(2)')
      .perform(getPlayerState(browser, context))
      .getAttribute(control, 'class', function () {
        this.assert.equal(context.Player.Subtitles.currentLanguage, context.Player.Subtitles.languages[1], 'we can select the second subtitle in the list')
      })
  },

  'can change audio': function (browser) {
    const context = {}
    const control = '.vjs-audio-button'
    browser
      .perform(getPlayerState(browser, context))
      .isVisible(control, function (result) {
        const count = context.Player.Audio.languages.length
        if (!result.value) {
          browser.assert.ok(count == 1, 'we have only one audio track, ' + control + ' should be invisible')
        } else {
          browser.assert.ok(count > 1, 'we have ' + count + ' audio tracks, ' + control + ' button should be visible')
        }
      })
      .click(control)
      .click(control + ' .vjs-menu ul li:nth-child(1)')
      .perform(getPlayerState(browser, context))
      .getAttribute(control, 'class', function () {
        this.assert.equal(context.Player.Audio.currentLanguage, context.Player.Audio.languages[0], 'we can select the first audio track in the list')
      })
      .click(control)
      .click(control + ' .vjs-menu ul li:nth-child(2)')
      .perform(getPlayerState(browser, context))
      .getAttribute(control, 'class', function () {
        this.assert.equal(context.Player.Audio.currentLanguage, context.Player.Audio.languages[1], 'we can select the second audio track in the list')
      })
  },

  'can seek': function (browser) {
    const context = {}
    const control = '.vjs-progress-holder'
    browser
      .waitForElementPresent(control, 1000)
      .element('css selector', control, function (response) {
        const id = response.value.ELEMENT
        const wait = 250  // milliseconds
        const start = 10    // x pixels form the start of the seek bar
        const middle = 100  // x pixels form the start of the seek bar
        const end = 150     // x pixels form the start of the seek bar
        browser
          .moveTo(id, start, 0).mouseButtonClick().pause(wait)
          .perform(getProperty(browser, context, 'video', 'currentTime', 'startTime'))
          .moveTo(id, end, 0).mouseButtonClick().pause(wait)
          .perform(getProperty(browser, context, 'video', 'currentTime', 'endTime'))
          .getAttribute(control, 'class', function () {
            this.assert.ok(context.startTime < context.endTime, 'we can seek forward')
          })
          .perform(getProperty(browser, context, 'video', 'currentTime', 'startTime'))
          .moveTo(id, middle, 0).mouseButtonClick().pause(wait)
          .perform(getProperty(browser, context, 'video', 'currentTime', 'endTime'))
          .getAttribute(control, 'class', function () {
            this.assert.ok(context.startTime > context.endTime, 'we can seek backward')
          })
      })
  },

  'it shows related content at the start of the credits': function (browser) {
    const context = {}
    const relatedScreen = '.related-screen'
    const progressControl = '.vjs-progress-holder'
    browser
      .perform(getPlayerState(browser, context))
      .perform(getProperty(browser, context, 'video', 'duration'))
      .getElementSize(progressControl, function (result) {
        const progressBarWidth = result.value.width
        const pixelBeforeCredits = Math.round(timeToPixel(context.Player.options.creditsBegin - 3, 0, context.duration, 0, progressBarWidth))
        browser.element('css selector', progressControl, function (response) {
          const id = response.value.ELEMENT
          const wait = 250  // milliseconds
          browser
            .moveTo(id, 10, 0).mouseButtonClick().pause(wait)
            .waitForElementNotVisible(relatedScreen, 1000)
            .moveTo(id, pixelBeforeCredits, 0).mouseButtonClick().pause(wait)
            .waitForElementVisible(relatedScreen, 60000)  // yes, a minute, is not an error
            .click('.related-close-container')  // close related screen (to allow further testing)
        })
      })
  },

  'it navigates to the details page when click on title': function (browser) {
    const context = {}
    const control = '.video-js-title'
    browser
      .perform(getPlayerState(browser, context))
      .getAttribute(control, 'class', function () {
        const url = context.Player.options.link
        browser
          .waitForElementPresent(control, 1000)
          .click(control).pause(1000)
          .assert.urlEquals(url)
      })
  },

  // close the browser when all tests finished
  after: function (browser) {
    browser.end()
  }
}

function getProperty(browser, context, selector, property, saveAs) {
  saveAs = saveAs || property

  return function (done) {
    browser.execute(function (selector, property) {
      if (selector === 'document') {
        return document[property]
      }

      var element = document.querySelector(selector)
      if (element) {
        return element[property]
      }

      throw new Error('invalid selector: "' + selector + '"')
    }, [selector, property], function (result) {
      context[saveAs] = result.value
      done()
    })
  }
}

function getPlayerState(browser, context) {
  return function (done) {
    browser.execute(function () {
      var player = window.QB.Player

      return {
        options: window.QB.options,
        Audio: {
          languages: player.Audio.languages(),
          currentLanguage: player.Audio.currentLanguage()
        },
        Subtitles: {
          languages: player.Subtitles.languages(),
          currentLanguage: player.Subtitles.currentLanguage()
        }
      }
    }, [], function (result) {
      context.Player = result.value
      done()
    })
  }
}

function timeToPixel(currentTime, minTime, maxTime, minPixel, maxPixel) {
  if (maxPixel < minPixel) {
    throw new Error('minPixel is bigger than maxPixel')
  }

  if (maxTime < minTime) {
    throw new Error('minTime is bigger than maxTime')
  }

  const totalTime = maxTime - minTime
  const totalPixels = maxPixel - minPixel
  const t = currentTime - minTime

  return totalPixels * (t / totalTime)
}
