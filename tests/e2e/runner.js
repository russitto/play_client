const request = require('request')
const fs = require('fs')
const expect = require('chai').expect
const config = require('./config.js')

const req = {
  url: config.test_settings.default.globals.apiUrl,
  json: true,
  strictSSL: false,
  body: {
    hash: config.test_settings.default.globals.hash,
    operator: 'qubit_vod',
    quality: 'HD',
    language: 'en',
    stream: 'dash',
    username: config.test_settings.default.globals.username,
    uuid: config.test_settings.default.globals.uuid
  },
  headers: {
    'User-Agent': 'nightwatch'
  }
}

function getConfigContents(config, url) {
  const newConfig = JSON.parse(JSON.stringify(config))
  if (newConfig.test_settings.default.globals) {
    newConfig.test_settings.default.globals.url = url
  } else {
    newConfig.test_settings.default.globals = {url: url}
  }

  return 'module.exports = ' + JSON.stringify(newConfig)
}

request.post(req, function (error, response, body) {
  try {
    expect(error).to.equal(null)
    expect(response.statusCode).to.equal(200)
    expect(body.embedUrl).to.not.equal(null)

    const tempConfig = 'nightwatch.conf.js'
    fs.writeFile(tempConfig, getConfigContents(config, body.embedUrl), function (fileError) {
      expect(fileError).to.equal(null)

      const environments = Object.keys(config.test_settings).filter(function (key) { return key !== 'default' })
      run(environments, tempConfig, function (err, code) {
        fs.unlink(tempConfig)
        if (err) {
          throw error
        }
        process.exit(code)
      })
    })
  } catch (e) {
    console.log('ERROR for request to:', req.url)
    console.log('status:', response.statusCode)
    console.log('body:', body)
    throw e
  }
})

function run(environments, config, callback, code) {
  if (!environments || environments.length == 0) {
    return callback(null, code | 0)
  }

  test(environments[0], config, function (err, code) {
    if (err) {
      return callback(err)
    }

    run(environments.slice(1), config, callback, code)
  })
}

function banner(text, size, lineChar) {
  size = size || 60
  lineChar = lineChar || '='

  let middle = ' [ ' + text + ' ] '
  if (!text || text.length == 0) {
    middle = ''
  }

  const lineSize = (size - middle.length) / 2
  if (lineSize < 0) {
    console.log(middle)
  } else {
    let line = ''
    for (let i = 0; i < lineSize; i++) {
      line += lineChar
    }
    let out = line + middle + line
    if (out.length % 2 == 0) {
      out += lineChar
    }
    console.log(out)
  }
}

function test(environment, config, callback) {
  const options = ['--config', config, '--env', environment]

  banner(environment)

  const spawn = require('cross-spawn')
  const runner = spawn('./node_modules/nightwatch/bin/nightwatch', options, {stdio: 'inherit'})

  runner.on('exit', function (code) {
    callback(null, code)
  })

  runner.on('error', function (err) {
    callback(err)
  })
}
