/* globals module, require */
module.exports = {
  src_folders: ['tests/e2e/specs'],
  output_folder: 'tests/e2e/reports',

  test_settings: {
    default: {
      selenium_port: 4444,
      // selenium_host: '192.168.19.23',
      selenium_host: '192.168.18.140',
      silent: true,
      globals: {
        apiUrl: 'https://' + getIPs()[0] + ':8090/v1/play',
        username: 'lsanchez@qubit.tv',
        hash: '%0F%F0%C9%F7%F5b%91%D5%FD%E1r%07%28%D6v%B5%9DKYu%117%96%ECUeF%B1%D2%F2O%FC%DE%B7%83%26%91U%E0%D80%B1%AC%BAN%91FS%C6%0E%21%B6%2F%BA%3D%CF%DA%FF%CA-%BC%85%BF%CD%17%A0ZX%21%40%2A%81%C4%60%7D%C8%2A%0A%84%AD',
        uuid: '09d55bd5-81f3-4cf2-9944-c12d7b1da5c8_CrounchingTigerHiddenDragon2000'
      }
    },

    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },

    /*
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },

    ie: {
      desiredCapabilities: {
        browserName: 'internet explorer',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },

    edge: {
      desiredCapabilities: {
        browserName: 'MicrosoftEdge',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
    */

  }
}

function getIPs() {
  var ifaces = require('os').networkInterfaces()
  var addresses = []
  Object.keys(ifaces).forEach(function (ifname) {
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return
      }

      addresses.push(iface.address)
    })
  })

  return addresses
}
