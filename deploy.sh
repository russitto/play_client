#!/bin/bash
ENVIRONMENT="$1"

workdir="$HOME/repos/play_client"
if [ "staging" == "$ENVIRONMENT" ] ; then
  workdir="/var/www/dash/p"
fi

cd $workdir
git pull
