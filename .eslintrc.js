module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "node": false
  },
  "globals": {
    "videojs": true,
    "_": true,
    "LZString": true,
    "$YB": true   // youbora
  },
  "parserOptions": {
    "sourceType": "module"
  },
  "extends": "eslint:recommended",
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "single",
      { "allowTemplateLiterals": true }
    ],
    "semi": [
      "error",
      "never"
    ],
    "max-len": [
      "warn",
      160
    ],
    "no-extra-semi": "error",
    "no-console": "warn",
    "curly": "error",
    "prefer-const": "error"
  }
}
