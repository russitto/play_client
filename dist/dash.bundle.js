(function () {
'use strict';

/**
 * Configuration of the client
 *
 * @module lib/config
 */

/**
 * A flag to know if we are in debug mode or not.
 * This enables or disables output of some messages.
 * <br>
 * In production release set to <code>false</code>.
 *
 * @member {Boolean} debug
 */
var debug = true;

/**
 * @private
 */
var scriptSuffix = (debug ? '.js' : '.min.js');

/**
 * URL of qubit services api.
 *
 * Has the form <b>domain</b>/index.php/<b>version</b>/<b>datatype</b>/<b>apkid</b> where:
 *
 * <ul>
 *   <li><b>domain</b> is the url to the service (staging, prod, etc)</li>
 *   <li><b>version</b> is the version of the api to use: 'v1', 'v2', etc.</li>
 *   <li><b>datatype</b> is the type of the response, we will mostly use <code>json</code></li>
 *   <li><b>apkid</b> is the unique id of the "application" issuing the request</li>
 * </ul>
 *
 * @private
 */
// const qubitApiUrl = '//servicesv5.qbst.com.ar/index.php/v2/json/9088-697E'  // staging
var qubitApiUrl = '//services.qubit.tv/index.php/v2/json/9088-697E';  // prod

/**
 * All styles used by the player. Each url must be relative to the html file or absolute.
 *
 * @member {String[]} styles
 */
var styles = [
  '//vjs.zencdn.net/5.16.0/video-js.min.css',
  '../dist/qubit.css'
];

/**
 * Shaka urls
 *
 * @member {Object} Dash
 * @member {String} Dash.lib
 * @member {String} Dash.youbora
 * @see https://github.com/google/shaka-player
 */
var Dash = {
  lib: '//cdnjs.cloudflare.com/ajax/libs/shaka-player/2.1.8/shaka-player.compiled.js',
  youbora: '//smartplugin.youbora.com/v5/javascript/videojs5/5.3.8/sp.min.js'
};

/**
 * HLS urls
 *
 * @member {Object} HLS
 * @member {String} HLS.lib
 * @member {String} HLS.youbora
 * @see https://github.com/video-dev/hls.js
 */
var HLS = {
  lib: '//cdn.jsdelivr.net/hls.js/0.7.6/hls' + scriptSuffix,
  youbora: '//smartplugin.youbora.com/5.0/js/hlsjs/5.3.0/sp.min.js'
};

/**
 * URL of Qubit CLS service, used to know if we can play a given content for the given user
 *
 * @member {String} qubitClsService
 */
var qubitClsService = qubitApiUrl + '/commercial/cls';

/**
 * List of available languages. This will be loaded to videojs interface.
 *
 * @member {String[]} languages
 */
var languages = ['es'];

var Config = {
  debug: debug,
  styles: styles,
  Dash: Dash,
  HLS: HLS,
  qubitClsService: qubitClsService,
  languages: languages
};

/**
 * A collection of general use functions
 *
 * @module lib/utils
 */

/**
 * Synonim of {@link https://developer.mozilla.org/en/docs/Web/API/Console/log|console.log},
 * only if {@link Config.debug} is enabled.
 */
function log() {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  if (Config.debug) {
    console.log.apply(console, args);  // eslint-disable-line no-console
  }
}

var _errorFunction = console.error ? console.error : console.log; // eslint-disable-line no-console

/**
 * Synonim of {@link https://developer.mozilla.org/en/docs/Web/API/Console/error|console.error}
 * if available, {@link https://developer.mozilla.org/en/docs/Web/API/Console/log|console.log} otherwise.
 */
function error() {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  _errorFunction.apply(console, args);
}

/**
 * Loads a style on the page.
 *
 * @param {!String} url - The url of the style to load.
 * @return {HTMLElement} The link element attached to document's head.
 */
function loadStyle(url) {
  log('Utils.loadStyle("' + url + '")');

  if (_.isEmpty(url)) {
    throw new Error('Utils.loadStyle(): url can\'t be null, undefined or empty')
  }

  var style = document.createElement('link');
  style.rel = 'stylesheet';
  style.href = url;

  var head = document.querySelector('head');
  head.appendChild(style);

  return style
}

/**
 * Loads a script on the page.
 *
 * @param {!String} url - The url of the script to load.
 * @param {Function} [onLoad] - A function of the form <code>function onLoad(element) {}</code><br>
 *                              Called when the script was succesfully loaded.
 * @param {Function} [onError] - A function of the form <code>function onError(error) {}</code><br>
 *                               Called when the script could not be loaded.
 * @return {HTMLElement} The script element attached to document's head.
 */
function loadScriptAsync(url, onLoad, onError) {
  log('Utils.loadScriptAsync("' + url + '")');

  if (_.isEmpty(url)) {
    onError && onError(new Error('Utils.loadScriptAsync(): url can\'t be null, undefined or empty'));
    return null
  }

  var script = document.createElement('script');
  script.src = url;

  var head = document.querySelector('head');
  head.appendChild(script);

  function clean() {
    script.removeEventListener('load', onScriptLoaded);
    script.removeEventListener('error', onScriptLoadError);
  }

  function onScriptLoaded() {
    log('Utils.loadScriptAsync(): "' + url + '" loaded');
    clean();
    onLoad(script);
  }

  function onScriptLoadError() {
    error('Utils.loadScriptAsync(): "' + url + '" loading error');
    clean();
    onError(new Error('Failed to load ' + url));
  }

  if (onLoad) {
    script.addEventListener('load', onScriptLoaded);
  }

  if (onError) {
    script.addEventListener('error', onScriptLoadError);
  }

  return script
}

/**
 * Loads a script on the page.
 *
 * @param {String} url - The url of the script to load
 * @return {Promise} A promise that resolve to an {@link https://www.w3schools.com/jsref/dom_obj_all.asp|HTMLElemnt}
 */
function loadScript(url) {
  return new Promise(function (resolve, reject) {
    loadScriptAsync(url, resolve, reject);
  })
}

/**
 * Loads multiple scripts on the page, sequantially
 *
 * @param {Stringp[]} urls - The list of urls to load in order
 * @return {Promise} A promise that resolve when the last url was loaded
 */
function loadScripts(urls) {
  log('Utils.loadScripts([' + urls + '])');

  return new Promise(function (resolve, reject) {
    if (!_.isArray(urls)) {
      return reject(new Error('Utils.loadScripts(): urls must be an array of strings'))
    }

    if (_.isEmpty(urls)) {
      return reject(new Error('Utils.loadScripts(): urls can\'t be null, undefined or empty'))
    }

    (function load(ref) {
      var url = ref[0];
      var rest = ref.slice(1);

      if (!_.isString(url)) {
        return reject(new Error('Utils.loadScripts(): url must be a string'))
      }

      if (_.isEmpty(url)) {
        return reject(new Error('Utils.loadScripts(): url can\'t be null, undefined or empty'))
      }

      var callback = resolve;
      if (!_.isEmpty(rest)) {
        callback = load.bind(null, rest);
      }

      loadScriptAsync(url, callback, reject);
    })(urls);
  })
}

/**
 * Transforms an array like object into an array. For this to work, the object must have:
 * <ul>
 *  <li>An integer index accessor: <code>object[index]</code>
 *  <li>A length property: <code>object.length</code>
 * </ul>
 *
 * @example
 * var object = {0: 'first', 1: 'second', 3: 'third', length: 3}
 * var array = Utils.toArray(object)
 * // array is ['first', 'second', 'third']
 *
 * @param {!Object} object - Any array like object
 * @return {Object[]} A native javascript array with all the elements of the original object
 */
function toArray(object) {
  return Array.prototype.slice.call(object, 0)
}

/**
 * Bypass the value returned by a promise to the next one
 *
 * @example
 * var first = new Promise(function (resolve) { resolve("first") })
 * var second = new Promise(function (resolve) { resolve("second") })
 * var log = console.log.bind(console)
 *
 * first
 *   .then(second)
 *   .then(log)     // ouputs "second"
 *
 * first
 *   .then(Utils.bypass(second))
 *   .then(log)     // outputs "first"
 *
 * @param {!Function} fn - A function that returns a value or a Promise. This will be called and bypassed.
 * @param {...*} args - Extra arguments passed to the function.<br>
 *                      As <code>fn.apply(null, args)</code>
 * @return {Function} A function that returns its own argument when the promise resolves
 */
function bypass(fn) {
  var args = [], len = arguments.length - 1;
  while ( len-- > 0 ) args[ len ] = arguments[ len + 1 ];

  if (!_.isFunction(fn)) {
    throw new Error('Utils.bypass(): fn must be a function')
  }

  if (!fn) {
    throw new Error('Utils.bypass(): fn can\'t be null or undefined')
  }

  return function (argument) {
    if (_.isEmpty(args)) {
      args = [argument];
    }
    return Promise.resolve(fn.apply(null, args)).then(function () { return argument; })
  }
}

/**
 * Check if the page was reloaded
 *
 * @return {Boolean} <code>true</code> if the page was reloaded, <code>false</code> otherwise
 */
function wasPageReloaded() {
  return !(window.performance && window.performance.navigation.type != 1)
}

/**
 * Performs an HTTP request.
 *
 * @private
 * @param {!String} url - The url to perform the requesst to.
 * @param {!String} method - The method to use: POST, GET, etc.
 * @param {?Object} headers - Extra headers to add to the request.
 * @param {?Object} data - The data to send on the request body.
 * @return {Promise} A promise that resolves to the responseText of the request
 */
function request(url, method, headers, data) {
  log('Utils.request("' + url + '", ' + method + ', ' + JSON.stringify(headers) + ', ' + JSON.stringify(data) + ')');

  return new Promise(function (resolve, reject) {
    if (_.isEmpty(url)) {
      return reject(new Error('Utils.request(' + method + '): url can\'t be null, undefined or empty'))
    }

    if (!_.isString(url)) {
      return reject(new Error('Utils.request(' + method + '): url must be a string'))
    }

    if (_.isEmpty(method)) {
      return reject(new Error('Utils.request(' + method + '): method can\'t be null, undefined or empty'))
    }

    if (!_.isString(method)) {
      return reject(new Error('Utils.request(' + method + '): method must be a string'))
    }

    var xhr = new XMLHttpRequest();

    function clean() {
      xhr.removeEventListener('error', onError);
      xhr.removeEventListener('abort', onError);
      xhr.removeEventListener('load', onLoad);
    }

    function onError(err) {
      log('Utils.request(' + method + ', ' + url + '): rejecting with error');
      clean();
      reject(new Error(err));
    }

    function onLoad() {
      clean();
      var status = xhr.status;
      if (status >= 200 && status <= 299) {
        log(("Utils.request(" + url + ", " + method + ") resolving with " + status + " :: " + ((xhr.responseText || '').substring(0, 50))));
        resolve(xhr.responseText);
      } else {
        log(("Utils.request(" + url + ", " + method + ") rejecting with " + status + " :: " + ((xhr.responseText || '').substring(0, 50))));
        reject(xhr.responseText);
      }
    }

    xhr.addEventListener('error', onError);
    xhr.addEventListener('abort', onError);
    xhr.addEventListener('load', onLoad);
    xhr.open(method, url, true);

    headers = headers || [];

    // use application/json by default but if we got null then do not pass Content-Type
    if (headers['Content-Type'] !== null) {
      headers['Content-Type'] = 'application/json';
    }

    Object.keys(headers)
      .forEach(function (name) {
        var value = headers[name];
        if (value) {
          xhr.setRequestHeader(name, value);
        }
      });

    if (_.isEmpty(data)) {
      xhr.send();
    } else {
      xhr.send(JSON.stringify(data));
    }
  })
}

/**
 * Performs an HTTP GET request.
 *
 * @function get
 * @param {!String} url - The url to perform the request to.
 * @param {?Object} headers - Extra headers to add to the request.
 * @return {Promise} A promise that resolves to the responseText of the request
 */
var get = function (url, headers) {
  return request(url, 'GET', headers, null)
};

/**
 * Performs an HTTP POST request.
 *
 * @function post
 * @param {!String} url - The url to perform the request to.
 * @param {?Object} headers - Extra headers to add to the request.
 * @param {?Object} data - The data to send in the request.
 * @return {Promise} A promise that resolves to the responseText of the request
 */
var post = function (url, headers, data) {
  return request(url, 'POST', headers, data)
};

/**
 * Returns the base of a given url
 *
 * @example
 * Utils.getBaseUrl('http://dom.ain/folder/file.html')      // output 'http://dom.ain/folder/'
 * Utils.getBaseUrl('http://dom.ain/folder/sub/file.html')  // output 'http://dom.ain/folder/sub/'
 * Utils.getBaseUrl('//dom.ain/folder/sub/file.html')       // output '//dom.ain/folder/sub/'
 * Utils.getBaseUrl('http://dom.ain/file.html')             // output 'http://dom.ain/'
 * Utils.getBaseUrl('http://dom.ain/file.html?param=1')     // output 'http://dom.ain/'
 * Utils.getBaseUrl('http://dom.ain/file.html#hash')        // output 'http://dom.ain/'
 * Utils.getBaseUrl('http://dom.ain')                       // output 'http://dom.ain/'
 * Utils.getBaseUrl('invalid-url')                          // Error
 *
 * @param {!String} url - The given url.
 * @return {String} The base of the given url.
 */
function getBaseUrl(url) {
  if (_.isEmpty(url)) {
    throw new Error('Utils.getBaseUrl(): url can\'t be null, undefined or empty')
  }

  //TODO: use an url parser
  var index = url.lastIndexOf('/');
  if (index < 0) {
    throw new Error('Utils.getBaseUrl(): invalid url "' + url + '"')
  }

  if (url[index - 1] == '/') { // we found an url with only [protocol]://[domain]
    return url + '/'
  }

  return url.substr(0, index + 1)
}

/**
 * Removess all the HTMLElemnt's from its own parents.
 *
 * It uses <code>element.querySelectorAll(selector)</code> to select the elements to remove.
 *
 * @param {!HTMLElement} element - The root element to start the search of the elements to remove.
 * @param {!String} selector - The selector to use to filter elements to remove.
 */
function removeAll(element, selector) {
  if (!element) {
    throw new Error('Utils.removeAll(): element can\'t be null or undefined')
  }

  toArray(element.querySelectorAll(selector))
    .forEach(function (child) { return child.parentElement.removeChild(child); });
}

/**
 * Transform a map of values to an encoded query string. It will always
 * return a string even if the object is invalid.
 *
 * The object can have any structure, but is recommended to be a flat key:value map
 * without any nested structures. The nested objects will not be processed correctly.
 *
 * @param {Object} object - The key/value map to encode
 * @return {String} A string of the form key1=value1&key2=value2&...
 *
 * @see https://tools.ietf.org/html/rfc3986#section-3.4
 */
function querystring(object) {
  if (!_.isObject(object) || _.isEmpty(object) || _.isArray(object)) {
    return ''
  }

  return Object.keys(object)
    .map(function (key) { return encodeURIComponent(key) + '=' + encodeURIComponent(object[key]); })
    .join('&')
}

/**
 * Finds the position of an {@link HTMLElement} on the page.
 *
 * @param {HTMLElement} element - The element to find it's position on the page
 * @return {Object} The position of the element in the page as <code>{left: x, top: y}</code>
 */
function findElementPosition(element) {
  if (!element.getBoundingClientRect || !element.parentNode) {
    return {
      left: 0,
      top: 0
    }
  }

  var box = element.getBoundingClientRect();
  var body = document.body;

  var clientLeft = document.documentElement.clientLeft || body.clientLeft || 0;
  var scrollLeft = window.pageXOffset || body.scrollLeft;
  var left = box.left + scrollLeft - clientLeft;

  var clientTop = document.documentElement.clientTop || body.clientTop || 0;
  var scrollTop = window.pageYOffset || body.scrollTop;
  var top = box.top + scrollTop - clientTop;

  // Android sometimes returns slightly off decimal values, so need to round
  return {
    left: Math.round(left),
    top: Math.round(top)
  }
}

/**
 * Create HTML element
 *
 * @param el - type of HTML element
 * @param elClass - element class name
 * @param id - element id name (optional)
 * @return - HTML element with class and id(optional)
 */
function createEl(el, elClass, id) {
  var tempEl = document.createElement(el);
  tempEl.className = elClass;
  if (id ) {
    tempEl.setAttribute('id', id);
  }
  return tempEl
}

/**
 * Get HTML element by id
 *
 * @param id - element id name
 * @return - HTML element whit provided id
 */
function getElById(id) {
  return document.getElementById(id) 
}

/**
 * Get HTML element by Class
 *
 * @param elClass - element class name
 * @return - HTML element with provided class
 */
function getElByClass(elClass) {
  return document.getElementsByClassName(elClass) 
}

/**
 * Show HTML Element by class name
 *
 * @param el - HTML element 
 * @param className - element class name
 * @return - showed HTML element
 */
function show(el, className) {
  return el.classList.remove(className)
}

/**
 * Hide HTML Element by class name
 *
 * @param el - HTML element 
 * @param className - element class name
 * @return - hided HTML element
 */
function hide(el, className) {
  return el.classList.add(className)
}


var browser = {
  Unknown: 'browser unknown',
  Edge: 'Edge',
  Firefox: 'Firefox',
  Opera: 'Opera',
  IE: 'Internet Explorer',
  Chrome: 'Google Chrome',
  Safari: 'Safari'
};

function getBrowser() {
  var ua = navigator.userAgent;
  if (ua.indexOf(' Edge/') >= 0)    { return browser.Edge }
  if (ua.indexOf(' Firefox/') >= 0) { return browser.Firefox }
  if (ua.indexOf(' OPR/') >= 0)     { return browser.Opera }
  if (ua.indexOf(' Trident/') >= 0) { return browser.IE }
  if (ua.indexOf(' Chrome/') >= 0)  { return browser.Chrome }
  if (ua.indexOf(' Safari/') >= 0)  { return browser.Safari }
  return browser.Unknown
}

var Utils = {
  log: log,
  error: error,
  loadStyle: loadStyle,
  loadScriptAsync: loadScriptAsync,
  loadScript: loadScript,
  loadScripts: loadScripts,
  toArray: toArray,
  bypass: bypass,
  wasPageReloaded: wasPageReloaded,
  get: get,
  post: post,
  getBaseUrl: getBaseUrl,
  removeAll: removeAll,
  querystring: querystring,
  findElementPosition: findElementPosition,
  createEl: createEl,
  getElById: getElById,
  getElByClass: getElByClass,
  show: show,
  hide: hide,

  isEdge:   function () { return getBrowser() === browser.Edge; },
  isIE:     function () { return getBrowser() === browser.IE; },
  isChrome: function () { return getBrowser() === browser.Chrome; }
};

var REFRESH_INTERVAL = 12 * 60 * 1000;   // 12 minutes

var token = null;

function getAuthHeader() {
  return {Authorization: 'Bearer ' + token}
}

function getNewToken(url) {
  return Utils.post(url, getAuthHeader())
    .then(JSON.parse)
    .then(function (response) {
      Utils.log(("Auth.getNewToken(" + url + ") JWT: " + (response.jwt)));
      token = response.jwt;
    })
}

function initialize$1(options) {
  token = options.token;

  // Every REFRESH_INTERVAL (in milliseconds), get a new token and ignore any errors
  var refreshUrl = options.api + '/refresh';
  setInterval(function () { return getNewToken(refreshUrl).catch(Utils.error); }, REFRESH_INTERVAL);

  return token
}

function getToken() {
  return token
}

var Auth = {
  initialize: initialize$1,
  getToken: getToken
};

/**
 * An observable pattern implementation
 *
 * @interface Observable
 */
function Observable() {
  var observers = {};

  function observersFor(event) {
    return (observers[event] = observers[event] || [])
  }

  /**
   * Adds an observer to be notified when the given event is triggered.
   *
   * @memberof Observable
   * @param {String} event - The event to observe.
   * @param {Function} observer - The observer function to call when the event is triggered.
   */
  function on(event, observer) {
    if (typeof event !== 'string') {
      throw new TypeError('event should be a string')
    }

    if (typeof observer !== 'function') {
      throw new TypeError('observer should be a function')
    }

    var list = observersFor(event);
    var index = list.indexOf(observer);
    if (index === -1) {
      list.push(observer);
    }
  }

  /**
   * Adds an observer to be notified only once and then remove itself.
   *
   * @memberof Observable
   * @param {String} event - The event to observe.
   * @param {Function} observer - The observer function to call when the event is triggered.
   */
  function one(event, observer) {
    var self = this;

    function callback(data) {
      self.off(event, callback);
      observer && observer(data);
    }

    self.on(event, callback);
  }

  /**
   * Removes the observer for the given event. After this call the observer will
   * no longer be notified if the event is triggered.
   *
   * @memberof Observable
   * @param {String} event - The event to stop observing.
   * @param {Function} observer - The observer function to stop calling.
   */
  function off(event, observer) {
    if (typeof event !== 'string') {
      throw new TypeError('event should be a string')
    }

    if (typeof observer !== 'function') {
      throw new TypeError('observer should be a function')
    }

    var list = observersFor(event);
    var index = list.indexOf(observer);
    if (index !== -1) {
      list.splice(index, 1);
    }
  }

  /**
   * Notify every registered observer that a given event happend.
   *
   * @memberof Observable
   * @param {String} event - The event to notify.
   * @param {Object} [data] - The object to send to every observer.
   */
  function notify(event, data) {
    if (typeof event !== 'string') {
      throw new TypeError('event should be a string')
    }

    /**
     * traverse a copy of the array for the case 'off'
     * is called by an observer
     */
    [].concat(observersFor(event))
      .forEach(function (observer) { return observer && observer(data); });
  }

  return {
    on: on,
    one: one,
    off: off,
    notify: notify
  }
}

/***
 *  REMAINING THINGS:
 *
 *    - Youbora: on chromecast is sending requests, but in local is still sending pings, do we need to stop the local instance?
 */

var CAST_SENDER_URL = '//www.gstatic.com/cv/js/sender/v1/cast_sender.js';
// const CHROMECAST_APP_ID = '4E839F3A' // shaka demo receiver
var CHROMECAST_APP_ID = '3A9615FD'; // players receiver players.qubit.tv/receiver
// const CHROMECAST_APP_ID = '63501C05' // players dev receiver players.qubit.tv/receiver-dev

var STATUS_CHANGED = 'caststatuschanged';
var READY = 'ready';

/**
 * Chromecast properties
 */
var proxy = null;
var remoteVideo = null;
var remotePlayer = null;

function isCastAvailable() {
  try {
    return !!(window.chrome && window.chrome.cast && window.shaka && window.shaka.cast && window.shaka.cast.CastProxy)
  } catch (e) {
    return false
  }
}

function canCast() {
  try {
    return !!(window.chrome && window.chrome.cast && window.chrome.cast.isAvailable && proxy && proxy.canCast())
  } catch (e) {
    return false
  }
}

function isCasting() {
  try {
    return !!(proxy && proxy.isCasting())
  } catch (e) {
    return false
  }
}

function initialize$2(QB) {
  if (!Utils.isChrome()) {
    Utils.log('Chromecast not compatible with your browser');
    return Promise.resolve()
  }

  var Button = videojs.getComponent('Button');

  var ChromecastButton = videojs.extend(Button, {
    constructor: function (player, options) {
      Utils.log('ChromecastButton()');
      options.label = 'Chromecast';
      Button.call(this, player, options);
      this.el_.title = player.localize('Watch on TV');
      proxy.addEventListener('caststatuschanged', this.onCastStatusChanged.bind(this));

      /**
       * the proxy doesn't fire 'caststatuschanged' if we load the player
       * and we were already casting, so we have to fire it by hand
       */
      if (Chromecast.isCasting()) {
        this.onCastStatusChanged();
      }
    },

    onCastStatusChanged: function () {
      Utils.log('ChromecastButton.onCastStatusChanged()');
      if (!Chromecast.isCastAvailable()) {
        this.hide();
        QB.vjs.removeClass('cast-enabled');
        return
      }

      var isCasting = Chromecast.isCasting();
      if (isCasting) {
        this.addClass('chromecast-connected');
      } else {
        this.removeClass('chromecast-connected');
      }

      Chromecast.notify(STATUS_CHANGED, isCasting);
    },

    buildCSSClass: function () {
      return 'vjs-chromecast-button ' + Button.prototype.buildCSSClass.call(this)
    },

    handleClick: function () {
      Button.prototype.handleClick.call(this);

      var button = this.el()
        , player = this.player()
        , paused = player.paused();

      function onCastSuccess() {
        Utils.log(("Chromecast.onCastSuccess() casting to \"" + (proxy.receiverName()) + "\""));

        var appData = {
          asset: {
            licenseServers: QB.options.widevine
          },
          api: {
            url: QB.options.api,
            token: Auth.getToken(),
            operator: QB.options.operator,
            transaction: QB.options.transaction,
            uuid: QB.options.uuid
          },
          poster: QB.options.poster || '',
          title: QB.options.title || '',
          youbora: QB.options.youbora
        };

        Utils.log('Chromecast.onCastSuccess() setAppData:', appData);
        proxy.setAppData(appData);

        button.disabled = false;
        if (paused) {
          player.pause();
        } else {
          player.play();
        }
      }

      function onCastError(error) {
        button.disabled = false;
        if (error.code !== window.shaka.util.Error.Code.CAST_CANCELED_BY_USER) {
          Utils.log('Chromecast.onCastError():', error);
        }
        player.play();
      }

      if (Chromecast.isCasting()) {
        proxy.suggestDisconnect();
      } else {
        button.disabled = true;
        proxy.cast().then(onCastSuccess, onCastError);
      }
    }
  });

  function waitForChromecastToLoad() {
    Utils.log('Chromecast.waitForChromecastToLoad()');
    var reconnectAttemps = 10;

    return new Promise(function (resolve, reject) {
      function retry() {
        if (Chromecast.isCastAvailable()) {
          Utils.log('Chromecast initialized');
          return resolve()
        }

        if (reconnectAttemps <= 0) {
          return reject(new Error('Cast APIs not available. Max reconnect attempts'))
        }

        Utils.log('Chromecast not available. Trying to reconnect ' + reconnectAttemps + ' more ' + (reconnectAttemps == 1 ? 'time' : 'times'));
        reconnectAttemps -= 1;
        setTimeout(function () { return retry(resolve, reject); }, 1000);
      }

      retry();
    })
  }

  function createProxy() {
    Utils.log('Chromecast.createProxy()');

    if (_.isEmpty(QB.options.widevine)) {
      throw new Error('Chromecast.createProxy() ERROR: Can\'t cast without widevine license server')
    }

    proxy = new window.shaka.cast.CastProxy(QB.video, QB.Player.implementation, CHROMECAST_APP_ID);
    remoteVideo = proxy.getVideo();
    remotePlayer = proxy.getPlayer();
  }

  function showButton() {
    Utils.log('Chromecast.showButton()');
    videojs.registerComponent('ChromecastButton', ChromecastButton);

    function onReady() {
      Utils.log('Chromecast.showButton() QB.Player.READY');
      QB.vjs.addClass('cast-enabled');
      QB.vjs.controlBar.addChild('ChromecastButton');
    }

    if (QB.Player.isReady()) {
      onReady();
    } else {
      QB.Player.one(QB.Player.READY, onReady);
    }
  }

  function syncTime() {
    var onTimeupdate = function () {
      if (Chromecast.isCasting()) {
        QB.vjs.trigger('timeupdate');
      }
    };

    QB.Player.one(QB.Player.READY, function () { return window.videojs.on(remoteVideo, 'timeupdate', onTimeupdate); });
  }

  function notifyReady() {
    Chromecast.notify(Chromecast.READY);
  }

  return new Promise(function (resolve, reject) {
    Utils.loadScript(CAST_SENDER_URL)
      .then(resolve)
      .then(waitForChromecastToLoad)
      .then(createProxy)
      .then(showButton)
      .then(syncTime)
      .then(notifyReady)
      .catch(reject);
  })
}

var Chromecast = Object.create(Observable(), {
  getRemoteVideo: { value: function () { return remoteVideo; } },
  getRemotePlayer: { value: function () { return remotePlayer; } },
  isCastAvailable: { value: isCastAvailable },
  canCast: { value: canCast },
  isCasting: { value: isCasting },
  initialize: { value: initialize$2 },
  STATUS_CHANGED: { value: STATUS_CHANGED },
  READY: { value: READY }
});

/**
 * Ued to save and get the playing time
 *
 * @module lib/continuous_experience
 */

function getUrl(options) {
  return options.api + '/experience'
}

/**
 * It gets the time in wich the video must start playing.
 *
 * @param {!PlayerOptions} options - The player options
 * @return {Promise} A promise that resolves when we got the time
 *                   to start playing. It can be instantaneously.
 */
function getTime(options) {
  return new Promise(function (resolve, reject) {
    if (!options) {
      reject(new Error('ContinuousExperience.getTime(): options can\'t be null or undefined'));
    }

    if (_.isEmpty(options.api)) {
      Utils.log('ContinuousExperience.getTime(): we don\'t have an url configured');
      return reject(new Error('ContinuousExperience.getTime(): we don\'t have an url configured'))
    }

    var data = {
      operator: options.operator,
      uuid: options.uuid,
    };

    var query = Utils.querystring(data);
    var url = getUrl(options) + (query.length ? '?' : '') + query;

    var headers = {
      Authorization: 'Bearer ' + Auth.getToken()
    };

    Utils.get(url, headers)
      .then(JSON.parse)
      .then(function (response) { return resolve(response.time); })
      .catch(reject);
  })
}

/**
 * The last time saved
 *
 * @private
 */
var lastTime = 0;

/**
 * It sets the current time of the video
 *
 * @param {!PlayerOptions} options - The player options.
 * @param {Number} time - The current time in which the video is playing.
 * @param {Number} duration - The total duration of the video.
 * @return {Promise} A promise that resolves when we saved the time.
 */
function setTime(options, time, duration) {
  if (!options) {
    throw new Error('ContinuousExperience.setTime(): options can\'t be null or undefined')
  }

  return new Promise(function (resolve, reject) {
    if (_.isEmpty(options.api)) {
      reject(new Error('ContinuousExperience.setTime(): options.api can\'t be null, undefined or empty'));
    }

    Utils.log('ContinuousExperience.setTime(time:', time, 'duration:', duration, 'lastTime:', lastTime, ')');

    time = Math.round(time);
    if (time && duration && time != lastTime) {
      lastTime = time;

      var data = {
        operator: options.operator,
        uuid: options.uuid,
        repr: options.transaction,
        time: time
      };

      var url = getUrl(options);
      var headers = {
        Authorization: 'Bearer ' + Auth.getToken()
      };

      Utils.post(url, headers, data)
        .then(JSON.parse)
        .then(function (data) {
          Utils.log('ContinuousExperience.setTime() response:', data);
          if (data.success) {   // because REST is overrated (apparently)
            resolve();
          } else {
            reject();
          }
        })
        .catch(reject);
    } else {
      resolve();
    }
  })
}

var ContinuousExperience = {
  getTime: getTime,
  setTime: setTime
};

/**
 * Dependencies, extra scripts to load to make the player work.
 *
 * @module lib/dependencies
 */

var scriptSuffix$1 = (Config.debug ? '.js' : '.min.js');

var dependencies = [
  { /* promises */
    url: '//cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.0/es6-promise' + scriptSuffix$1,
    replace: window.Promise
  },

  { /* lzstring */
    url: '//cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string' + scriptSuffix$1,
    replace: window.LZString
  },

  { /* videojs */
    url: '//vjs.zencdn.net/5.20.1/video.min.js',   // video.js doesn't work, but video.min.js does
    replace: window.videojs
  },

  { /* lodash */
    url: '//cdn.jsdelivr.net/lodash/4.17.4/lodash' + scriptSuffix$1,
    replace: window._
  },

  { /* Object.assign (for IE11) */
    url: '//cdn.jsdelivr.net/npm/object-assign-polyfill@0.1.0/index' + scriptSuffix$1,
    replace: Object.assign
  }
];

/**
 * Load every dependency needed by the player
 *
 * @param {Function} done - A callback to be called when every dependency was loaded
 */
function load(done) {
  var head = document.querySelector('head');
  var remaining = dependencies.length;

  function loaded(url) {
    remaining -= 1;
    Utils.log('loaded', url);
    if (remaining == 0) {
      done();
    }
  }

  function error(url) {
    throw new Error('missing dependency: ' + url)
  }

  function load(dependency) {
    var script = document.createElement('script');

    function clean() {
      script.removeEventListener('load', onLoad);
      script.removeEventListener('error', onError);
    }

    function onLoad() {
      clean();
      loaded(dependency.url);
    }

    function onError() {
      clean();
      error(dependency.url);
    }

    if (typeof dependency.replace != 'undefined') {
      // dependency already loaded
      loaded(dependency.url);
    } else {
      script.addEventListener('load', onLoad);
      script.addEventListener('error', onError);

      script.src = dependency.url;
      head.appendChild(script);
    }
  }

  for (var i = 0, length = dependencies.length; i < length; ++i) {
    load(dependencies[i]);
  }
}

var Dependencies = {
  load: load
};

var es = {
  'captions settings': 'Configuración',
  'Play': 'Reproducir',
  'Audio Track': 'Audio',
  'Pause': 'Pausar',
  'Current Time': 'Tiempo reproducido',
  'Duration Time': 'Duración total',
  'Remaining Time': 'Tiempo restante',
  'Stream Type': 'Tipo de secuencia',
  'LIVE': 'DIRECTO',
  'Loaded': 'Cargado',
  'Progress': 'Progreso',
  'Fullscreen': 'Pantalla completa',
  'Non-Fullscreen': 'Pantalla no completa',
  'Mute': 'Desactivar sonido',
  'Unmute': 'Activar sonido',
  'Playback Rate': 'Velocidad de reproducción',
  'Subtitles': 'Subtítulos',
  'Subtitles off': 'Desactivado',
  'subtitles off': 'Desactivado',
  'Captions': 'Leyendas',
  'captions off': 'Sin Leyendas',
  'Chapters': 'Capítulos',
  'You aborted the media playback': 'Ha interrumpido la reproducción del vídeo.',
  'A network error caused the media download to fail part-way.':
    'Un error de red ha interrumpido la descarga del vídeo.',
  'The media could not be loaded, either because the server or network failed or because the format is not supported.':
    'No se ha podido cargar el vídeo debido a un fallo de red o del servidor o porque el formato es incompatible.',
  'The media playback was aborted due to a corruption problem or because the media used features your browser did not support.':
    'La reproducción de vídeo se ha interrumpido por un problema de corrupción de datos o porque el vídeo precisa funciones que su navegador no ofrece.',
  'No compatible source was found for this media.': 'No se ha encontrado ninguna fuente compatible con este vídeo.',
  'Playing <%=title%> on <%=device%>': 'Reproduciendo en <%=device%>: <%=title%>',
  '<%=title%> paused on <%=device%>': 'En pausa en <%=device%>: <%=title%>',
  'Watch on TV': 'Ver en la TV',
  'Return to home': 'Volver al catálogo',
  'Continue watching': 'Seguí viendo',
  ' seconds': ' segundos',
  'Episode <%=episode%> starts in ': 'Episodio <%=episode%> comienza en ',
  '<%=seconds%> seconds': '<%=seconds%> segundos',
  'View': 'Ver',

  'aa':'Afar',
  'ab':'Abjaso',
  'ae':'Avéstico',
  'af':'Afrikaans',
  'ak':'Akano',
  'am':'Amárico',
  'an':'Aragonés',
  'ar':'Árabe',
  'as':'Asamés',
  'av':'Avar',
  'ay':'Aimara',
  'az':'Azerí',
  'ba':'Baskir',
  'be':'Bielorruso',
  'bg':'Búlgaro',
  'bh':'Bhojpurí',
  'bi':'Bislama',
  'bm':'Bambara',
  'bn':'Bengalí',
  'bo':'Tibetano',
  'br':'Bretón',
  'bs':'Bosnio',
  'ca':'Catalán',
  'ce':'Checheno',
  'ch':'Chamorro',
  'co':'Corso',
  'cr':'Cree',
  'cs':'Checo',
  'cu':'Eslavo eclesiástico antiguo',
  'cv':'Chuvasio',
  'cy':'Galés',
  'da':'Danés',
  'de':'Alemán',
  'dv':'Maldivo',
  'dz':'Dzongkha',
  'ee':'Ewe',
  'el':'Griego (moderno)',
  'en':'Inglés',
  'eo':'Esperanto',
  'es':'Español',
  'et':'Estonio',
  'eu':'Euskera',
  'fa':'Persa',
  'ff':'Fula',
  'fi':'Finés',
  'fj':'Fiyi',
  'fo':'Feroés',
  'fr':'Francés',
  'fy':'Frisón',
  'ga':'Irlandés',
  'gd':'Gaélico escocés',
  'gl':'Gallego',
  'gn':'Guaraní',
  'gu':'Guyaratí',
  'gv':'Manés',
  'ha':'Hausa',
  'he':'Hebreo',
  'hi':'Hindú',
  'ho':'Hiri motu',
  'hr':'Croata',
  'ht':'Haitiano',
  'hu':'Húngaro',
  'hy':'Armenio',
  'hz':'Herero',
  'ia':'Interlingua',
  'id':'Indonesio',
  'ie':'Occidental',
  'ig':'Igbo',
  'ii':'Yi de Sichuán',
  'ik':'Inupiaq',
  'io':'Ido',
  'is':'Islandés',
  'it':'Italiano',
  'iu':'Inuktitut',
  'ja':'Japonés',
  'jv':'Javanés',
  'ka':'Georgiano',
  'kg':'Kongo',
  'ki':'Kikuyu',
  'kj':'Kuanyama',
  'kk':'Kazajo',
  'kl':'Groenlandés',
  'km':'Camboyano',
  'kn':'Canarés',
  'ko':'Coreano',
  'kr':'Kanuri',
  'ks':'Cachemiro',
  'ku':'Kurdo',
  'kv':'Komi',
  'kw':'Córnico',
  'ky':'Kirguís',
  'la':'Latín',
  'lb':'Luxemburgués',
  'lg':'Luganda',
  'li':'Limburgués',
  'ln':'Lingala',
  'lo':'Lao',
  'lt':'Lituano',
  'lu':'Luba-katanga',
  'lv':'Letón',
  'mg':'Malgache',
  'mh':'Marshalés',
  'mi':'Maorí',
  'mk':'Macedonio',
  'ml':'Malayalam',
  'mn':'Mongol',
  'mr':'Maratí',
  'ms':'Malayo',
  'mt':'Maltés',
  'my':'Birmano',
  'na':'Nauruano',
  'nb':'Noruego bokmål',
  'nd':'Ndebele del norte',
  'ne':'Nepalí',
  'ng':'Ndonga',
  'nl':'Holandés',
  'nn':'Nynorsk',
  'no':'Noruego',
  'nr':'Ndebele del sur',
  'nv':'Navajo',
  'ny':'Chichewa',
  'oc':'Occitano',
  'oj':'Ojibwa',
  'om':'Oromo',
  'or':'Oriya',
  'os':'Osético',
  'pa':'Panyabí',
  'pi':'Pali',
  'pl':'Polaco',
  'ps':'Pastú',
  'pt':'Portugués',
  'qu':'Quechua',
  'rm':'Romanche',
  'rn':'Kirundi',
  'ro':'Rumano',
  'ru':'Ruso',
  'rw':'Ruandés',
  'sa':'Sánscrito',
  'sc':'Sardo',
  'sd':'Sindhi',
  'se':'Sami septentrional',
  'sg':'Sango',
  'si':'Cingalés',
  'sk':'Eslovaco',
  'sl':'Esloveno',
  'sm':'Samoano',
  'sn':'Shona',
  'so':'Somalí',
  'sq':'Albanés',
  'sr':'Serbio',
  'ss':'Suazi',
  'st':'Sesotho',
  'su':'Sundanés',
  'sv':'Sueco',
  'sw':'Suajili',
  'ta':'Tamil',
  'te':'Telugú',
  'tg':'Tayiko',
  'th':'Tailandés',
  'ti':'Tigriña',
  'tk':'Turcomano',
  'tl':'Tagalo',
  'tn':'Setsuana',
  'to':'Tongano',
  'tr':'Turco',
  'ts':'Tsonga',
  'tt':'Tártaro',
  'tw':'Twi',
  'ty':'Tahitiano',
  'ug':'Uigur',
  'uk':'Ucraniano',
  'ur':'Urdu',
  'uz':'Uzbeko',
  've':'Venda',
  'vi':'Vietnamita',
  'vo':'Volapük',
  'wa':'Valón',
  'wo':'Wolof',
  'xh':'Xhosa',
  'yi':'Yídish',
  'yo':'Yoruba',
  'za':'Chuan',
  'zh':'Chino',
  'zu':'Zulú'
};

var languages$1 = {
  'es': es
  // add more languages here
};

var Languages = {
  load: function () {
    Object.keys(languages$1)
      .forEach(function (language) { return videojs.addLanguage(language, languages$1[language]); });
  }
};

/**
 * Player options
 *
 * @module lib/options
 */

/**
 * It transforms an uri hash into a {@link PlayerOptions} object
 *
 * @param {String} hash - The url hash <i>("#....")</i> as an encoded uri component
 *                        that can be transformed into a JSON.
 * @return {Promise} A promise that resolves to an object with
 *                   all options encoded in the hash. The object
 *                   is an instance of {@link PlayerOptions}
 */
function fromUriHash(hash) {
  return new Promise(function (resolve, reject) {
    if (_.size(hash) < 2) {
      return reject(new Error('There is no hash to load'))
    }

    resolve(fromString(LZString.decompressFromEncodedURIComponent(hash.substr(1))));
  })
}

/**
 * It transforms a json string into a {@link PlayerOptions} object
 *
 * @param {String} jsonString - The json string representation of a {@link PlayerOptions} object.
 * @return {PlayerOptions} - The decoded object with defaults already filled in.
 */
function fromString(jsonString) {
  var options = JSON.parse(jsonString);

  if (_.isEmpty(options)) {
    throw new Error('There is no options to load')
  }

  if (_.isEmpty(options.source)) {
    throw new Error('There is no source to load')
  }

  if (!_.isArray(options.source) || options.source.length == 0) {
    throw new Error('options.source must be an array of source objects with at least one object')
  }

  if (!options.operator) {
    options.operator = 'qubit';
  }

  if (!options.transaction) {
    options.transaction = 0;
  } else {
    options.transaction = parseInt(options.transaction);
  }

  if (!options.user) {
    options.user = '';
  }

  if (!options.link) {
    options.link = 'javascript:history.back()';
  }

  if (!options.audioLanguage) {
    options.audioLanguage = options.language || document.documentElement.lang || 'en';
  }

  if (!options.subtitleLanguage) {
    options.subtitleLanguage = null;   // no subtitles
  }

  if (options.token) {
    options.token = options.token.trim();
    // options.proxyHeaders = options.proxyHeaders || {}
    // options.proxyHeaders.Authorization = 'Bearer ' + options.token
    // options.proxyHeaders['Content-Type'] = 'application/octet-stream'
  }

  if (!options.youbora) {
    options.youbora = {
      accountCode: options.operator,
      media: {
        isLive: false,    //TODO: get as parameter
        title: options.title,
        resource: getSourceUrl(options)
      },
      operator: 'AR', //TODO get as parameter
      playerType: 'shaka',
      transactionCode: options.transaction,
      username: options.user,
      extraParams: {
        param1: options.operator
      }
    };
  }

  return options
}

/**
 * Given a {@link PlayerOptions} and an (optional) language it returns the url of the source to load.
 *
 * @param {PlayerOptions} options - The player options.
 * @param {String} [language] - The language to load or nothing if you want the first source available.
 * @return {String} - The url of the given source
 */
function getSourceUrl(options, language) {
  if (_.isEmpty(options)) {
    throw new Error('options can\'t be empty')
  }

  var sources = options.source;
  if (!_.isArray(sources) || sources.length == 0) {
    throw new Error('options.source must be an array of source objects with at least one object')
  }

  for (var i = 0, length = sources.length; i < length; ++i) {
    if (sources[i].lang === language) {
      return sources[i].url
    }
  }

  return sources[0].url
}

var Options = {
  fromUriHash: fromUriHash,
  fromString: fromString,
  getSourceUrl: getSourceUrl
};

var Thumbnails = {
  initialize: function (QB) {
    var Component = videojs.getComponent('Component')
      , GRID_CELLS_WIDTH = 10
      , GRID_CELLS_HEIGHT = 10
      , GRID_WIDTH = 2400           // pixels
      , GRID_HEIGHT = 1360          // pixels
      , TILE_WIDTH = GRID_WIDTH / GRID_CELLS_WIDTH
      , TILE_HEIGHT = GRID_HEIGHT / GRID_CELLS_HEIGHT
      , THUMBNAIL_INTERVAL = 240 / 101;    // 101 images every 4 minutes (240 seconds)

    function toGridPoint(i) {
      return {
        x: i % GRID_CELLS_WIDTH,
        y: Math.floor(i / GRID_CELLS_WIDTH)
      }
    }

    function setImagePosition(image, index) {
      var point = toGridPoint(index)
        , top = TILE_HEIGHT * point.y
        , left = TILE_WIDTH * point.x;

      image.style.top = '-' + (top + TILE_HEIGHT) + 'px';  // the "+ TILE_HEIGHT" if because of the loading image
      image.style.left = '-' + left + 'px';
    }

    function getId(url) {
      return 'id' + url.split('').filter(function (c) { return _.includes('0123456789', c); }).join('')
    }

    var ThumbnailDisplay = videojs.extend(Component, {
      constructor: function (player, options) {
        var this$1 = this;

        Component.call(this, player, options);

        this.standalone = options.standalone;
        this.urlTemplate = options.url;
        this.index = 1;

        if (this.standalone) {
          this.addClass('vjs-thumbnail-holder-standalone');
        }

        player.ready(function () { return this$1.on(player.controlBar.progressControl.el(), 'mousemove', _.throttle(this$1.update.bind(this$1), 25)); });

        this.loading = document.createElement('div');
        this.loading.className = 'vjs-thumbnail-loading';
        this.el().appendChild(this.loading);
      },

      createEl: function () {
        return Component.prototype.createEl.call(this, 'div', {
          className: 'vjs-thumbnail-holder'
        })
      },

      url: function (index) {
        var imageIndex = 1 + Math.floor(index / (GRID_CELLS_WIDTH * GRID_CELLS_HEIGHT));
        return this.urlTemplate.replace('%s', ('000' + imageIndex).slice(-3))
      },

      update: function (event) {
        var player = this.player()
          , duration = player.duration()
          , thumbnailCount = duration / THUMBNAIL_INTERVAL;

        if (!event || thumbnailCount == 0) {
          return
        }

        if (this.standalone && event) {
          var position = event.pageX - Utils.findElementPosition(this.el().parentNode).left
            , offset = this.el().getBoundingClientRect().width / 2;
          this.el().style.left = (position - offset) + 'px';
        }

        var seekBar = _.get(player, 'controlBar.progressControl.seekBar')
          , mouseTime = seekBar.calculateDistance(event) * duration;

        this.index = Math.floor(mouseTime / THUMBNAIL_INTERVAL);

        if (this.index != this.lastIndex) {
          this.setCurrentThumbnail(this.index);
          this.lastIndex = this.index;
        }
      },

      setCurrentThumbnail: function (index) {
        var url = this.url(index)
          , id = getId(url)
          , image = this.getImage(this.el(), id)
          , isLoading = (image.getAttribute('data-loading') === 'true');

        if (this.currentThumbnailId) {
          document.getElementById(this.currentThumbnailId).style.display = 'none';
        }

        this.currentThumbnailId = image.id;

        if (image.src != url) {
          image.src = url;
        }

        if (isLoading) {
          image.style.display = 'none';
          this.loading.style.display = 'block';
        } else {
          image.style.display = 'block';
          this.loading.style.display = 'none';
          setImagePosition(image, index % (GRID_CELLS_WIDTH * GRID_CELLS_HEIGHT));
        }
      },

      getImage: function (element, id) {
        var image = document.getElementById(id);

        if (!image) {
          image = document.createElement('img');
          image.id = id;
          image.className = 'vjs-thumbnail';
          image.setAttribute('data-loading', true);
          image.style.display = 'none';
          image.onload = this.imageLoaded.bind(this, image);
          element.appendChild(image);
        }

        return image
      },

      imageLoaded: function (image) {
        image.setAttribute('data-loading', false);
        image.onload = null;
        if (this.currentThumbnailId == image.id) {
          image.style.display = 'block';
          this.loading.style.display = 'none';
        }
      }
    });

    Component.registerComponent('ThumbnailDisplay', ThumbnailDisplay);

    videojs.plugin('thumbnails', function (options) {
      var parent = this.controlBar.progressControl.seekBar
        , hasMouseTimeDisplay = _.get(options.vjs, 'controlBar.progressControl.seekBar.mouseTimeDisplay');

      if (!_.isBoolean(hasMouseTimeDisplay)) {
        hasMouseTimeDisplay = true;    // videojs has mouse display by default
      }

      if (hasMouseTimeDisplay) {
        parent = parent.mouseTimeDisplay;
      }

      parent.addChild('ThumbnailDisplay', {
        standalone: !hasMouseTimeDisplay,
        url: options.thumbs
      });
    });

    QB.vjs.thumbnails(QB.options);
  }
};

var Related = { 
  initialize: function (QB) {
    var registerPlugin = videojs.registerPlugin || videojs.plugin;
    var suggestedVideoEndcap = function(options) {
      var player = this;
      var creditsBegin = options.creditsBegin;
      var timerId;
      var content;
      var timeoutLoop;

      // Process related or nextEpisode based in contentType 
      processByContentType(options);
      
      function processByContentType(options) {
        // Log when serie_chapters_tree return empty from api Play call to apiVOD content/detail 
        if ( options.contentType == 'Serie' && _.isEmpty(options.serieUid) ) {
          Utils.error('*** SERIE UID NOT FOUND *** RECOMM CANCELED ***');
          return true
        } 
        if (options.contentType != 'Serie') {
          return createScreenRelated(options)
        }
        return creditsBeginLoop()
      } 
      
      function creditsBeginLoop(){
        timeoutLoop = setTimeout(askForEndCredits, 15000);
        return timeoutLoop
      }

      function createScreenRelated(options) {
        relatedContent(options).then( function (data) {
          return createRelatedBase(options, data) 
        }).then( function () {
          if (options.contentType == 'Serie') {
            timerId = timer(content);
            showRelated();
          }
        });
      }
      
      function relatedContent(options) {
        if (!options) {
          throw new Error('related.relatedContent(): options can\'t be null or undefined')
        }
        
        return new Promise( function (resolve, reject) {
          var data = {
            operator: options.operator,
            uuid: options.uuid,
            fieldset: 'all',
            contentType: options.contentType
          };
          
          if ( options.contentType == 'Serie') {
            data.serieUid = options.serieUid;
          }
          
          var headers = {
            Authorization: 'Bearer ' + Auth.getToken()
          };
          
          var url = options.api + '/recomm';
          Utils.post( url, headers, data)
            .then(JSON.parse)
            .then(function (data) {
              if (data.recomm.length > 0) {
                return resolve(data)
              } else {
                return reject()
              }
            })
            .catch(reject);
        })
      }
    
      function timer(content) {
        clearTimeout(timeoutLoop);
        var seconds = 20;
        var stringSec = player.localize(' seconds');
        var timerId = setInterval( function() {
          Utils.getElById('epTimer').innerHTML = seconds + stringSec;
          seconds--;
          if (seconds == 0) {
            hideRelated();
            clearInterval(timerId);           
            playContent(options, content);
          }
        },1000);
        return timerId
      }
      
      function createRelatedBase(options, data) {
        var _frag = document.createDocumentFragment();
        var _aside = Utils.createEl('aside', 'related-screen nodisplay', 'related_screen');
        asideEventListener(_aside); 
        
        var _relatedClose = Utils.createEl('div', 'related-close-container');
        var _closeRelatedButton = Utils.createEl('a', 'big-related-round-button', 'close');
        _closeRelatedButton.innerHTML = 'X';
        _relatedClose.appendChild(_closeRelatedButton);
        
        var _relatedHeader = Utils.createEl('div', 'related-header');
        _relatedHeader.innerHTML = player.localize('Continue watching');
        
        var _relatedScreen = createRelatedScreen(data);
        
        var _relatedFooter = Utils.createEl('a', 'related-footer'); 
        _relatedFooter.innerHTML = player.localize('Return to home');
        _relatedFooter.href = options.homeCatalog;
        _relatedFooter.target = '_self';

        _aside.appendChild(_relatedClose);
        _aside.appendChild(_relatedHeader);
        _aside.appendChild(_relatedScreen);
        _aside.appendChild(_relatedFooter);

        _frag.appendChild(_aside);
        player.el().appendChild(_frag);
        
        creditsBeginLoop();
      }
      
      function asideEventListener(_aside) {
        _aside.addEventListener('click', function(event) {
          
          if ( event.target.id == 'close') {
            if (options.contentType == 'Serie') {
              clearInterval(timerId);
            }
            Utils.hide(_aside, 'nodisplay');
            player.play();
          }
          
          if (event.target.id == 'posterImage1') {
            var _poster2 = Utils.getElById('posterImage0');
            var outline = _poster2.style.outline;
            if ( outline != 'unset') {
              _poster2.style.outline = 'unset';
              toggleVisibility('detailsRight', event.target.id);
            }
            toggleVisibility('detailsLeft', event.target.id);
          }
          
          if (event.target.id == 'posterImage0') {
            var _poster1 = Utils.getElById('posterImage1');
            var outline$1 = _poster1.style.outline;
            if (outline$1 != 'unset') {
              _poster1.style.outline = 'unset';
              toggleVisibility('detailsLeft', event.target.id);
            }
            toggleVisibility('detailsRight', event.target.id);
          }
        }, false);
      
      }
      
      function toggleVisibility(detailsId, posterId) {
        var _details = Utils.getElById(detailsId); 
        var _poster = Utils.getElById(posterId); 

        if (_details) {
          var visibility = _details.style.visibility;
          if ( visibility == 'hidden') {
            _poster.style.outline = '1px solid #ff6d00';
            return _details.style.visibility = 'visible' 
          } else {
            _poster.style.outline = 'unset';
            return _details.style.visibility = 'hidden' 
          }
        }
      }

      function createEpisodeHeader(episode) {
        var _nextEpisodeTimer = Utils.createEl('span', 'episode-timer', 'epTimer');
        _nextEpisodeTimer.innerHTML = _.template(player.localize('<%=seconds%> seconds'))({seconds: 20});
        var _divEpisodeHeader = Utils.createEl('div', 'episode-header', 'epHeader');
        var _nextEpisodeTitle = Utils.createEl('span', 'episode-title', 'epTitle');
        _nextEpisodeTitle.innerHTML = _.template(player.localize('Episode <%=episode%> starts in '))({episode: episode});
        _nextEpisodeTitle.appendChild(_nextEpisodeTimer);
        _divEpisodeHeader.appendChild(_nextEpisodeTitle);
        
        return _divEpisodeHeader 
      }
      
      function createRelatedScreen(data) {
        if (options.contentType == 'Serie') {
          return createNextEpisode(data)
        }
        return createRelatedMovies(data) 
      }
      
      function createNextEpisode(data) {
        var episode = data.detail.Content.episode;
        content = data.recomm; 
        var _divEpHeader = createEpisodeHeader(episode, content);
        var _divNextEpisode = Utils.createEl('div', 'next-episode', 'epToPlay');
        var _divSeriePoster = Utils.createEl('div', 'serie-poster-container', 'episodePoster');
        var _img = Utils.createEl('img', 'serie-poster-img outline-active', 'posterImage');
        _img.src = data.detail.Content.metadata.img_large;
        _img.addEventListener('click', function () { return playContent(options, content, timerId); });
        
        _divNextEpisode.appendChild(_divEpHeader);
        _divSeriePoster.appendChild(_img);
        
        _divNextEpisode.appendChild(_divSeriePoster);
        
        return _divNextEpisode
      }
      
      function createRelatedMovies(data) {
        var sugs = data.recomm;
        var  i = sugs.length - 1 > 1 ? 1 : sugs.length - 1;
        
        var _divRelated = Utils.createEl('div', 'related-gallery', 'relatedMovies');
        var _divDetailsLeft = Utils.createEl('div', 'related-media-details related-margin-right', 'detailsLeft');
        var _h1TitleLeft = Utils.createEl('h1');
        _h1TitleLeft.innerHTML = sugs[1].title;
        _divDetailsLeft.appendChild(_h1TitleLeft);
        // plot detail text
        var _h3PlotLeft = Utils.createEl('h3');
        _h3PlotLeft.innerHTML = sugs[1].short_summary;
        _divDetailsLeft.appendChild(_h3PlotLeft);
        _divDetailsLeft.style.visibility = 'hidden'; 
        // row with action buttons
        var _rowActionsLeft = Utils.createEl('div', 'related-row');
        var _addToListLeft = Utils.createEl('a', 'btn related-round-button inactive');
        // action play button 
        var _playButtonLeft = Utils.createEl('a', 'btn related-play-button');
        _playButtonLeft.addEventListener('click', function () { return playContent(options, sugs[1].uuid); });
        _playButtonLeft.target = '_self';
        var _playIcon = Utils.createEl('i');
        _playIcon.innerHTML = player.localize('View');
        // action favorite button
        var _favIconLeft = Utils.createEl('i', 'not-fa', 'iconLeft');
        if ( sugs[1].favorite) {
          _favIconLeft.className = 'yes-fa';
          _addToListLeft.classList.remove('inactive');
          _addToListLeft.classList.add('active');
        }
        _addToListLeft.addEventListener('click', function() {
          // change icon and color
          _favIconLeft = toggleClassFav(_favIconLeft);
          _addToListLeft = toggleColorFavorite(_addToListLeft);
          // post to change favorite value 
          postFavorite(sugs[1], options);
        }, false);
        
        _playButtonLeft.appendChild(_playIcon);
        _addToListLeft.appendChild(_favIconLeft);
        _rowActionsLeft.appendChild(_playButtonLeft);
        _rowActionsLeft.appendChild(_addToListLeft);
        
        _divDetailsLeft.appendChild(_rowActionsLeft);
        _divRelated.appendChild(_divDetailsLeft);

        // construct the individual suggested content pieces
        for (; i >= 0; --i) {
          var _divPoster = Utils.createEl('div', 'related-poster-container', 'poster'+i);

          var _img = Utils.createEl('img', 'related-poster-img', 'posterImage'+i);

          _img.src = sugs[i].metadata.img_large;
          _img.style.outline = 'unset';
          
          _divPoster.appendChild(_img);
          _divRelated.appendChild(_divPoster);

        }
       
        var _divDetailsRight = Utils.createEl('div', 'related-media-details related-margin-left', 'detailsRight');
        var _h1TitleRight = Utils.createEl('h1');
        _h1TitleRight.innerHTML = sugs[0].title;

        _divDetailsRight.appendChild(_h1TitleRight);
        
        var _h3PlotRight = Utils.createEl('h3');
        _h3PlotRight.innerHTML = sugs[0].short_summary;
        _divDetailsRight.appendChild(_h3PlotRight);
        _divDetailsRight.style.visibility = 'hidden'; 
        
        //create row with action buttons
        var _rowActionsRight = Utils.createEl('div', 'related-row');
        var _addToListRight = Utils.createEl('a', 'btn related-round-button inactive');
        var _playButtonRight = Utils.createEl('a', 'btn related-play-button');
        _playButtonRight.addEventListener('click', function () { return playContent(options, sugs[0].uuid); });
        _playButtonRight.target = '_self';
        var _playIconRight = Utils.createEl('i');
        _playIconRight.innerHTML = player.localize('View');
        var _favIconRight = Utils.createEl('i', 'not-fa', 'iconRight');
        
        if ( sugs[0].favorite) {
          _favIconRight.className = 'yes-fa';
          _addToListRight.classList.remove('inactive');
          _addToListRight.classList.add('active');
        }
        
        _addToListRight.addEventListener('click', function() {
          // change icon and color
          _favIconRight = toggleClassFav(_favIconRight);
          _addToListRight = toggleColorFavorite(_addToListRight);
          // post to change favorite value 
          postFavorite(sugs[0], options);
        }, false);
        
        _playButtonRight.appendChild(_playIconRight);
        _addToListRight.appendChild(_favIconRight);
        _rowActionsRight.appendChild(_playButtonRight);
        _rowActionsRight.appendChild(_addToListRight);
        
        _divDetailsRight.appendChild(_rowActionsRight);
        _divRelated.appendChild(_divDetailsRight);

        
        return _divRelated
      }     
      
      function toggleColorFavorite(el) {
        if ( el.classList.contains('inactive')) {
          el.classList.remove('inactive');
          el.classList.add('active');
          return el
        }
        el.classList.remove('active');
        el.classList.add('inactive');
        return el
      }
      
      function showRelated() {
        var relatedAside = Utils.getElByClass('related-screen')[0];
        Utils.show(relatedAside, 'nodisplay');
      }
      
      function hideRelated() {
        var relatedAside = Utils.getElByClass('related-screen')[0];
        Utils.hide(relatedAside, 'nodisplay');
      }
      
      function askForEndCredits() {
        var currentTime = QB.video.currentTime;
        if (creditsBegin == 0) {
          creditsBegin = QB.video.duration - 20;
        }
        
        // content is serie and credits begin
        if ( currentTime >= creditsBegin && options.contentType == 'Serie') {
          return createScreenRelated(options)
        }
        //content is Pelicula and credits Begin
        if (currentTime >= creditsBegin && options.contentType != 'Serie') {
          return showRelated()
        }
        // credits is not Began  
        return creditsBeginLoop()
      }
      
      function toggleClassFav(el) {
        if (el.className == 'yes-fa') {
          el.className = 'not-fa';
          return el
        }
        el.className = 'yes-fa';
        return el
      }
      
      function postFavorite(related, options) {
        return new Promise( function (resolve, reject) {
          var data = {
            operator: options.operator,
            uuid: related.uuid
          };

          var headers = {
            Authorization: 'Bearer ' + Auth.getToken()
          };
          
          var url = options.api + '/favorite';
          Utils.post( url, headers, data)
            .then(JSON.parse)
            .then(function (data) {
              if (!data) {
                return reject()
              }
              return resolve(data)
            })
            .catch(reject);
         
        })
      } 
      // Show related on pause
      /*player.on('pause', function() {
        if (options.contentType != 'Serie') {
          showRelated()
        }
      })

      player.on('play', function() {
        if (options.contentType != 'Serie') {
          hideRelated()
        }
      })*/
    };

    function playContent(options, uuid, timerId) {
      if (options.contentType == 'Serie') {
        clearInterval(timerId);           
      }
      var headers = {
        Authorization: 'Bearer ' + Auth.getToken()
      };

      var data = {
        operator: options.operator,
        quality: options.quality,
        language: options.language,
        stream: options.stream,
        username: options.user,
        uuid: uuid
      };

      var url = options.api + '/content';
      showLoadingScreen();
      Utils.post(url, headers, data)
        .then(JSON.parse)
        .then(function (data) {
          if (data.embedUrl) {
            window.location.href = data.embedUrl;
            window.location.reload(true);
          } else {
            //TODO: improve error handling
            Utils.error('no embed url on /content response');
            hideLoadingScreen();
          }
        })
        .catch(function (err) {
          //TODO: improve error handling
          Utils.error(err);
          hideLoadingScreen();
        });
    }

    function showLoadingScreen() {
      var loading = Utils.createEl('div', 'loading');

      var overlay = Utils.createEl('div', 'related-overlay', 'related_loading');
      overlay.appendChild(loading);

      var body = document.querySelector('body');
      body.appendChild(overlay);
    }

    function hideLoadingScreen() {
      var element = document.getElementById('related_loading');
      if (element) {
        var body = document.querySelector('body');
        body.removeChild(element);
      }
    }

    registerPlugin('suggestedVideoEndcap', suggestedVideoEndcap);
    QB.vjs.suggestedVideoEndcap(QB.options);
  }
};

var AudioMenu = function (QB, videojs, audio) {
  var MenuItem = videojs.getComponent('MenuItem')
    , Component = videojs.getComponent('Component')
    , AudioTrackButton = videojs.getComponent('AudioTrackButton');

  var QubitAudioTrackButton = videojs.extend(AudioTrackButton, {
    constructor: function (player, options) {
      Utils.log('QubitAudioTrackButton()');
      options.label = options.lang;
      AudioTrackButton.call(this, player, options);
    },

    createItems: function () {
      Utils.log('QubitAudioTrackButton.createItems(), languages:', audio.languages());
      var player = this.player();
      var languages = audio.languages();
      if (languages.length < 2) {
        return []
      }
      return languages.map(function (language) { return new QubitAudioTrackMenuItem(player, {
          language: language,
          selectable: true
        }); }
      )
    }
  });

  Component.registerComponent('AudioTrackButton', QubitAudioTrackButton);

  var QubitAudioTrackMenuItem = videojs.extend(MenuItem, {
    constructor: function(player, options) {
      var this$1 = this;

      this.language = options.language;
      Utils.log('QubitAudioTrackMenuItem(), language:', this.language);
      options.label = this.language || 'Unknown';
      options.selected = (audio.currentLanguage() == this.language);

      MenuItem.call(this, player, options);

      audio.on(audio.CHANGED, function (language) { return this$1.selected(this$1.language == language); });
    },

    handleClick: function (event) {
      Utils.log('QubitAudioTrackMenuItem.handleClick(), language:', this.language);
      MenuItem.prototype.handleClick.call(this, event);
      audio.selectLanguage(this.language);
    }
  });

  QB.Player.on(QB.Player.LOADED, function () {
    Utils.log('AudioMenu.initializeAudio() QB.Player.LOADED');
    audio.initialize(QB.options.audioLanguage);
  });
};

var SubtitlesMenu = function (QB, videojs, subtitles) {
  var MenuItem = videojs.getComponent('MenuItem')
    , Component = videojs.getComponent('Component')
    , MenuButton = videojs.getComponent('MenuButton');

  var QubitSubtitlesButton = videojs.extend(MenuButton, {
    constructor: function (player, options) {
      options.label = options.lang;
      MenuButton.call(this, player, options);
    },

    buildCSSClass: function () {
      return 'vjs-subtitles-button ' + MenuButton.prototype.buildCSSClass.call(this)
    },

    createItems: function () {
      var player = this.player();
      var langs = subtitles.languages();
      Utils.log('QubitSubtitlesMenuItem.createItems() langs:', langs);
      // position 0 -> subtitles off
      if (langs.length < 2) {
        return []
      }
      return subtitles.languages().map(function (language) { return new QubitSubtitlesMenuItem(player, {
          language: language,
          selectable: true
        }); }
      )
    }
  });

  Component.registerComponent('SubtitlesButton', QubitSubtitlesButton);

  var QubitSubtitlesMenuItem = videojs.extend(MenuItem, {
    constructor: function(player, options) {
      var this$1 = this;

      this.language = options.language;
      options.label = this.language || 'Unknown';
      options.selected = subtitles.currentLanguage() == this.language;

      MenuItem.call(this, player, options);

      subtitles.on(subtitles.CHANGED, function (language) {
        Utils.log('QubitSubtitlesMenuItem subtitles.CHANGED to', language);
        this$1.selected(this$1.language == language);
        updateTracks(language);
      });
    },

    handleClick: function (event) {
      MenuItem.prototype.handleClick.call(this, event);
      var language = this.language;
      subtitles.selectLanguage(language);
    }
  });

  Chromecast.on(Chromecast.STATUS_CHANGED, function () { return updateTracks(subtitles.currentLanguage()); });

  function updateTracks(language) {
    Utils.log(("SubtitlesButton updateTracks(" + language + ") casting? " + (Chromecast.isCasting())));
    var mode = Chromecast.isCasting() ? 'disabled' : 'showing';
    Utils.toArray(QB.vjs.textTracks()).forEach(function (track) { return track.mode = ((track.language == language) ? mode : 'disabled'); });
  }

  QubitSubtitlesButton.prototype.controlText_ = 'Subtitles';

  var subtitlesAvailable = QB.options.subtitles.length;

  if (subtitlesAvailable == 0) {
    Utils.log('SubtitlesMenu.initializeSubtitles(): no subtitles, no subtitles button');
  } else {
    Utils.log(("SubtitlesMenu.initializeSubtitles(): " + subtitlesAvailable + " languages, showing subtitles button"));
    Utils.removeAll(QB.video, 'track');
    QB.options.subtitles.forEach(function (subtitle) {
      Utils.log('SubtitlesMenu.initializeSubtitles() adding', JSON.stringify(subtitle));
      var track = document.createElement('track');
      track.setAttribute('src', subtitle.url);
      track.setAttribute('srclang', subtitle.lang);
      track.setAttribute('lang', subtitle.lang);
      track.setAttribute('label', subtitle.lang);
      track.setAttribute('type', 'subtitles');
      track.setAttribute('kind', 'subtitles');
      QB.video.appendChild(track);
    });
  }

  QB.Player.on(QB.Player.LOADED, function () {
    Utils.log('SubtitlesMenu.initializeSubtitles() QB.Player.LOADED');
    subtitles.initialize(QB.options.subtitleLanguage);
  });
};

var BackSecondsButton = function (QB, videojs, seconds) {
  var Component = videojs.getComponent('Component')
    , MenuButton = Component.getComponent('MenuButton');

  var BackSecondsButton = videojs.extend(MenuButton, {
    constructor: function (player, options) {
      Utils.log('BackSecondsButton()');
      MenuButton.call(this, player, options);
      this.seconds_ = options.seconds;
      this.el().title = "-" + (this.seconds_) + "\"";
    },

    buildCSSClass: function () {
      return 'vjs-icon-replay ' + MenuButton.prototype.buildCSSClass.call(this)
    },

    handleClick: function (event) {
      Utils.log('BackSecondsButton.handleClick()');
      MenuButton.prototype.handleClick.call(this, event);
      var player = this.player();
      player.currentTime(player.currentTime() - this.seconds_);
    }
  });

  Component.registerComponent('BackSecondsButton', BackSecondsButton);

  QB.Player.on(QB.Player.LOADED, function () {
    Utils.log('BackSecondsButton() QB.Player.LOADED');
    QB.vjs.controlBar.addChild('BackSecondsButton', {
      seconds: seconds
    });
  });
};

var QubitTech = function (QB, videojs, QubitPlayer, Chromecast) {
  var Html5 = videojs.getComponent('Html5');

  function getVideo() {
    if (Chromecast.canCast()) {
      return Chromecast.getRemoteVideo()
    }

    return QB.video
  }

  var QubitTech = videojs.extend(Html5, {
    constructor: function (options, ready) {
      options.el = QB.video;

      Html5.call(this, options, ready);

      this.castVolume_ = _.debounce(function (level, muted) {
        getVideo().volume = muted ? 0 : level;
        getVideo().muted = muted;
      }.bind(this), 200);

      this.castCurrentTime_ = _.debounce(function (time) {
        getVideo().currentTime = time;
      }.bind(this), 200);

      this.player().on('playing', function () {
        Utils.log('QubitTech() player is playing');
        if (options.autoplay) {
          this.play();
        }
      }.bind(this));

      QubitPlayer.on(QubitPlayer.LOADED, function () {
        Utils.log('QubitTech() player is loaded');
        if (Chromecast.isCasting()) {
          this.player().trigger('playing');
        }
      }.bind(this));

      /**
       * This is a relatively small hack for youbora videojs plugin.
       * It's plugin expect videojs instance to have a tech available
       * and given tech must have one of the following properties:
       *
       *  "hls" when using videojs-contrib-hls
       *  "hls_" when using hls.js
       *  "shakaPlayer" when using Shaka Player
       *
       * So every QubitPlayer must have a name and an implementation to set
       * this up
       */
      this[QubitPlayer.name] = QubitPlayer.implementation;
    },

    name: function () {
      return 'videojs-qubit-tech'
    },

    play: function () {
      Utils.log('QubitTech.play() casting?', Chromecast.isCasting());
      this.paused_ = false;
      if (Chromecast.isCasting()) {
        getVideo().play();
        this.trigger('play');
      } else {
        Html5.prototype.play.call(this);
      }
    },

    pause: function () {
      Utils.log('QubitTech.pause() casting?', Chromecast.isCasting());
      this.paused_ = true;
      if (Chromecast.isCasting()) {
        getVideo().pause();
        this.trigger('pause');
      } else {
        Html5.prototype.pause.call(this);
      }
    },

    paused: function () {
      return this.paused_
    },

    currentTime: function () {
      if (getVideo().currentTime == 0) {
        return this.currentTime_ || 0
      }
      return this.currentTime_ = getVideo().currentTime
    },

    setCurrentTime: function (time) {
      Utils.log(("QubitTech.setCurrentTime(" + time + ") casting?"), Chromecast.isCasting());
      if (Chromecast.isCasting()) {
        this.currentTime_ = time;
        this.castCurrentTime_(this.currentTime_);
        this.trigger('timeupdate');
      } else {
        Html5.prototype.setCurrentTime.call(this, time);
      }
    },

    volume: function () {
      return this.volume_
    },

    setVolume: function (level, muted) {
      Utils.log(("QubitTech.setVolume(" + level + ", " + muted + ") casting?"), Chromecast.isCasting());
      this.volume_ = level;
      this.muted_ = muted;
      if (Chromecast.isCasting()) {
        this.castVolume_(this.volume_, this.muted_);
        this.trigger('volumechange');
      } else {
        Html5.prototype.setVolume.call(this, level, muted);
      }
    },

    muted: function () {
      return this.muted_
    },

    setMuted: function (muted) {
      Utils.log(("QubitTech.setMuted(" + muted + ") casting?"), Chromecast.isCasting());
      this.muted_ = muted;
      if (Chromecast.isCasting()) {
        this.castVolume_(this.volume_, this.muted_);
        this.trigger('volumechange');
      } else {
        Html5.prototype.setMuted.call(this, muted);
      }
    },

    duration: function () {
      Utils.log('QubitTech.duration() casting?', Chromecast.isCasting());
      if (Chromecast.isCasting()) {
        return getVideo().duration
      }

      return Html5.prototype.duration.call(this)
    },

    supportsFullScreen: function () {
      Utils.log('QubitTech.supportsFullScreen() casting?', Chromecast.isCasting());
      if (Chromecast.isCasting()) {
        return false
      }
      return Html5.prototype.supportsFullScreen.call(this)
    },

    enterFullScreen: function () {
      Utils.log('QubitTech.enterFullScreen() casting?', Chromecast.isCasting());
      if (!Chromecast.isCasting()) {
        return Html5.prototype.enterFullScreen.call(this)
      }
    }
  });

  /**
   * This tech is always supported.
   * (Not really, but is a proxy, we want this tech to be always available)
   */
  QubitTech.isSupported = function () { return true; };

  /**
   * This tech can play anything.
   * (Not really, but is a proxy, we want this tech to be always available)
   */
  QubitTech.canPlaySource = function () { return true; };

  /**
   * If this is false, manual `timeupdate` events will be triggred
   */
  QubitTech.featuresTimeupdateEvents = false;

  /**
   * We must register this tech as a videojs Component to work.
   */
  var Component = videojs.getComponent('Component');
  Component.registerComponent('QubitTech', QubitTech);

  /**
   * We must register this tech as a videojs Tech to work.
   */
  var Tech = videojs.getComponent('Tech');
  Tech.registerTech('QubitTech', QubitTech);

  /**
   * videojs.options.techOrder is an array of names of
   * possible techs to load, overriding it to load only
   * this tech allow us to use this tech as a proxy to
   * the real player, using videojs just as a UI component
   */
  videojs.options.techOrder.unshift('QubitTech');
};

/**
 * @namespace QB
 *
 * @property {PlayerOptions} options - The player options.
 *
 * @property {HTMLElement} title - Title of current video, is an HTMLElement with a link
 * to previous content or a <code>history.back()</code> call.
 *
 * @property {videojs} vjs - Instance of videojs. We use it to manage all the UI:
 * play/pause button, seek bar, volume icon, etc.
 *
 * @property {HTMLVideoElement} video - The native video tag.
 *
 * @property {QubitPlayer} Player - The technology used to play the current media.
 *
 * @property {Function} initialize - Initializes the player.
 */
var QB = Object.create(Observable(), {
  options: {
    value: null,
    writable: true
  },
  title: {
    value: null,
    writable: true
  },
  vjs: {
    value: null,
    writable: true
  },
  video: {
    value: null,
    writable: true
  },
  Player: {
    value: null,
    writable: true
  },
  initialize: {
    value: initialize
  }
});

function initialize(player) {
  Utils.log('QB.initialize()');

  Dependencies.load(function () {
    if (!window.Promise && window.ES6Promise) {
      window.Promise = window.ES6Promise;
    }

    Options.fromUriHash(location.hash)
      .then(saveOptions)
      .then(initializeAuth)
      /**
       * we need to initialize continuous experience before everything else,
       * we use this call to validate that the user can play the requested media
       */
      .then(initializeContinuousExperience)
      .then(subtitlesStyleHack)   // do not remove, see comment below
      .then(loadStyles)
      .then(loadLanguages)
      .then(createTitle)
      .then(createVideo)
      .then(initializePlayer.bind(null, player))
      .then(initializeChromecast)
      /**
       * investigate why we must load the media 2 times,
       * first here (after chromecast) and secondly after
       * videojs is loaded
       */
      .then(loadMedia)
      .then(initializeVideoJSComponents)
      .then(initializeVideoJS)
      .then(initializeYoubora)
      .then(initializePlugins)
      .then(function () {
        var onLoad = loadMedia();
        if (Utils.isIE()) {
          /**
           * This is a hotfix to make the player work on IE 11.
           * TODO: investigate why we have to load a secoond time after a few seconds
           */
          onLoad.then(function () { return setTimeout(loadMedia, 5000); });
        }
      })
      .catch(function (err) {
        if (_.isError(err)) {
          Utils.error('QB.initialize:', err.messsage, '\n', err.stack);
        } else {
          Utils.error('QB.initialize:',  err);
        }
      });
  });
}

function saveOptions(options) {
  Utils.log(("QB.saveOptions(" + (JSON.stringify(options)) + ")"));
  QB.options = options;
}

function initializeAuth() {
  Utils.log('QB.initializeAuth()');
  return Auth.initialize(QB.options)
}

function subtitlesStyleHack() {
  Utils.log('QB.subtitlesStyleHack()');
  /**
   * Some browsers doesn't support changing subtitles style, but they read localstorage for
   * the last saved configuration, so we must force the configuration we want here, even when
   * the css is correct
   */
  try {
    window.localStorage.setItem('vjs-text-track-settings', '{"backgroundOpacity":"0","edgeStyle":"dropshadow"}');
  } catch (e) {
    Utils.error(e);
  }
}

function loadStyles() {
  Utils.log('QB.loadStyles()');
  Config.styles.forEach(Utils.loadStyle);
}

function loadLanguages() {
  Utils.log('QB.loadLanguages()');
  Languages.load();
}

function createTitle() {
  Utils.log('QB.createTitle()');

  if (QB.options.title) {
    var headTitle = document.querySelector('head title');
    headTitle.innerHTML = QB.options.title;

    var backButton = document.createElement('a');
    backButton.className = 'btn-back';
    backButton.href = QB.options.link;

    var textBackButton = document.createElement('h1');
    textBackButton.className = 'text-back-button';
    //textBackButton.innerText = '← ' + QB.options.title
    textBackButton.innerText = QB.options.title;

    var titleElement = document.createElement('div');
    titleElement.className = 'video-js-title';
    titleElement.appendChild(backButton);
    titleElement.appendChild(textBackButton);


    var body = document.querySelector('body');
    body.appendChild(titleElement);

    QB.title = titleElement;
  }
}

function createVideo() {
  Utils.log('QB.createVideo()');

  var video = document.createElement('video');
  video.className = 'video-js vjs-default-skin vjs-big-play-centered vjs-desktop';
  video.crossOrigin = 'anonymous';
  video.preload = 'none';
  video.autoplay = true;
  video.controls = false;

  if (QB.options.poster) {
    video.poster = QB.options.poster;
  }

  var body = document.querySelector('body');
  body.appendChild(video);

  QB.video = video;
}

function initializePlayer(player) {
  Utils.log('QB.initializePlayer()');
  QB.Player = player;
  return QB.Player.initialize(QB)
}

function initializeChromecast() {
  Utils.log('QB.initializeChromecast() QB.Player.enableChromecast? ', QB.Player.enableChromecast());
  if (QB.Player.enableChromecast()) {
    return new Promise(function (resolve) {
      Chromecast.on(Chromecast.READY, function () {
        QB.Player.setImplementation(Chromecast.getRemotePlayer());
      });

      Chromecast.on(Chromecast.STATUS_CHANGED, function (isCasting) {
        if (isCasting) {
          stopContinuousExperience();
        } else {
          startContinuousExperience();
        }
      });

      Chromecast.initialize(QB)
        .then(resolve)
        .catch(function (err) {
          Utils.error(err);
          resolve();   // we don't care for chromecast init errors
        });
    })
  }
}

function initializeVideoJS() {
  Utils.log('QB.initializeVideoJS()');

  QB.options.vjs = {
    persistTextTrackSettings: true,
    controlBar: {
      volumeMenuButton: {
        inline: false,
        vertical: true
      },
      progressControl: {
        seekBar: {
          mouseTimeDisplay: true
        }
      },
      remainingTimeDisplay: false,
      currentTimeDisplay: true,
      durationDisplay: true
    }
  };

  QB.video.controls = true;
  QB.vjs = window.videojs(QB.video, QB.options.vjs);

  if (QB.title) {
    QB.vjs.on('useractive', function () {
      QB.title.style.display = 'block';
    });
    QB.vjs.on('userinactive', function () {
      QB.title.style.display = 'none';
    });
  }

  window.videojs.on(QB.video, 'ready', function () {
    QB.Player.ready();
    if (_.has(QB.Player, 'implementation.addEventListener')) {
      QB.Player.implementation.addEventListener('buffering', function (event) {
        if (event.buffering && !Chromecast.isCasting()) {
          QB.vjs.addClass('vjs-waiting');
        } else {
          QB.vjs.removeClass('vjs-waiting');
        }
      });
    }
  });

  var parent = QB.vjs.el().parentNode;
  if (Utils.isEdge()) { parent.className += parent.className + ' ms-edge'; }
  if (Utils.isIE())   { parent.className += parent.className + ' ms-ie';   }
}

function initializeYoubora() {
  Utils.log('QB.initializeYoubora()');
  return QB.Player.initializeYoubora(QB)
}

function saveTime(time) {
  Utils.log(("QB.saveTime(" + time + ") QB.options.time = " + (QB.options.time)));
  if (Utils.wasPageReloaded() && time < QB.options.creditsBegin) {
    QB.options.time = time;
  }
}

var continuousExperienceIntervalId = 0;

function startContinuousExperience() {
  Utils.log('QB.startContinuousExperience()');
  continuousExperienceIntervalId = setInterval(postCurrentTime, 15000);
}

function stopContinuousExperience() {
  Utils.log('QB.stopContinuousExperience()');
  clearInterval(continuousExperienceIntervalId);
}

function postCurrentTime() {
  if (QB.video && QB.video.duration) {
    ContinuousExperience.setTime(QB.options, QB.video.currentTime, QB.video.duration)
      .catch(function (err) {
        Utils.error('QB.postCurrentTime() ERROR calling ContinuousExperience.setTime()', err);
      });
  }
}

function initializeContinuousExperience() {
  Utils.log('QB.initializeContinuousExperience() QB.options.time:', QB.options.time);

  return new Promise(function (resolve, reject) {
    ContinuousExperience.getTime(QB.options)
      .then(saveTime)
      .then(startContinuousExperience)
      .then(resolve)
      .catch(function (err) {
        // if we can't initialize, it means that we failed to authorize the current session,
        // the solution is to go back from we were loaded, that's the options.link property
        Utils.log(("QB.initializeContinuousExperience() ERROR: " + err));
        if (!Config.debug) {
          window.location.href = QB.options.link;
        }
        reject(err);
      });
  })
}

// ADD MORE PLUGINS HERE

function initializePlugins() {
  Utils.log('QB.initializePlugins()');
  Thumbnails.initialize(QB);
  Related.initialize(QB);
  //ADD more plugins here
}

/** videojs specific components **/
function initializeVideoJSComponents() {
  AudioMenu(QB, videojs, QB.Player.Audio);
  SubtitlesMenu(QB, videojs, QB.Player.Subtitles);
  BackSecondsButton(QB, videojs, 30);
  QubitTech(QB, videojs, QB.Player, Chromecast);
}

function loadMedia() {
  Utils.log('QB.loadMedia()');

  var loadPromise = Promise.resolve();
  if (!Chromecast.isCasting()) {
    Utils.log('QB.loadMedia() not casting, loading media');
    var promise = QB.Player.load(QB.options);
    if (promise) {
      loadPromise = promise;
    }
  }

  return loadPromise.then(function () { return QB.Player.notify(QB.Player.LOADED); })
}

/**
 * Export QB so it can be tested
 */
window.QB = QB;

/**
 * Persistent properties for a given object
 *
 * @interface Persistent
 */
function Persistent(name, storage) {
  if ( name === void 0 ) name = '';
  if ( storage === void 0 ) storage = window.sessionStorage;

  /**
   * @private
   */
  var storageKey = name.toLowerCase();

  function key(s) {
    return storageKey + '_' + (s || '').toLowerCase()
  }

  /**
   * Persists the a value of a property, it creates or updates an existing one.
   *
   * @memberof Persistent
   * @param {String} property - The name of the property to update or to create
   * @param {!Object} value - The new or first value of the property
   */
  function save(property, value) {
    Utils.log(("Persistent(" + name + ").save(" + property + ", " + value + ")"));
    storage[key(property)] = value;
  }

  /**
   * Returns the current selected language
   *
   * @memberof Persistent
   * @param {String} property - The name of the property to fetch
   * @returns {String} The value of the property saved or null
   */
  function load(property) {
    Utils.log(("Persistent(" + name + ").load(" + property + ") => " + (storage[key(property)])));
    return storage[key(property)]
  }

  return {
    save: save,
    load: load
  }
}

function NotImplemented(name) {
  return function () {
    throw new Error('function ' + name + '() not implemented')
  }
}

function UndefinedProperty(name) {
  return function () {
    throw new Error('property "' + name + '" can\'t be undefined')
  }
}

/**
 * Any object that has multiple languages and can have a current one selected.
 *
 * @interface Multilingual
 * @implements {Observable}
 */
function Multilingual(name) {
  if ( name === void 0 ) name = '';

  /**
   * An option to denote "no language"
   *
   * @memberof Multilingual
   * @const
   */
  var OFF = name + ' off';

  /**
   * When {@link Multilingual.selectLanguage} is called, a notification is ussed using {@link Observable.notify}
   * with this event as parameter and the new language.
   *
   * @memberof Multilingual
   * @const
   */
  var CHANGED = name + '_LANGUAGE_CHANGED';

  /**
   * Returns a list of all current langages this multilingual object has.
   *
   * @function languages
   * @abstract
   * @memberof Multilingual
   * @returns {String[]} A list of languages. ie: ['en', 'es']
   */
  var languages = NotImplemented('Multilingual.languages');

  /**
   * Returns the current selected language
   *
   * @memberof Multilingual
   * @returns {String} The current selected language
   */
  function currentLanguage() {
    Utils.log(("Multilingual(" + name + ").currentLanguage() " + (this.load('language'))));
    return this.load('language')
  }

  /**
   * Change the current selected language and notify every observer.
   *
   * @memberof Multilingual
   * @param {String} language - The new selected language to set.
   *
   * @see {@link Observable}
   */
  function selectLanguage(language) {
    Utils.log(("Multilingual(" + name + ").selectLanguage(" + language + ")"));
    this.save('language', language);
    this.notify(CHANGED, language);
  }

  /**
   * Load the saved language for the current session, the given language or
   * the first one available
   *
   * @memberof Multilingual
   * @param {String} language - A language to select.
   */
  function initialize(language) {
    Utils.log(("Multilingual(" + name + ").initialize(" + language + ")"));
    // select the saved language, the passed one or the first available
    var languages = this.languages();
    var savedLanguage = this.load('language');
    if (languages.indexOf(savedLanguage) !== -1) {
      // if we have a valid saved language, use it!
      this.selectLanguage(savedLanguage);
    } else {
      // if we do not have a valid saved language use the one we got
      // or the first available
      this.selectLanguage(language || languages[0]);
    }
  }

  var MultilingualInstance = Object.create(Observable(), {
    initialize: {value: initialize},
    languages: {value: languages},
    currentLanguage: {value: currentLanguage},
    selectLanguage: {value: selectLanguage},
    CHANGED: {value: CHANGED},
    OFF: {value: OFF}
  });

  return Object.assign(MultilingualInstance, Persistent(name))
}

var Player = function (properties) {
  var _ready = false;

  return Object.create(Observable(), {
    LOADED: {
      value: 'LOADED'
    },
    name: {
      value: properties.name || UndefinedProperty('Player.name')
    },
    implementation: {
      value: properties.implementation || UndefinedProperty('Player.implementation'),
      writable: true
    },
    setImplementation: {
      value: properties.setImplementation || NotImplemented('Player.setImplementation')
    },
    enableChromecast: {
      value: properties.enableChromecast || NotImplemented('Player.supportsChromecast')
    },
    initialize: {
      value: properties.initialize || NotImplemented('Player.initialize')
    },
    initializeYoubora: {
      value: properties.initializeYoubora || NotImplemented('Player.initializeYoubora')
    },
    load: {
      value: properties.load || NotImplemented('Player.load')
    },
    Audio: {
      value: properties.Audio || UndefinedProperty('Player.Audio'),
      writable: true
    },
    Subtitles: {
      value: properties.Subtitles || UndefinedProperty('Player.Subtitles'),
      writable: true
    },

    READY: {
      value: 'READY'
    },
    ready: {
      value: function () {
        _ready = true;
        this.notify(this.READY);
      }
    },
    isReady:{
      value: function () {
        return _ready
      }
    }
  })
};

/**
 * It uses {@link https://shaka-player-demo.appspot.com/docs/api/shaka.Player.html|ShakaPlayer},
 * an implementation of a DASH player. And adss <code>QB.shaka</code> property
 *
 * @namespace DashPlayer
 * @implements {QubitPlayer}
 */

function loadManifest(options) {
  Utils.log('DashPlayer.loadManifest(', options, ')');
  var url = Options.getSourceUrl(options);
  return Utils.get(url, {'Content-Type': null})
    .then(function (xml) { return processManifest(url, options, xml); })
}

function forEachChild(tag, childTagName, fn) {
  return Utils.toArray(tag.childNodes).filter(byTagName(childTagName)).forEach(fn)
}

function byTagName(name) {
  if (_.isEmpty(name)) {
    throw new Error('name can\'t be null, undefined or empty')
  }
  return function (element) {
    return element && element.tagName && element.tagName.toLowerCase() == name.toLowerCase()
  }
}

function forEachRepresentation(mpd, fn) {
  forEachChild(mpd, 'Period', function (period) {
    forEachChild(period, 'AdaptationSet', function (adaptationSet) {
      forEachChild(adaptationSet, 'Representation', function (representation) {
        fn(period, adaptationSet, representation);
      });
    });
  });
}

function processManifest(url, options, xml) {
  Utils.log('DashPlayer.processManifest(url:', url, ', options:', options, ', xml:', (''+xml).substr(0, 50), ')');
  var mpd = getMpd(url, xml);

  var subtitles = extractSubtitles(mpd);
  if (subtitles.length) {
    options.subtitles = subtitles;
  }

  options.dashSource = url;
  return options
}

function getMpd(url, xml) {
  Utils.log('DashPlayer.getMpd(url:', url, ', xml:', (''+xml).substr(0, 50), ')');
  var parser = new window.DOMParser();
  var doc = parser.parseFromString(xml, 'text/xml');

  if (!doc.childNodes || doc.childNodes.length == 0) {
    throw new Error('manifest has no elements ' + url)
  }

  var mpd = doc.childNodes[0];
  if (!(mpd && mpd.tagName && mpd.tagName.toLowerCase() == 'mpd')) {
    throw new Error('manifest has no MPD element as root ' + url)
  }

  return mpd
}

function extractSubtitles(mpd) {
  var subtitles = [];
  forEachRepresentation(mpd, function (period, adaptationSet, representation) {
    if (adaptationSet.getAttribute('mimeType') == 'text/vtt') {
      var lang = adaptationSet.getAttribute('lang');
      forEachChild(representation, 'BaseURL', function (url) {
        subtitles.push({
          lang: lang,
          url: url.innerHTML
        });
      });
    }
  });

  Utils.log('DashPlayer.extractSubtitles() =>', subtitles);
  return subtitles
}

function configure(options) {
  var config = {
    drm: {
      servers: {}
    },
    restrictions: {
      minBandwidth: 8000  // bit/second
    },
    streaming: {
      bufferBehind: 30,   // seconds
      bufferingGoal: 60   // seconds
    }
  };

  if (options.playready) {
    config.drm.servers['com.microsoft.playready'] = options.playready;
  }

  if (options.widevine) {
    config.drm.servers['com.widevine.alpha'] = options.widevine;
    // for our cls proxy:
    // config.drm.servers['com.widevine.alpha'] = options.api
    //   + '/cls?user=' + options.userId
    //   + '&operator=' + options.operator
    //   + '&uuid=' + options.uuid
  }
  if (options.proxyHeaders) {
    ShakaPlayer.implementation.getNetworkingEngine().registerRequestFilter(function(type, request) {
      if (type == window.shaka.net.NetworkingEngine.RequestType.LICENSE) {
        for (var head in options.proxyHeaders) {
          request.headers[head] = options.proxyHeaders[head];
        }
      }
    });
  }

  Utils.log('DashPlayer.configureShakaPlayer() config:', config);

  ShakaPlayer.implementation.configure(config);
}

function setImplementation(implementation) {
  Utils.log('DashPlayer.setImplementation()', implementation);
  ShakaPlayer.implementation = implementation;
}

function createAudio() {
  var audioLanguages = null;

  var AudioBase = Multilingual('Audio');
  return Object.create(AudioBase, {
    languages: {value: function () {
      Utils.log('DashPlayer.Audio.languages()');
      if (_.isEmpty(audioLanguages)) {
        audioLanguages = ShakaPlayer.implementation.getAudioLanguages();
      }
      return audioLanguages
    }},

    selectLanguage: {value: function (language) {
      Utils.log('DashPlayer.Audio.selectLanguage(', language, ')');
      var shakaPlayer = ShakaPlayer.implementation;
      var abr = shakaPlayer.getConfiguration().abr;
      shakaPlayer.selectAudioLanguage(language);
      shakaPlayer.configure({abr: abr, preferredAudioLanguage: language});
      AudioBase.selectLanguage.call(this, language);
    }}
  })
}

function createSubtitles(options) {
  var SubtitlesBase = Multilingual('Subtitles');
  return Object.create(SubtitlesBase, {
    languages: {value: function () {
      Utils.log('DashPlayer.Subtitles.languages()');
      var languages = ShakaPlayer.implementation.getTextLanguages() || [];
      if (languages.length == 0) {
        languages = options.subtitles.map(function (subtitle) { return subtitle.lang; });
      }
      return [SubtitlesBase.OFF].concat(languages)
    }},

    selectLanguage: {value: function (language) {
      Utils.log('DashPlayer.Subtitles.selectLanguage(', language, ')');
      var shakaPlayer = ShakaPlayer.implementation
        , tracks = shakaPlayer.getTextTracks()
        , track = tracks.filter(function (track) { return track.language === language; })[0];

      shakaPlayer.setTextTrackVisibility(!!track);
      if (track) {
        var abr = shakaPlayer.getConfiguration().abr;
        shakaPlayer.selectTrack(track);
        shakaPlayer.configure({abr: abr, preferredTextLanguage: language});
      } else {
        shakaPlayer.configure({preferredTextLanguage: null});
      }

      SubtitlesBase.selectLanguage.call(this, language);
    }}
  })
}

function initialize$3(QB) {
  Utils.log('DashPlayer.initialize()');

  var options = QB.options;

  ShakaPlayer.Audio = createAudio();
  ShakaPlayer.Subtitles = createSubtitles(options);

  return Utils.loadScript(Config.Dash.lib).then(function () {
    if (!window.shaka) {
      throw new Error('Can\'t use dash if window.shaka is not available')
    }

    window.shaka.polyfill.installAll();

    if (options.widevine && !options.playready) {
      var track = document.createElement('track');
      track.label = 'Shaka Player TextTrack';
      track.type = 'subtitles';
      QB.video.appendChild(track);
    }

    setImplementation(new window.shaka.Player(QB.video));
    configure(options);

    return loadManifest(options)
  })
}

function initializeYoubora$1(QB) {
  return Utils.loadScript(Config.Dash.youbora)
    .then(function () { return QB.vjs.youbora(QB.options.youbora); })
    .catch(Utils.error)
}

function load$1(options) {
  Utils.log('DashPlayer.load()', options, 'shaka config:', ShakaPlayer.implementation.getConfiguration());

  if (_.isEmpty(options.dashSource)) {
    throw new Error('DashPlayer.initialize(): no options.dashSource found')
  }

  /**
   * FIXME: options.time sometimes is the end of the media, in that case we
   * must NOT load with that time, but with 0, the issue is that we need to
   * load to know the duration. We have an egg/chicken problem here.
   */
  if (options.time && _.isNumber(options.time)) {
    return ShakaPlayer.implementation.load(options.dashSource, options.time)
  } else {
    return ShakaPlayer.implementation.load(options.dashSource)
  }
}

var ShakaPlayer = Player({
  name: 'shakaPlayer',  /* needed for Youbora */
  implementation: null, /* needed for Youbora & Chromecast */
  setImplementation: setImplementation,
  enableChromecast: function () { return true; },
  initialize: initialize$3,
  initializeYoubora: initializeYoubora$1,
  load: load$1,
  Audio: null,
  Subtitles: null
});

function onDocumentLoad() {
  QB.initialize(ShakaPlayer);
}

if (document.readyState === 'loaded') {
  onDocumentLoad();
} else {
  document.addEventListener('DOMContentLoaded', onDocumentLoad);
}

}());
