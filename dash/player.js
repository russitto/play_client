/**
 * It uses {@link https://shaka-player-demo.appspot.com/docs/api/shaka.Player.html|ShakaPlayer},
 * an implementation of a DASH player. And adss <code>QB.shaka</code> property
 *
 * @namespace DashPlayer
 * @implements {QubitPlayer}
 */

import Config from '../lib/config'
import Options from '../lib/options'
import Utils from '../lib/utils'
import Multilingual from '../lib/multilingual'

function loadManifest(options) {
  Utils.log('DashPlayer.loadManifest(', options, ')')
  const url = Options.getSourceUrl(options)
  return Utils.get(url, {'Content-Type': null})
    .then(xml => processManifest(url, options, xml))
}

function forEachChild(tag, childTagName, fn) {
  return Utils.toArray(tag.childNodes).filter(byTagName(childTagName)).forEach(fn)
}

function byTagName(name) {
  if (_.isEmpty(name)) {
    throw new Error('name can\'t be null, undefined or empty')
  }
  return function (element) {
    return element && element.tagName && element.tagName.toLowerCase() == name.toLowerCase()
  }
}

function forEachRepresentation(mpd, fn) {
  forEachChild(mpd, 'Period', function (period) {
    forEachChild(period, 'AdaptationSet', function (adaptationSet) {
      forEachChild(adaptationSet, 'Representation', function (representation) {
        fn(period, adaptationSet, representation)
      })
    })
  })
}

function processManifest(url, options, xml) {
  Utils.log('DashPlayer.processManifest(url:', url, ', options:', options, ', xml:', (''+xml).substr(0, 50), ')')
  const mpd = getMpd(url, xml)

  const subtitles = extractSubtitles(mpd)
  if (subtitles.length) {
    options.subtitles = subtitles
  }

  options.dashSource = url
  return options
}

function getMpd(url, xml) {
  Utils.log('DashPlayer.getMpd(url:', url, ', xml:', (''+xml).substr(0, 50), ')')
  const parser = new window.DOMParser()
  const doc = parser.parseFromString(xml, 'text/xml')

  if (!doc.childNodes || doc.childNodes.length == 0) {
    throw new Error('manifest has no elements ' + url)
  }

  const mpd = doc.childNodes[0]
  if (!(mpd && mpd.tagName && mpd.tagName.toLowerCase() == 'mpd')) {
    throw new Error('manifest has no MPD element as root ' + url)
  }

  return mpd
}

function extractSubtitles(mpd) {
  const subtitles = []
  forEachRepresentation(mpd, function (period, adaptationSet, representation) {
    if (adaptationSet.getAttribute('mimeType') == 'text/vtt') {
      const lang = adaptationSet.getAttribute('lang')
      forEachChild(representation, 'BaseURL', function (url) {
        subtitles.push({
          lang: lang,
          url: url.innerHTML
        })
      })
    }
  })

  Utils.log('DashPlayer.extractSubtitles() =>', subtitles)
  return subtitles
}

function configure(options) {
  const config = {
    drm: {
      servers: {}
    },
    restrictions: {
      minBandwidth: 8000  // bit/second
    },
    streaming: {
      bufferBehind: 30,   // seconds
      bufferingGoal: 60   // seconds
    }
  }

  if (options.playready) {
    config.drm.servers['com.microsoft.playready'] = options.playready
  }

  if (options.widevine) {
    config.drm.servers['com.widevine.alpha'] = options.widevine
    // for our cls proxy:
    // config.drm.servers['com.widevine.alpha'] = options.api
    //   + '/cls?user=' + options.userId
    //   + '&operator=' + options.operator
    //   + '&uuid=' + options.uuid
  }
  if (options.proxyHeaders) {
    ShakaPlayer.implementation.getNetworkingEngine().registerRequestFilter(function(type, request) {
      if (type == window.shaka.net.NetworkingEngine.RequestType.LICENSE) {
        for (const head in options.proxyHeaders) {
          request.headers[head] = options.proxyHeaders[head]
        }
      }
    })
  }

  Utils.log('DashPlayer.configureShakaPlayer() config:', config)

  ShakaPlayer.implementation.configure(config)
}

function setImplementation(implementation) {
  Utils.log('DashPlayer.setImplementation()', implementation)
  ShakaPlayer.implementation = implementation
}

function createAudio() {
  let audioLanguages = null

  const AudioBase = Multilingual('Audio')
  return Object.create(AudioBase, {
    languages: {value: function () {
      Utils.log('DashPlayer.Audio.languages()')
      if (_.isEmpty(audioLanguages)) {
        audioLanguages = ShakaPlayer.implementation.getAudioLanguages()
      }
      return audioLanguages
    }},

    selectLanguage: {value: function (language) {
      Utils.log('DashPlayer.Audio.selectLanguage(', language, ')')
      const shakaPlayer = ShakaPlayer.implementation
      const abr = shakaPlayer.getConfiguration().abr
      shakaPlayer.selectAudioLanguage(language)
      shakaPlayer.configure({abr: abr, preferredAudioLanguage: language})
      AudioBase.selectLanguage.call(this, language)
    }}
  })
}

function createSubtitles(options) {
  const SubtitlesBase = Multilingual('Subtitles')
  return Object.create(SubtitlesBase, {
    languages: {value: function () {
      Utils.log('DashPlayer.Subtitles.languages()')
      let languages = ShakaPlayer.implementation.getTextLanguages() || []
      if (languages.length == 0) {
        languages = options.subtitles.map(subtitle => subtitle.lang)
      }
      return [SubtitlesBase.OFF].concat(languages)
    }},

    selectLanguage: {value: function (language) {
      Utils.log('DashPlayer.Subtitles.selectLanguage(', language, ')')
      const shakaPlayer = ShakaPlayer.implementation
        , tracks = shakaPlayer.getTextTracks()
        , track = tracks.filter(track => track.language === language)[0]

      shakaPlayer.setTextTrackVisibility(!!track)
      if (track) {
        const abr = shakaPlayer.getConfiguration().abr
        shakaPlayer.selectTrack(track)
        shakaPlayer.configure({abr: abr, preferredTextLanguage: language})
      } else {
        shakaPlayer.configure({preferredTextLanguage: null})
      }

      SubtitlesBase.selectLanguage.call(this, language)
    }}
  })
}

function initialize(QB) {
  Utils.log('DashPlayer.initialize()')

  const options = QB.options

  ShakaPlayer.Audio = createAudio()
  ShakaPlayer.Subtitles = createSubtitles(options)

  return Utils.loadScript(Config.Dash.lib).then(function () {
    if (!window.shaka) {
      throw new Error('Can\'t use dash if window.shaka is not available')
    }

    window.shaka.polyfill.installAll()

    if (options.widevine && !options.playready) {
      const track = document.createElement('track')
      track.label = 'Shaka Player TextTrack'
      track.type = 'subtitles'
      QB.video.appendChild(track)
    }

    setImplementation(new window.shaka.Player(QB.video))
    configure(options)

    return loadManifest(options)
  })
}

function initializeYoubora(QB) {
  return Utils.loadScript(Config.Dash.youbora)
    .then(() => QB.vjs.youbora(QB.options.youbora))
    .catch(Utils.error)
}

function load(options) {
  Utils.log('DashPlayer.load()', options, 'shaka config:', ShakaPlayer.implementation.getConfiguration())

  if (_.isEmpty(options.dashSource)) {
    throw new Error('DashPlayer.initialize(): no options.dashSource found')
  }

  /**
   * FIXME: options.time sometimes is the end of the media, in that case we
   * must NOT load with that time, but with 0, the issue is that we need to
   * load to know the duration. We have an egg/chicken problem here.
   */
  if (options.time && _.isNumber(options.time)) {
    return ShakaPlayer.implementation.load(options.dashSource, options.time)
  } else {
    return ShakaPlayer.implementation.load(options.dashSource)
  }
}

import Player from '../lib/player'

const ShakaPlayer = Player({
  name: 'shakaPlayer',  /* needed for Youbora */
  implementation: null, /* needed for Youbora & Chromecast */
  setImplementation: setImplementation,
  enableChromecast: () => true,
  initialize: initialize,
  initializeYoubora: initializeYoubora,
  load: load,
  Audio: null,
  Subtitles: null
})

export default ShakaPlayer
