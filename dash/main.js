import QB from '../lib/qb'
import DashPlayer from './player'

function onDocumentLoad() {
  QB.initialize(DashPlayer)
}

if (document.readyState === 'loaded') {
  onDocumentLoad()
} else {
  document.addEventListener('DOMContentLoaded', onDocumentLoad)
}
