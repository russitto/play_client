#!/usr/bin/env sh

verbose=0
browser=1
protocol=https
stream=""
env=dev
output=
cleared=0
debug=0
quality=SD
localaddress=`ip address | grep "inet " | awk '{split($2,a,"/"); print(a[1])}' | head -2 | tail -1`
ip=$localaddress

jq=`which jq`

# to get the sessionToken get project login_with_hash (git@git.qubit.tv:mrussitto/login_with_hash.git) and run:
# node index.js [mail] [pass]
# get SessionToken from the response
user="lsanchez@qubit.tv"
sessionToken="%0F%F0%C9%F7%F5b%91%D5%FD%E1r%07%28%D6v%B5%9DKYu%117%96%ECUeF%B1%D2%F2O%FC%DE%B7%83%26%91U%E0%D80%B1%AC%BAN%91FS%C6%0E%21%B6%2F%BA%3D%CF%DA%FF%CA-%BC%85%BF%CD%17%A0ZX%21%40%2A%81%C4%60%7D%C8%2A%0A%84%AD"
# id="0ab6e9de-e7b8-4e94-bb2b-a787bb777c5a_ElViajeDeChihiro2001"              # ja, es
# id="09d55bd5-81f3-4cf2-9944-c12d7b1da5c8_CrounchingTigerHiddenDragon2000"   # zh, es
# id="0a482c91-79bf-4eff-9301-cc8b0b868855_AceVenturaWhenNatureCalls1995"     # en
# id="0c240661-67c2-4a56-921f-668950a96297_KongSkullIsland2016"               # es, en
# id="18f26755-0334-47a2-b7bf-852458b29e62_TheWhiteRibbon"                    # de
id="0c69cf62-88be-4166-bfd2-1ddeaebd03fb_TheFateOfTheFurious2017"           # es, en

usage() {
  echo "Usage: $0 [options]" 1>&2
  echo "" 1>&2
  echo "Options:" 1>&2
  echo "  -a          Use http instead of https" 1>&2
  echo "  -c          Clean all files." 1>&2
  echo "  -d          Set debug flag. Opens debug.html instead of index.html" 1>&2
  echo "  -e ENV      What environment to open: dev, staging or prod. Default is dev." 1>&2
  echo "  -h HASH     SessionToken of user to login. To get a SessionToken use git.qubit.tv:mrussito/login_with_hash" 1>&2
  echo "  -i ID       Id of movie to play." 1>&2
  echo "  -n          Do not open url on chrome, just prints the url." 1>&2
  echo "  -o FILE     Output url to given file." 1>&2
  echo "  -p IP       IP to send as 'userIp'." 1>&2
  echo "  -q QUALITY  Quality to load: SD or HD. SD by default." 1>&2
  echo "  -s STREAM   What stream to open: dash, hls or playready." 1>&2
  echo "  -u USER     User to login." 1>&2
  echo "  -v          Show all output." 1>&2
  exit 1
}

clean() {
  rm url 2>/dev/null >/dev/null
  rm out 2>/dev/null >/dev/null
}

error() {
  if [ $cleared -eq 0 ]; then
    clean
    echo $1
    usage
  fi
  exit $cleared
}

while getopts "acde:h:i:no:p:q:s:u:v" flag
do
  case "$flag" in
    a)
      protocol=http
      ;;
    c)
      cleared=1
      clean
      ;;
    d)
      debug=1
      ;;
    e)
      env=$OPTARG
      ;;
    h)
      sessionToken=$OPTARG
      ;;
    i)
      id=$OPTARG
      ;;
    n)
      browser=0
      ;;
    o)
      output=$OPTARG
      ;;
    p)
      ip=$OPTARG
      ;;
    q)
      quality=$OPTARG
      ;;
    s)
      stream=$OPTARG
      ;;
    u)
      user=$OPTARG
      ;;
    v)
      verbose=1
      ;;
    *)
      usage
      ;;
  esac
done

if [ -z $stream ]; then
  error "No stream provided"
fi

case "$env" in
  dev)
    api=$protocol://$localaddress:8090
    ;;
  staging)
    api=$protocol://staging-front:8090
    ;;
  prod)
    api=$protocol://play.qubit.tv
    ;;
  *)
    error "Invalid environment: $env"
    ;;
esac

cat > body << EOF
{
  "hash": "$sessionToken",
  "operator": "qubit_vod",
  "quality": "$quality",
  "language": "en",
  "stream": "$stream",
  "username": "$user",
  "uuid": "$id",
  "userIp": "$ip"
}
EOF

data=`cat body`

echo "POST $api/v1/play"

if [ $verbose -eq 1 ]; then
  if [ -x "$jq" ]; then
    cat body | $jq
  else
    cat body
  fi
fi

rm body

echo ""
if [ -x "$jq" ]; then
  curl -v -k -H "Content-Type: application/json" -X POST -d "$data" $api/v1/play 2> /dev/null | jq . > out
else
  curl -v -k -H "Content-Type: application/json" -X POST -d "$data" $api/v1/play 2> /dev/null > out
fi

cat out | sed -n 's/.*"embedUrl".*:.*"\(.*\)".*/\1/p' > url

if [ $debug -eq 1 ]; then
  cat url | sed -e 's/#/debug.html#/' > url2
  mv url2 url
fi

if [ $verbose -eq 1 ]; then
  if [ -x "$jq" ]; then
    cat out | $jq
  else
    cat out
  fi
  echo ""
fi

if [ -s url ]; then
  if [ $browser -eq 1 ]; then
    google-chrome `cat url`
  else
    cat url
    cat url | xsel -ib
    echo ""
    echo "url copied to clipboard"
    if [ ! -z $output ]; then
      echo "<html><body><a href='`cat url`'>`cat url`</a></body></html>" > $output
      echo "url copied to $output"
    fi
  fi
  clean
else
  cat out
  error "No url found"
fi

