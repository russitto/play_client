#!/bin/bash

project="p"

branch=`git branch | grep '^* ' | sed -e 's/^* //' -e 's/ at .*//'`
if [[ "$branch" != 'master' && $branch != '(HEAD detached' ]]; then
  echo 'Only in master branch'
  exit 1
fi

version=`grep '"version"' package.json | sed -r 's/.*"version"\s*:\s*"([0-9\.]*)".*/\1/'`

if [ "$#" -eq 0 ]; then
  echo "Usage:
$0 stage|preprod|prod [ftp_password]"
  exit 2
fi

env=$1
if [ $env != "stage" -a $env != "preprod" -a $env != "prod" ]; then
  echo "Usage:
$0 stage|preprod|prod [ftp_password]"
  exit 3
fi

user=players
if [ "$#" -gt 1 ]; then
  user="$user,$2"
fi

base=`dirname $0`
cd $base
dir="$env/$project/$version"
lftp -e "mkdir -p $dir && cd $dir && mirror -R dist dist && mirror -R dash dash && mirror -R hls hls; exit" -u $user sftp://172.31.22.245
