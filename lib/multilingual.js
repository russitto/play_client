import Utils from './utils'
import Observable from './observable'
import Persistent from './persistent'
import { NotImplemented } from './errors'

/**
 * Any object that has multiple languages and can have a current one selected.
 *
 * @interface Multilingual
 * @implements {Observable}
 */
export default function Multilingual(name = '') {
  /**
   * An option to denote "no language"
   *
   * @memberof Multilingual
   * @const
   */
  const OFF = name + ' off'

  /**
   * When {@link Multilingual.selectLanguage} is called, a notification is ussed using {@link Observable.notify}
   * with this event as parameter and the new language.
   *
   * @memberof Multilingual
   * @const
   */
  const CHANGED = name + '_LANGUAGE_CHANGED'

  /**
   * Returns a list of all current langages this multilingual object has.
   *
   * @function languages
   * @abstract
   * @memberof Multilingual
   * @returns {String[]} A list of languages. ie: ['en', 'es']
   */
  const languages = NotImplemented('Multilingual.languages')

  /**
   * Returns the current selected language
   *
   * @memberof Multilingual
   * @returns {String} The current selected language
   */
  function currentLanguage() {
    Utils.log(`Multilingual(${name}).currentLanguage() ${this.load('language')}`)
    return this.load('language')
  }

  /**
   * Change the current selected language and notify every observer.
   *
   * @memberof Multilingual
   * @param {String} language - The new selected language to set.
   *
   * @see {@link Observable}
   */
  function selectLanguage(language) {
    Utils.log(`Multilingual(${name}).selectLanguage(${language})`)
    this.save('language', language)
    this.notify(CHANGED, language)
  }

  /**
   * Load the saved language for the current session, the given language or
   * the first one available
   *
   * @memberof Multilingual
   * @param {String} language - A language to select.
   */
  function initialize(language) {
    Utils.log(`Multilingual(${name}).initialize(${language})`)
    // select the saved language, the passed one or the first available
    const languages = this.languages()
    const savedLanguage = this.load('language')
    if (languages.indexOf(savedLanguage) !== -1) {
      // if we have a valid saved language, use it!
      this.selectLanguage(savedLanguage)
    } else {
      // if we do not have a valid saved language use the one we got
      // or the first available
      this.selectLanguage(language || languages[0])
    }
  }

  const MultilingualInstance = Object.create(Observable(), {
    initialize: {value: initialize},
    languages: {value: languages},
    currentLanguage: {value: currentLanguage},
    selectLanguage: {value: selectLanguage},
    CHANGED: {value: CHANGED},
    OFF: {value: OFF}
  })

  return Object.assign(MultilingualInstance, Persistent(name))
}
