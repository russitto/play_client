/**
 * An observable pattern implementation
 *
 * @interface Observable
 */
export default function Observable() {
  const observers = {}

  function observersFor(event) {
    return (observers[event] = observers[event] || [])
  }

  /**
   * Adds an observer to be notified when the given event is triggered.
   *
   * @memberof Observable
   * @param {String} event - The event to observe.
   * @param {Function} observer - The observer function to call when the event is triggered.
   */
  function on(event, observer) {
    if (typeof event !== 'string') {
      throw new TypeError('event should be a string')
    }

    if (typeof observer !== 'function') {
      throw new TypeError('observer should be a function')
    }

    const list = observersFor(event)
    const index = list.indexOf(observer)
    if (index === -1) {
      list.push(observer)
    }
  }

  /**
   * Adds an observer to be notified only once and then remove itself.
   *
   * @memberof Observable
   * @param {String} event - The event to observe.
   * @param {Function} observer - The observer function to call when the event is triggered.
   */
  function one(event, observer) {
    const self = this

    function callback(data) {
      self.off(event, callback)
      observer && observer(data)
    }

    self.on(event, callback)
  }

  /**
   * Removes the observer for the given event. After this call the observer will
   * no longer be notified if the event is triggered.
   *
   * @memberof Observable
   * @param {String} event - The event to stop observing.
   * @param {Function} observer - The observer function to stop calling.
   */
  function off(event, observer) {
    if (typeof event !== 'string') {
      throw new TypeError('event should be a string')
    }

    if (typeof observer !== 'function') {
      throw new TypeError('observer should be a function')
    }

    const list = observersFor(event)
    const index = list.indexOf(observer)
    if (index !== -1) {
      list.splice(index, 1)
    }
  }

  /**
   * Notify every registered observer that a given event happend.
   *
   * @memberof Observable
   * @param {String} event - The event to notify.
   * @param {Object} [data] - The object to send to every observer.
   */
  function notify(event, data) {
    if (typeof event !== 'string') {
      throw new TypeError('event should be a string')
    }

    /**
     * traverse a copy of the array for the case 'off'
     * is called by an observer
     */
    [].concat(observersFor(event))
      .forEach(observer => observer && observer(data))
  }

  return {
    on: on,
    one: one,
    off: off,
    notify: notify
  }
}
