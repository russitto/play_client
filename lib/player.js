import { NotImplemented, UndefinedProperty } from './errors'
import Observable from './observable'

export default function (properties) {
  let _ready = false

  return Object.create(Observable(), {
    LOADED: {
      value: 'LOADED'
    },
    name: {
      value: properties.name || UndefinedProperty('Player.name')
    },
    implementation: {
      value: properties.implementation || UndefinedProperty('Player.implementation'),
      writable: true
    },
    setImplementation: {
      value: properties.setImplementation || NotImplemented('Player.setImplementation')
    },
    enableChromecast: {
      value: properties.enableChromecast || NotImplemented('Player.supportsChromecast')
    },
    initialize: {
      value: properties.initialize || NotImplemented('Player.initialize')
    },
    initializeYoubora: {
      value: properties.initializeYoubora || NotImplemented('Player.initializeYoubora')
    },
    load: {
      value: properties.load || NotImplemented('Player.load')
    },
    Audio: {
      value: properties.Audio || UndefinedProperty('Player.Audio'),
      writable: true
    },
    Subtitles: {
      value: properties.Subtitles || UndefinedProperty('Player.Subtitles'),
      writable: true
    },

    READY: {
      value: 'READY'
    },
    ready: {
      value: function () {
        _ready = true
        this.notify(this.READY)
      }
    },
    isReady:{
      value: function () {
        return _ready
      }
    }
  })
}
