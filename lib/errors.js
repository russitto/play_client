export function NotImplemented(name) {
  return function () {
    throw new Error('function ' + name + '() not implemented')
  }
}

export function UndefinedProperty(name) {
  return function () {
    throw new Error('property "' + name + '" can\'t be undefined')
  }
}
