import Utils from './utils'

const REFRESH_INTERVAL = 12 * 60 * 1000   // 12 minutes

let token = null

function getAuthHeader() {
  return {Authorization: 'Bearer ' + token}
}

function getNewToken(url) {
  return Utils.post(url, getAuthHeader())
    .then(JSON.parse)
    .then(response => {
      Utils.log(`Auth.getNewToken(${url}) JWT: ${response.jwt}`)
      token = response.jwt
    })
}

function initialize(options) {
  token = options.token

  // Every REFRESH_INTERVAL (in milliseconds), get a new token and ignore any errors
  const refreshUrl = options.api + '/refresh'
  setInterval(() => getNewToken(refreshUrl).catch(Utils.error), REFRESH_INTERVAL)

  return token
}

function getToken() {
  return token
}

export default {
  initialize: initialize,
  getToken: getToken
}
