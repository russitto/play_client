import Auth from './auth'
import Chromecast from './chromecast'
import Config from './config'
import ContinuousExperience from './continuous_experience'
import Dependencies from './dependencies'
import Languages from './languages/all'
import Observable from './observable'
import Options from './options'
import Utils from './utils'

/**
 * @namespace QB
 *
 * @property {PlayerOptions} options - The player options.
 *
 * @property {HTMLElement} title - Title of current video, is an HTMLElement with a link
 * to previous content or a <code>history.back()</code> call.
 *
 * @property {videojs} vjs - Instance of videojs. We use it to manage all the UI:
 * play/pause button, seek bar, volume icon, etc.
 *
 * @property {HTMLVideoElement} video - The native video tag.
 *
 * @property {QubitPlayer} Player - The technology used to play the current media.
 *
 * @property {Function} initialize - Initializes the player.
 */
const QB = Object.create(Observable(), {
  options: {
    value: null,
    writable: true
  },
  title: {
    value: null,
    writable: true
  },
  vjs: {
    value: null,
    writable: true
  },
  video: {
    value: null,
    writable: true
  },
  Player: {
    value: null,
    writable: true
  },
  initialize: {
    value: initialize
  }
})

function initialize(player) {
  Utils.log('QB.initialize()')

  Dependencies.load(function () {
    if (!window.Promise && window.ES6Promise) {
      window.Promise = window.ES6Promise
    }

    Options.fromUriHash(location.hash)
      .then(saveOptions)
      .then(initializeAuth)
      /**
       * we need to initialize continuous experience before everything else,
       * we use this call to validate that the user can play the requested media
       */
      .then(initializeContinuousExperience)
      .then(subtitlesStyleHack)   // do not remove, see comment below
      .then(loadStyles)
      .then(loadLanguages)
      .then(createTitle)
      .then(createVideo)
      .then(initializePlayer.bind(null, player))
      .then(initializeChromecast)
      /**
       * investigate why we must load the media 2 times,
       * first here (after chromecast) and secondly after
       * videojs is loaded
       */
      .then(loadMedia)
      .then(initializeVideoJSComponents)
      .then(initializeVideoJS)
      .then(initializeYoubora)
      .then(initializePlugins)
      .then(() => {
        const onLoad = loadMedia()
        if (Utils.isIE()) {
          /**
           * This is a hotfix to make the player work on IE 11.
           * TODO: investigate why we have to load a secoond time after a few seconds
           */
          onLoad.then(() => setTimeout(loadMedia, 5000))
        }
      })
      .catch(err => {
        if (_.isError(err)) {
          Utils.error('QB.initialize:', err.messsage, '\n', err.stack)
        } else {
          Utils.error('QB.initialize:',  err)
        }
      })
  })
}

function saveOptions(options) {
  Utils.log(`QB.saveOptions(${JSON.stringify(options)})`)
  QB.options = options
}

function initializeAuth() {
  Utils.log('QB.initializeAuth()')
  return Auth.initialize(QB.options)
}

function subtitlesStyleHack() {
  Utils.log('QB.subtitlesStyleHack()')
  /**
   * Some browsers doesn't support changing subtitles style, but they read localstorage for
   * the last saved configuration, so we must force the configuration we want here, even when
   * the css is correct
   */
  try {
    window.localStorage.setItem('vjs-text-track-settings', '{"backgroundOpacity":"0","edgeStyle":"dropshadow"}')
  } catch (e) {
    Utils.error(e)
  }
}

function loadStyles() {
  Utils.log('QB.loadStyles()')
  Config.styles.forEach(Utils.loadStyle)
}

function loadLanguages() {
  Utils.log('QB.loadLanguages()')
  Languages.load()
}

function createTitle() {
  Utils.log('QB.createTitle()')

  if (QB.options.title) {
    const headTitle = document.querySelector('head title')
    headTitle.innerHTML = QB.options.title

    const backButton = document.createElement('a')
    backButton.className = 'btn-back'
    backButton.href = QB.options.link

    const textBackButton = document.createElement('h1')
    textBackButton.className = 'text-back-button'
    //textBackButton.innerText = '← ' + QB.options.title
    textBackButton.innerText = QB.options.title

    const titleElement = document.createElement('div')
    titleElement.className = 'video-js-title'
    titleElement.appendChild(backButton)
    titleElement.appendChild(textBackButton)


    const body = document.querySelector('body')
    body.appendChild(titleElement)

    QB.title = titleElement
  }
}

function createVideo() {
  Utils.log('QB.createVideo()')

  const video = document.createElement('video')
  video.className = 'video-js vjs-default-skin vjs-big-play-centered vjs-desktop'
  video.crossOrigin = 'anonymous'
  video.preload = 'none'
  video.autoplay = true
  video.controls = false

  if (QB.options.poster) {
    video.poster = QB.options.poster
  }

  const body = document.querySelector('body')
  body.appendChild(video)

  QB.video = video
}

function initializePlayer(player) {
  Utils.log('QB.initializePlayer()')
  QB.Player = player
  return QB.Player.initialize(QB)
}

function initializeChromecast() {
  Utils.log('QB.initializeChromecast() QB.Player.enableChromecast? ', QB.Player.enableChromecast())
  if (QB.Player.enableChromecast()) {
    return new Promise(resolve => {
      Chromecast.on(Chromecast.READY, () => {
        QB.Player.setImplementation(Chromecast.getRemotePlayer())
      })

      Chromecast.on(Chromecast.STATUS_CHANGED, isCasting => {
        if (isCasting) {
          stopContinuousExperience()
        } else {
          startContinuousExperience()
        }
      })

      Chromecast.initialize(QB)
        .then(resolve)
        .catch(err => {
          Utils.error(err)
          resolve()   // we don't care for chromecast init errors
        })
    })
  }
}

function initializeVideoJS() {
  Utils.log('QB.initializeVideoJS()')

  QB.options.vjs = {
    persistTextTrackSettings: true,
    controlBar: {
      volumeMenuButton: {
        inline: false,
        vertical: true
      },
      progressControl: {
        seekBar: {
          mouseTimeDisplay: true
        }
      },
      remainingTimeDisplay: false,
      currentTimeDisplay: true,
      durationDisplay: true
    }
  }

  QB.video.controls = true
  QB.vjs = window.videojs(QB.video, QB.options.vjs)

  if (QB.title) {
    QB.vjs.on('useractive', function () {
      QB.title.style.display = 'block'
    })
    QB.vjs.on('userinactive', function () {
      QB.title.style.display = 'none'
    })
  }

  window.videojs.on(QB.video, 'ready', function () {
    QB.Player.ready()
    if (_.has(QB.Player, 'implementation.addEventListener')) {
      QB.Player.implementation.addEventListener('buffering', function (event) {
        if (event.buffering && !Chromecast.isCasting()) {
          QB.vjs.addClass('vjs-waiting')
        } else {
          QB.vjs.removeClass('vjs-waiting')
        }
      })
    }
  })

  const parent = QB.vjs.el().parentNode
  if (Utils.isEdge()) { parent.className += parent.className + ' ms-edge' }
  if (Utils.isIE())   { parent.className += parent.className + ' ms-ie'   }
}

function initializeYoubora() {
  Utils.log('QB.initializeYoubora()')
  return QB.Player.initializeYoubora(QB)
}

function saveTime(time) {
  Utils.log(`QB.saveTime(${time}) QB.options.time = ${QB.options.time}`)
  if (Utils.wasPageReloaded() && time < QB.options.creditsBegin) {
    QB.options.time = time
  }
}

let continuousExperienceIntervalId = 0

function startContinuousExperience() {
  Utils.log('QB.startContinuousExperience()')
  continuousExperienceIntervalId = setInterval(postCurrentTime, 15000)
}

function stopContinuousExperience() {
  Utils.log('QB.stopContinuousExperience()')
  clearInterval(continuousExperienceIntervalId)
}

function postCurrentTime() {
  if (QB.video && QB.video.duration) {
    ContinuousExperience.setTime(QB.options, QB.video.currentTime, QB.video.duration)
      .catch(function (err) {
        Utils.error('QB.postCurrentTime() ERROR calling ContinuousExperience.setTime()', err)
      })
  }
}

function initializeContinuousExperience() {
  Utils.log('QB.initializeContinuousExperience() QB.options.time:', QB.options.time)

  return new Promise((resolve, reject) => {
    ContinuousExperience.getTime(QB.options)
      .then(saveTime)
      .then(startContinuousExperience)
      .then(resolve)
      .catch(err => {
        // if we can't initialize, it means that we failed to authorize the current session,
        // the solution is to go back from we were loaded, that's the options.link property
        Utils.log(`QB.initializeContinuousExperience() ERROR: ${err}`)
        if (!Config.debug) {
          window.location.href = QB.options.link
        }
        reject(err)
      })
  })
}

import Thumbnails from './plugins/thumbnails'
import Related from './plugins/related'
// ADD MORE PLUGINS HERE

function initializePlugins() {
  Utils.log('QB.initializePlugins()')
  Thumbnails.initialize(QB)
  Related.initialize(QB)
  //ADD more plugins here
}

/** videojs specific components **/
import AudioMenu from './videojs/audio_menu'
import SubtitlesMenu from './videojs/subtitles_menu'
import BackSecondsButton from './videojs/back_seconds_button'
import QubitTech from './videojs/qubit_tech'

function initializeVideoJSComponents() {
  AudioMenu(QB, videojs, QB.Player.Audio)
  SubtitlesMenu(QB, videojs, QB.Player.Subtitles)
  BackSecondsButton(QB, videojs, 30)
  QubitTech(QB, videojs, QB.Player, Chromecast)
}

function loadMedia() {
  Utils.log('QB.loadMedia()')

  let loadPromise = Promise.resolve()
  if (!Chromecast.isCasting()) {
    Utils.log('QB.loadMedia() not casting, loading media')
    const promise = QB.Player.load(QB.options)
    if (promise) {
      loadPromise = promise
    }
  }

  return loadPromise.then(() => QB.Player.notify(QB.Player.LOADED))
}

/**
 * Export QB so it can be tested
 */
window.QB = QB

export default QB
