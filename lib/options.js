/**
 * Player options
 *
 * @module lib/options
 */

/**
 * It transforms an uri hash into a {@link PlayerOptions} object
 *
 * @param {String} hash - The url hash <i>("#....")</i> as an encoded uri component
 *                        that can be transformed into a JSON.
 * @return {Promise} A promise that resolves to an object with
 *                   all options encoded in the hash. The object
 *                   is an instance of {@link PlayerOptions}
 */
function fromUriHash(hash) {
  return new Promise(function (resolve, reject) {
    if (_.size(hash) < 2) {
      return reject(new Error('There is no hash to load'))
    }

    resolve(fromString(LZString.decompressFromEncodedURIComponent(hash.substr(1))))
  })
}

/**
 * It transforms a json string into a {@link PlayerOptions} object
 *
 * @param {String} jsonString - The json string representation of a {@link PlayerOptions} object.
 * @return {PlayerOptions} - The decoded object with defaults already filled in.
 */
function fromString(jsonString) {
  const options = JSON.parse(jsonString)

  if (_.isEmpty(options)) {
    throw new Error('There is no options to load')
  }

  if (_.isEmpty(options.source)) {
    throw new Error('There is no source to load')
  }

  if (!_.isArray(options.source) || options.source.length == 0) {
    throw new Error('options.source must be an array of source objects with at least one object')
  }

  if (!options.operator) {
    options.operator = 'qubit'
  }

  if (!options.transaction) {
    options.transaction = 0
  } else {
    options.transaction = parseInt(options.transaction)
  }

  if (!options.user) {
    options.user = ''
  }

  if (!options.link) {
    options.link = 'javascript:history.back()'
  }

  if (!options.audioLanguage) {
    options.audioLanguage = options.language || document.documentElement.lang || 'en'
  }

  if (!options.subtitleLanguage) {
    options.subtitleLanguage = null   // no subtitles
  }

  if (options.token) {
    options.token = options.token.trim()
    // options.proxyHeaders = options.proxyHeaders || {}
    // options.proxyHeaders.Authorization = 'Bearer ' + options.token
    // options.proxyHeaders['Content-Type'] = 'application/octet-stream'
  }

  if (!options.youbora) {
    options.youbora = {
      accountCode: options.operator,
      media: {
        isLive: false,    //TODO: get as parameter
        title: options.title,
        resource: getSourceUrl(options)
      },
      operator: 'AR', //TODO get as parameter
      playerType: 'shaka',
      transactionCode: options.transaction,
      username: options.user,
      extraParams: {
        param1: options.operator
      }
    }
  }

  return options
}

/**
 * Given a {@link PlayerOptions} and an (optional) language it returns the url of the source to load.
 *
 * @param {PlayerOptions} options - The player options.
 * @param {String} [language] - The language to load or nothing if you want the first source available.
 * @return {String} - The url of the given source
 */
function getSourceUrl(options, language) {
  if (_.isEmpty(options)) {
    throw new Error('options can\'t be empty')
  }

  const sources = options.source
  if (!_.isArray(sources) || sources.length == 0) {
    throw new Error('options.source must be an array of source objects with at least one object')
  }

  for (let i = 0, length = sources.length; i < length; ++i) {
    if (sources[i].lang === language) {
      return sources[i].url
    }
  }

  return sources[0].url
}

export default {
  fromUriHash,
  fromString,
  getSourceUrl
}
