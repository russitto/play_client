import Utils from './utils'

/**
 * Persistent properties for a given object
 *
 * @interface Persistent
 */
export default function Persistent(name = '', storage = window.sessionStorage) {
  /**
   * @private
   */
  const storageKey = name.toLowerCase()

  function key(s) {
    return storageKey + '_' + (s || '').toLowerCase()
  }

  /**
   * Persists the a value of a property, it creates or updates an existing one.
   *
   * @memberof Persistent
   * @param {String} property - The name of the property to update or to create
   * @param {!Object} value - The new or first value of the property
   */
  function save(property, value) {
    Utils.log(`Persistent(${name}).save(${property}, ${value})`)
    storage[key(property)] = value
  }

  /**
   * Returns the current selected language
   *
   * @memberof Persistent
   * @param {String} property - The name of the property to fetch
   * @returns {String} The value of the property saved or null
   */
  function load(property) {
    Utils.log(`Persistent(${name}).load(${property}) => ${storage[key(property)]}`)
    return storage[key(property)]
  }

  return {
    save: save,
    load: load
  }
}
