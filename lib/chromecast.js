import Auth from './auth'
import Observable from './observable'
import Utils from './utils'

/***
 *  REMAINING THINGS:
 *
 *    - Youbora: on chromecast is sending requests, but in local is still sending pings, do we need to stop the local instance?
 */

const CAST_SENDER_URL = '//www.gstatic.com/cv/js/sender/v1/cast_sender.js'
// const CHROMECAST_APP_ID = '4E839F3A' // shaka demo receiver
const CHROMECAST_APP_ID = '3A9615FD' // players receiver players.qubit.tv/receiver
// const CHROMECAST_APP_ID = '63501C05' // players dev receiver players.qubit.tv/receiver-dev

const STATUS_CHANGED = 'caststatuschanged'
const READY = 'ready'

/**
 * Chromecast properties
 */
let proxy = null
let remoteVideo = null
let remotePlayer = null

function isCastAvailable() {
  try {
    return !!(window.chrome && window.chrome.cast && window.shaka && window.shaka.cast && window.shaka.cast.CastProxy)
  } catch (e) {
    return false
  }
}

function canCast() {
  try {
    return !!(window.chrome && window.chrome.cast && window.chrome.cast.isAvailable && proxy && proxy.canCast())
  } catch (e) {
    return false
  }
}

function isCasting() {
  try {
    return !!(proxy && proxy.isCasting())
  } catch (e) {
    return false
  }
}

function initialize(QB) {
  if (!Utils.isChrome()) {
    Utils.log('Chromecast not compatible with your browser')
    return Promise.resolve()
  }

  const Button = videojs.getComponent('Button')

  const ChromecastButton = videojs.extend(Button, {
    constructor: function (player, options) {
      Utils.log('ChromecastButton()')
      options.label = 'Chromecast'
      Button.call(this, player, options)
      this.el_.title = player.localize('Watch on TV')
      proxy.addEventListener('caststatuschanged', this.onCastStatusChanged.bind(this))

      /**
       * the proxy doesn't fire 'caststatuschanged' if we load the player
       * and we were already casting, so we have to fire it by hand
       */
      if (Chromecast.isCasting()) {
        this.onCastStatusChanged()
      }
    },

    onCastStatusChanged: function () {
      Utils.log('ChromecastButton.onCastStatusChanged()')
      if (!Chromecast.isCastAvailable()) {
        this.hide()
        QB.vjs.removeClass('cast-enabled')
        return
      }

      const isCasting = Chromecast.isCasting()
      if (isCasting) {
        this.addClass('chromecast-connected')
      } else {
        this.removeClass('chromecast-connected')
      }

      Chromecast.notify(STATUS_CHANGED, isCasting)
    },

    buildCSSClass: function () {
      return 'vjs-chromecast-button ' + Button.prototype.buildCSSClass.call(this)
    },

    handleClick: function () {
      Button.prototype.handleClick.call(this)

      const button = this.el()
        , player = this.player()
        , paused = player.paused()

      function onCastSuccess() {
        Utils.log(`Chromecast.onCastSuccess() casting to "${proxy.receiverName()}"`)

        const appData = {
          asset: {
            licenseServers: QB.options.widevine
          },
          api: {
            url: QB.options.api,
            token: Auth.getToken(),
            operator: QB.options.operator,
            transaction: QB.options.transaction,
            uuid: QB.options.uuid
          },
          poster: QB.options.poster || '',
          title: QB.options.title || '',
          youbora: QB.options.youbora
        }

        Utils.log('Chromecast.onCastSuccess() setAppData:', appData)
        proxy.setAppData(appData)

        button.disabled = false
        if (paused) {
          player.pause()
        } else {
          player.play()
        }
      }

      function onCastError(error) {
        button.disabled = false
        if (error.code !== window.shaka.util.Error.Code.CAST_CANCELED_BY_USER) {
          Utils.log('Chromecast.onCastError():', error)
        }
        player.play()
      }

      if (Chromecast.isCasting()) {
        proxy.suggestDisconnect()
      } else {
        button.disabled = true
        proxy.cast().then(onCastSuccess, onCastError)
      }
    }
  })

  function waitForChromecastToLoad() {
    Utils.log('Chromecast.waitForChromecastToLoad()')
    let reconnectAttemps = 10

    return new Promise((resolve, reject) => {
      function retry() {
        if (Chromecast.isCastAvailable()) {
          Utils.log('Chromecast initialized')
          return resolve()
        }

        if (reconnectAttemps <= 0) {
          return reject(new Error('Cast APIs not available. Max reconnect attempts'))
        }

        Utils.log('Chromecast not available. Trying to reconnect ' + reconnectAttemps + ' more ' + (reconnectAttemps == 1 ? 'time' : 'times'))
        reconnectAttemps -= 1
        setTimeout(() => retry(resolve, reject), 1000)
      }

      retry()
    })
  }

  function createProxy() {
    Utils.log('Chromecast.createProxy()')

    if (_.isEmpty(QB.options.widevine)) {
      throw new Error('Chromecast.createProxy() ERROR: Can\'t cast without widevine license server')
    }

    proxy = new window.shaka.cast.CastProxy(QB.video, QB.Player.implementation, CHROMECAST_APP_ID)
    remoteVideo = proxy.getVideo()
    remotePlayer = proxy.getPlayer()
  }

  function showButton() {
    Utils.log('Chromecast.showButton()')
    videojs.registerComponent('ChromecastButton', ChromecastButton)

    function onReady() {
      Utils.log('Chromecast.showButton() QB.Player.READY')
      QB.vjs.addClass('cast-enabled')
      QB.vjs.controlBar.addChild('ChromecastButton')
    }

    if (QB.Player.isReady()) {
      onReady()
    } else {
      QB.Player.one(QB.Player.READY, onReady)
    }
  }

  function syncTime() {
    const onTimeupdate = () => {
      if (Chromecast.isCasting()) {
        QB.vjs.trigger('timeupdate')
      }
    }

    QB.Player.one(QB.Player.READY, () =>
      window.videojs.on(remoteVideo, 'timeupdate', onTimeupdate))
  }

  function notifyReady() {
    Chromecast.notify(Chromecast.READY)
  }

  return new Promise((resolve, reject) => {
    Utils.loadScript(CAST_SENDER_URL)
      .then(resolve)
      .then(waitForChromecastToLoad)
      .then(createProxy)
      .then(showButton)
      .then(syncTime)
      .then(notifyReady)
      .catch(reject)
  })
}

const Chromecast = Object.create(Observable(), {
  getRemoteVideo: { value: () => remoteVideo },
  getRemotePlayer: { value: () => remotePlayer },
  isCastAvailable: { value: isCastAvailable },
  canCast: { value: canCast },
  isCasting: { value: isCasting },
  initialize: { value: initialize },
  STATUS_CHANGED: { value: STATUS_CHANGED },
  READY: { value: READY }
})

export default Chromecast
