/**
 * Ued to save and get the playing time
 *
 * @module lib/continuous_experience
 */

import Utils from './utils'
import Auth from './auth'

function getUrl(options) {
  return options.api + '/experience'
}

/**
 * It gets the time in wich the video must start playing.
 *
 * @param {!PlayerOptions} options - The player options
 * @return {Promise} A promise that resolves when we got the time
 *                   to start playing. It can be instantaneously.
 */
function getTime(options) {
  return new Promise((resolve, reject) => {
    if (!options) {
      reject(new Error('ContinuousExperience.getTime(): options can\'t be null or undefined'))
    }

    if (_.isEmpty(options.api)) {
      Utils.log('ContinuousExperience.getTime(): we don\'t have an url configured')
      return reject(new Error('ContinuousExperience.getTime(): we don\'t have an url configured'))
    }

    const data = {
      operator: options.operator,
      uuid: options.uuid,
    }

    const query = Utils.querystring(data)
    const url = getUrl(options) + (query.length ? '?' : '') + query

    const headers = {
      Authorization: 'Bearer ' + Auth.getToken()
    }

    Utils.get(url, headers)
      .then(JSON.parse)
      .then(response => resolve(response.time))
      .catch(reject)
  })
}

/**
 * The last time saved
 *
 * @private
 */
let lastTime = 0

/**
 * It sets the current time of the video
 *
 * @param {!PlayerOptions} options - The player options.
 * @param {Number} time - The current time in which the video is playing.
 * @param {Number} duration - The total duration of the video.
 * @return {Promise} A promise that resolves when we saved the time.
 */
function setTime(options, time, duration) {
  if (!options) {
    throw new Error('ContinuousExperience.setTime(): options can\'t be null or undefined')
  }

  return new Promise((resolve, reject) => {
    if (_.isEmpty(options.api)) {
      reject(new Error('ContinuousExperience.setTime(): options.api can\'t be null, undefined or empty'))
    }

    Utils.log('ContinuousExperience.setTime(time:', time, 'duration:', duration, 'lastTime:', lastTime, ')')

    time = Math.round(time)
    if (time && duration && time != lastTime) {
      lastTime = time

      const data = {
        operator: options.operator,
        uuid: options.uuid,
        repr: options.transaction,
        time: time
      }

      const url = getUrl(options)
      const headers = {
        Authorization: 'Bearer ' + Auth.getToken()
      }

      Utils.post(url, headers, data)
        .then(JSON.parse)
        .then(data => {
          Utils.log('ContinuousExperience.setTime() response:', data)
          if (data.success) {   // because REST is overrated (apparently)
            resolve()
          } else {
            reject()
          }
        })
        .catch(reject)
    } else {
      resolve()
    }
  })
}

export default {
  getTime,
  setTime
}
