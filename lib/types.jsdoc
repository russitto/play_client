/**
 * @typedef HTMLElement
 *
 * @description An HTML element: <code>&lt;div&gt;</code>, <code>&lt;a&gt;</code>,
 * <code>&lt;img&gt;</code>, <code>&lt;p&gt;</code>, etc.
 *
 * @see {@link https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-882764350|W3C HTML Elements}
 */

/**
 * @typedef HTMLVideoElement
 *
 * @description An HTML video element: <code>&lt;video&gt;</code>
 *
 * @see {@link https://dev.w3.org/html5/spec-author-view/video.html#video|W3C video element}
 */

/**
 * @typedef videojs
 *
 * @see {@link http://docs.videojs.com/docs/api/video.html|videojs docs}
 */

/**
 * @typedef PlayerOptions
 *
 * @property {MultilingualResource[]} source - The sources for the media to play. <b><i>required</i></b>
 *
 * @property {MultilingualResource[]} subtitles - The subtitles for the current media. Can be an empty array. <b><i>required</i></b>
 *
 * @property {String} [audioLanguage] - The preferred audio language to use.
 * <br>
 * Default: current html lang
 *
 * @property {String} [subtitleLanguage] - The preferred subtitles language to use.
 * <br>
 * Default: <code>null</code>
 *
 * @property {String} [operator] - The operator used by drm engine and Youbora.
 * <br>
 * Default: <code>"qubit"</code>
 *
 * @property {Number} [transaction] - The transaction id used by Play API and Youbora.
 * <br>
 * Default: <code>0</code>
 *
 * @property {String} [user] - The user used by Play API and Youbora.
 * <br>
 * Default: <code>""</code>
 *
 * @property {String} [link] - An url to go back to when user clicks on title or an error occurs.
 * <br>
 * Default: <code>"javascript:history.back()"</code>
 *
 * @property {String} [title] - The title of the media playing.
 * <br>
 * Default: <code>""</code>
 *
 * @property {YouboraOptions} [youbora] - The configuration for Youbora plugin.
 */

/**
 * @typedef MultilingualResource
 *
 * @property {String} lang - The language of the resource: 'en', 'es, 'multi', etc.
 * @property {String} url - The url of the resource.
 */

/**
 * @typedef YouboraOptions
 *
 * @see {@link https://github.com/NicePeopleAtWork/NicePlayers/blob/master/javascript/Shakaplayer/Example.html|Youbora Shaka Example}
 *
 * @property {String} accountCode
 * @property {String} media.source - The manifest or media file to play.
 * @property {String} operator
 * @property {String} playerType
 * @property {Number} transactionCode
 * @property {String} username
 */

/**
 * @interface QubitPlayer
 * @description A technology used by the player. Current implementations:
 * <ul>
 * <li>{@link QB.DashTech|DASH}</li>
 * <li>{@link QB.HlsTech|HLS}</li>
 * </ul>
 */

/**
 * @member Audio
 * @memberof QubitPlayer
 * @implements {Audio}
 */

/**
 * @member Subtitles
 * @memberof QubitPlayer
 * @implements {Subtitles}
 */

/**
 * @function QubitPlayer#initialize
 * @description This will initialize the technology.
 * @param {PlayerOptions} options - The player options.
 * @param {HTMLVideoElement} video - The video element in wich the media will be played.
 * @return {Promise} A promise that resolves when the instance is initialized.
 */

/**
 * @interface Audio
 * @description Audio management.
 */

/**
 * @function Audio#languages
 * @description Gets all the available languages of audio present in the current media.
 * @return {String[]} An array of all available audio languages i.e: <code>['en', 'es']</code>
 */

/**
 * @function Audio#selectLanguage
 * @description Selects the audio language to play.
 * @param {String} language - One of the elements of the array returned by {@link Audio#languages}
 */

/**
 * @function Audio#currentLanguage
 * @description Get the current selected audio language.
 * @return {String} The current selected audio language.
 */

/**
 * @member Audio#CHANGED
 * @description Changed event. Used to listen and notify when the current language of audio has changed.
 */

/**
 * @interface Subtitles
 * @description Subtitles management.
 */

/**
 * @function Subtitles#languages
 * @description Gets all the available languages of subtitles present in the current media.
 * @return {String[]} An array of all available subtitles languages i.e: <code>['en', 'es']</code>
 */

/**
 * @function Subtitles#selectLanguage
 * @description Selects the subtitles language to show.
 * @param {String} language - One of the elements of the array returned by {@link Subtitles#languages}
 */

/**
 * @function Subtitles#currentLanguage
 * @description Get the current selected subtitle language.
 * @return {String} The current selected subtitle language.
 */

/**
 * @member Subtitles#CHANGED
 * @description Changed event. Used to listen and notify when the current language of subtitles has changed.
 */
