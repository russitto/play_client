/**
 * A collection of general use functions
 *
 * @module lib/utils
 */

import Config from './config'

/**
 * Synonim of {@link https://developer.mozilla.org/en/docs/Web/API/Console/log|console.log},
 * only if {@link Config.debug} is enabled.
 */
function log(...args) {
  if (Config.debug) {
    console.log.apply(console, args)  // eslint-disable-line no-console
  }
}

const _errorFunction = console.error ? console.error : console.log // eslint-disable-line no-console

/**
 * Synonim of {@link https://developer.mozilla.org/en/docs/Web/API/Console/error|console.error}
 * if available, {@link https://developer.mozilla.org/en/docs/Web/API/Console/log|console.log} otherwise.
 */
function error(...args) {
  _errorFunction.apply(console, args)
}

/**
 * Loads a style on the page.
 *
 * @param {!String} url - The url of the style to load.
 * @return {HTMLElement} The link element attached to document's head.
 */
function loadStyle(url) {
  log('Utils.loadStyle("' + url + '")')

  if (_.isEmpty(url)) {
    throw new Error('Utils.loadStyle(): url can\'t be null, undefined or empty')
  }

  const style = document.createElement('link')
  style.rel = 'stylesheet'
  style.href = url

  const head = document.querySelector('head')
  head.appendChild(style)

  return style
}

/**
 * Loads a script on the page.
 *
 * @param {!String} url - The url of the script to load.
 * @param {Function} [onLoad] - A function of the form <code>function onLoad(element) {}</code><br>
 *                              Called when the script was succesfully loaded.
 * @param {Function} [onError] - A function of the form <code>function onError(error) {}</code><br>
 *                               Called when the script could not be loaded.
 * @return {HTMLElement} The script element attached to document's head.
 */
function loadScriptAsync(url, onLoad, onError) {
  log('Utils.loadScriptAsync("' + url + '")')

  if (_.isEmpty(url)) {
    onError && onError(new Error('Utils.loadScriptAsync(): url can\'t be null, undefined or empty'))
    return null
  }

  const script = document.createElement('script')
  script.src = url

  const head = document.querySelector('head')
  head.appendChild(script)

  function clean() {
    script.removeEventListener('load', onScriptLoaded)
    script.removeEventListener('error', onScriptLoadError)
  }

  function onScriptLoaded() {
    log('Utils.loadScriptAsync(): "' + url + '" loaded')
    clean()
    onLoad(script)
  }

  function onScriptLoadError() {
    error('Utils.loadScriptAsync(): "' + url + '" loading error')
    clean()
    onError(new Error('Failed to load ' + url))
  }

  if (onLoad) {
    script.addEventListener('load', onScriptLoaded)
  }

  if (onError) {
    script.addEventListener('error', onScriptLoadError)
  }

  return script
}

/**
 * Loads a script on the page.
 *
 * @param {String} url - The url of the script to load
 * @return {Promise} A promise that resolve to an {@link https://www.w3schools.com/jsref/dom_obj_all.asp|HTMLElemnt}
 */
function loadScript(url) {
  return new Promise(function (resolve, reject) {
    loadScriptAsync(url, resolve, reject)
  })
}

/**
 * Loads multiple scripts on the page, sequantially
 *
 * @param {Stringp[]} urls - The list of urls to load in order
 * @return {Promise} A promise that resolve when the last url was loaded
 */
function loadScripts(urls) {
  log('Utils.loadScripts([' + urls + '])')

  return new Promise(function (resolve, reject) {
    if (!_.isArray(urls)) {
      return reject(new Error('Utils.loadScripts(): urls must be an array of strings'))
    }

    if (_.isEmpty(urls)) {
      return reject(new Error('Utils.loadScripts(): urls can\'t be null, undefined or empty'))
    }

    (function load([url, ...rest]) {
      if (!_.isString(url)) {
        return reject(new Error('Utils.loadScripts(): url must be a string'))
      }

      if (_.isEmpty(url)) {
        return reject(new Error('Utils.loadScripts(): url can\'t be null, undefined or empty'))
      }

      let callback = resolve
      if (!_.isEmpty(rest)) {
        callback = load.bind(null, rest)
      }

      loadScriptAsync(url, callback, reject)
    })(urls)
  })
}

/**
 * Transforms an array like object into an array. For this to work, the object must have:
 * <ul>
 *  <li>An integer index accessor: <code>object[index]</code>
 *  <li>A length property: <code>object.length</code>
 * </ul>
 *
 * @example
 * var object = {0: 'first', 1: 'second', 3: 'third', length: 3}
 * var array = Utils.toArray(object)
 * // array is ['first', 'second', 'third']
 *
 * @param {!Object} object - Any array like object
 * @return {Object[]} A native javascript array with all the elements of the original object
 */
function toArray(object) {
  return Array.prototype.slice.call(object, 0)
}

/**
 * Bypass the value returned by a promise to the next one
 *
 * @example
 * var first = new Promise(function (resolve) { resolve("first") })
 * var second = new Promise(function (resolve) { resolve("second") })
 * var log = console.log.bind(console)
 *
 * first
 *   .then(second)
 *   .then(log)     // ouputs "second"
 *
 * first
 *   .then(Utils.bypass(second))
 *   .then(log)     // outputs "first"
 *
 * @param {!Function} fn - A function that returns a value or a Promise. This will be called and bypassed.
 * @param {...*} args - Extra arguments passed to the function.<br>
 *                      As <code>fn.apply(null, args)</code>
 * @return {Function} A function that returns its own argument when the promise resolves
 */
function bypass(fn, ...args) {
  if (!_.isFunction(fn)) {
    throw new Error('Utils.bypass(): fn must be a function')
  }

  if (!fn) {
    throw new Error('Utils.bypass(): fn can\'t be null or undefined')
  }

  return function (argument) {
    if (_.isEmpty(args)) {
      args = [argument]
    }
    return Promise.resolve(fn.apply(null, args)).then(() => argument)
  }
}

/**
 * Check if the page was reloaded
 *
 * @return {Boolean} <code>true</code> if the page was reloaded, <code>false</code> otherwise
 */
function wasPageReloaded() {
  return !(window.performance && window.performance.navigation.type != 1)
}

/**
 * Performs an HTTP request.
 *
 * @private
 * @param {!String} url - The url to perform the requesst to.
 * @param {!String} method - The method to use: POST, GET, etc.
 * @param {?Object} headers - Extra headers to add to the request.
 * @param {?Object} data - The data to send on the request body.
 * @return {Promise} A promise that resolves to the responseText of the request
 */
function request(url, method, headers, data) {
  log('Utils.request("' + url + '", ' + method + ', ' + JSON.stringify(headers) + ', ' + JSON.stringify(data) + ')')

  return new Promise(function (resolve, reject) {
    if (_.isEmpty(url)) {
      return reject(new Error('Utils.request(' + method + '): url can\'t be null, undefined or empty'))
    }

    if (!_.isString(url)) {
      return reject(new Error('Utils.request(' + method + '): url must be a string'))
    }

    if (_.isEmpty(method)) {
      return reject(new Error('Utils.request(' + method + '): method can\'t be null, undefined or empty'))
    }

    if (!_.isString(method)) {
      return reject(new Error('Utils.request(' + method + '): method must be a string'))
    }

    const xhr = new XMLHttpRequest()

    function clean() {
      xhr.removeEventListener('error', onError)
      xhr.removeEventListener('abort', onError)
      xhr.removeEventListener('load', onLoad)
    }

    function onError(err) {
      log('Utils.request(' + method + ', ' + url + '): rejecting with error')
      clean()
      reject(new Error(err))
    }

    function onLoad() {
      clean()
      const status = xhr.status
      if (status >= 200 && status <= 299) {
        log(`Utils.request(${url}, ${method}) resolving with ${status} :: ${(xhr.responseText || '').substring(0, 50)}`)
        resolve(xhr.responseText)
      } else {
        log(`Utils.request(${url}, ${method}) rejecting with ${status} :: ${(xhr.responseText || '').substring(0, 50)}`)
        reject(xhr.responseText)
      }
    }

    xhr.addEventListener('error', onError)
    xhr.addEventListener('abort', onError)
    xhr.addEventListener('load', onLoad)
    xhr.open(method, url, true)

    headers = headers || []

    // use application/json by default but if we got null then do not pass Content-Type
    if (headers['Content-Type'] !== null) {
      headers['Content-Type'] = 'application/json'
    }

    Object.keys(headers)
      .forEach(name => {
        const value = headers[name]
        if (value) {
          xhr.setRequestHeader(name, value)
        }
      })

    if (_.isEmpty(data)) {
      xhr.send()
    } else {
      xhr.send(JSON.stringify(data))
    }
  })
}

/**
 * Performs an HTTP GET request.
 *
 * @function get
 * @param {!String} url - The url to perform the request to.
 * @param {?Object} headers - Extra headers to add to the request.
 * @return {Promise} A promise that resolves to the responseText of the request
 */
const get = function (url, headers) {
  return request(url, 'GET', headers, null)
}

/**
 * Performs an HTTP POST request.
 *
 * @function post
 * @param {!String} url - The url to perform the request to.
 * @param {?Object} headers - Extra headers to add to the request.
 * @param {?Object} data - The data to send in the request.
 * @return {Promise} A promise that resolves to the responseText of the request
 */
const post = function (url, headers, data) {
  return request(url, 'POST', headers, data)
}

/**
 * Returns the base of a given url
 *
 * @example
 * Utils.getBaseUrl('http://dom.ain/folder/file.html')      // output 'http://dom.ain/folder/'
 * Utils.getBaseUrl('http://dom.ain/folder/sub/file.html')  // output 'http://dom.ain/folder/sub/'
 * Utils.getBaseUrl('//dom.ain/folder/sub/file.html')       // output '//dom.ain/folder/sub/'
 * Utils.getBaseUrl('http://dom.ain/file.html')             // output 'http://dom.ain/'
 * Utils.getBaseUrl('http://dom.ain/file.html?param=1')     // output 'http://dom.ain/'
 * Utils.getBaseUrl('http://dom.ain/file.html#hash')        // output 'http://dom.ain/'
 * Utils.getBaseUrl('http://dom.ain')                       // output 'http://dom.ain/'
 * Utils.getBaseUrl('invalid-url')                          // Error
 *
 * @param {!String} url - The given url.
 * @return {String} The base of the given url.
 */
function getBaseUrl(url) {
  if (_.isEmpty(url)) {
    throw new Error('Utils.getBaseUrl(): url can\'t be null, undefined or empty')
  }

  //TODO: use an url parser
  const index = url.lastIndexOf('/')
  if (index < 0) {
    throw new Error('Utils.getBaseUrl(): invalid url "' + url + '"')
  }

  if (url[index - 1] == '/') { // we found an url with only [protocol]://[domain]
    return url + '/'
  }

  return url.substr(0, index + 1)
}

/**
 * Removess all the HTMLElemnt's from its own parents.
 *
 * It uses <code>element.querySelectorAll(selector)</code> to select the elements to remove.
 *
 * @param {!HTMLElement} element - The root element to start the search of the elements to remove.
 * @param {!String} selector - The selector to use to filter elements to remove.
 */
function removeAll(element, selector) {
  if (!element) {
    throw new Error('Utils.removeAll(): element can\'t be null or undefined')
  }

  toArray(element.querySelectorAll(selector))
    .forEach(child => child.parentElement.removeChild(child))
}

/**
 * Transform a map of values to an encoded query string. It will always
 * return a string even if the object is invalid.
 *
 * The object can have any structure, but is recommended to be a flat key:value map
 * without any nested structures. The nested objects will not be processed correctly.
 *
 * @param {Object} object - The key/value map to encode
 * @return {String} A string of the form key1=value1&key2=value2&...
 *
 * @see https://tools.ietf.org/html/rfc3986#section-3.4
 */
function querystring(object) {
  if (!_.isObject(object) || _.isEmpty(object) || _.isArray(object)) {
    return ''
  }

  return Object.keys(object)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(object[key]))
    .join('&')
}

/**
 * Finds the position of an {@link HTMLElement} on the page.
 *
 * @param {HTMLElement} element - The element to find it's position on the page
 * @return {Object} The position of the element in the page as <code>{left: x, top: y}</code>
 */
function findElementPosition(element) {
  if (!element.getBoundingClientRect || !element.parentNode) {
    return {
      left: 0,
      top: 0
    }
  }

  const box = element.getBoundingClientRect()
  const body = document.body

  const clientLeft = document.documentElement.clientLeft || body.clientLeft || 0
  const scrollLeft = window.pageXOffset || body.scrollLeft
  const left = box.left + scrollLeft - clientLeft

  const clientTop = document.documentElement.clientTop || body.clientTop || 0
  const scrollTop = window.pageYOffset || body.scrollTop
  const top = box.top + scrollTop - clientTop

  // Android sometimes returns slightly off decimal values, so need to round
  return {
    left: Math.round(left),
    top: Math.round(top)
  }
}

/**
 * Create HTML element
 *
 * @param el - type of HTML element
 * @param elClass - element class name
 * @param id - element id name (optional)
 * @return - HTML element with class and id(optional)
 */
function createEl(el, elClass, id) {
  const tempEl = document.createElement(el)
  tempEl.className = elClass
  if (id ) {
    tempEl.setAttribute('id', id)
  }
  return tempEl
}

/**
 * Get HTML element by id
 *
 * @param id - element id name
 * @return - HTML element whit provided id
 */
function getElById(id) {
  return document.getElementById(id) 
}

/**
 * Get HTML element by Class
 *
 * @param elClass - element class name
 * @return - HTML element with provided class
 */
function getElByClass(elClass) {
  return document.getElementsByClassName(elClass) 
}

/**
 * Show HTML Element by class name
 *
 * @param el - HTML element 
 * @param className - element class name
 * @return - showed HTML element
 */
function show(el, className) {
  return el.classList.remove(className)
}

/**
 * Hide HTML Element by class name
 *
 * @param el - HTML element 
 * @param className - element class name
 * @return - hided HTML element
 */
function hide(el, className) {
  return el.classList.add(className)
}


const browser = {
  Unknown: 'browser unknown',
  Edge: 'Edge',
  Firefox: 'Firefox',
  Opera: 'Opera',
  IE: 'Internet Explorer',
  Chrome: 'Google Chrome',
  Safari: 'Safari'
}

function getBrowser() {
  var ua = navigator.userAgent
  if (ua.indexOf(' Edge/') >= 0)    { return browser.Edge }
  if (ua.indexOf(' Firefox/') >= 0) { return browser.Firefox }
  if (ua.indexOf(' OPR/') >= 0)     { return browser.Opera }
  if (ua.indexOf(' Trident/') >= 0) { return browser.IE }
  if (ua.indexOf(' Chrome/') >= 0)  { return browser.Chrome }
  if (ua.indexOf(' Safari/') >= 0)  { return browser.Safari }
  return browser.Unknown
}

export default {
  log,
  error,
  loadStyle,
  loadScriptAsync,
  loadScript,
  loadScripts,
  toArray,
  bypass,
  wasPageReloaded,
  get,
  post,
  getBaseUrl,
  removeAll,
  querystring,
  findElementPosition,
  createEl,
  getElById,
  getElByClass,
  show,
  hide,

  isEdge:   () => getBrowser() === browser.Edge,
  isIE:     () => getBrowser() === browser.IE,
  isChrome: () => getBrowser() === browser.Chrome
}
