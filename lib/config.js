/**
 * Configuration of the client
 *
 * @module lib/config
 */

/**
 * A flag to know if we are in debug mode or not.
 * This enables or disables output of some messages.
 * <br>
 * In production release set to <code>false</code>.
 *
 * @member {Boolean} debug
 */
const debug = true

/**
 * @private
 */
const scriptSuffix = (debug ? '.js' : '.min.js')

/**
 * URL of qubit services api.
 *
 * Has the form <b>domain</b>/index.php/<b>version</b>/<b>datatype</b>/<b>apkid</b> where:
 *
 * <ul>
 *   <li><b>domain</b> is the url to the service (staging, prod, etc)</li>
 *   <li><b>version</b> is the version of the api to use: 'v1', 'v2', etc.</li>
 *   <li><b>datatype</b> is the type of the response, we will mostly use <code>json</code></li>
 *   <li><b>apkid</b> is the unique id of the "application" issuing the request</li>
 * </ul>
 *
 * @private
 */
// const qubitApiUrl = '//servicesv5.qbst.com.ar/index.php/v2/json/9088-697E'  // staging
const qubitApiUrl = '//services.qubit.tv/index.php/v2/json/9088-697E'  // prod

/**
 * All styles used by the player. Each url must be relative to the html file or absolute.
 *
 * @member {String[]} styles
 */
const styles = [
  '//vjs.zencdn.net/5.16.0/video-js.min.css',
  '../dist/qubit.css'
]

/**
 * Shaka urls
 *
 * @member {Object} Dash
 * @member {String} Dash.lib
 * @member {String} Dash.youbora
 * @see https://github.com/google/shaka-player
 */
const Dash = {
  lib: '//cdnjs.cloudflare.com/ajax/libs/shaka-player/2.1.8/shaka-player.compiled.js',
  youbora: '//smartplugin.youbora.com/v5/javascript/videojs5/5.3.8/sp.min.js'
}

/**
 * HLS urls
 *
 * @member {Object} HLS
 * @member {String} HLS.lib
 * @member {String} HLS.youbora
 * @see https://github.com/video-dev/hls.js
 */
const HLS = {
  lib: '//cdn.jsdelivr.net/hls.js/0.7.6/hls' + scriptSuffix,
  youbora: '//smartplugin.youbora.com/5.0/js/hlsjs/5.3.0/sp.min.js'
}

/**
 * URL of Qubit CLS service, used to know if we can play a given content for the given user
 *
 * @member {String} qubitClsService
 */
const qubitClsService = qubitApiUrl + '/commercial/cls'

/**
 * List of available languages. This will be loaded to videojs interface.
 *
 * @member {String[]} languages
 */
const languages = ['es']

export default {
  debug,
  styles,
  Dash,
  HLS,
  qubitClsService,
  languages
}
