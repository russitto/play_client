import Utils from '../utils'

export default function (QB, videojs, seconds) {
  const Component = videojs.getComponent('Component')
    , MenuButton = Component.getComponent('MenuButton')

  const BackSecondsButton = videojs.extend(MenuButton, {
    constructor: function (player, options) {
      Utils.log('BackSecondsButton()')
      MenuButton.call(this, player, options)
      this.seconds_ = options.seconds
      this.el().title = `-${this.seconds_}"`
    },

    buildCSSClass: function () {
      return 'vjs-icon-replay ' + MenuButton.prototype.buildCSSClass.call(this)
    },

    handleClick: function (event) {
      Utils.log('BackSecondsButton.handleClick()')
      MenuButton.prototype.handleClick.call(this, event)
      const player = this.player()
      player.currentTime(player.currentTime() - this.seconds_)
    }
  })

  Component.registerComponent('BackSecondsButton', BackSecondsButton)

  QB.Player.on(QB.Player.LOADED, () => {
    Utils.log('BackSecondsButton() QB.Player.LOADED')
    QB.vjs.controlBar.addChild('BackSecondsButton', {
      seconds: seconds
    })
  })
}
