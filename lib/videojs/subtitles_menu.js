import Utils from '../utils'
import Chromecast from '../chromecast'

export default function (QB, videojs, subtitles) {
  const MenuItem = videojs.getComponent('MenuItem')
    , Component = videojs.getComponent('Component')
    , MenuButton = videojs.getComponent('MenuButton')

  const QubitSubtitlesButton = videojs.extend(MenuButton, {
    constructor: function (player, options) {
      options.label = options.lang
      MenuButton.call(this, player, options)
    },

    buildCSSClass: function () {
      return 'vjs-subtitles-button ' + MenuButton.prototype.buildCSSClass.call(this)
    },

    createItems: function () {
      const player = this.player()
      const langs = subtitles.languages()
      Utils.log('QubitSubtitlesMenuItem.createItems() langs:', langs)
      // position 0 -> subtitles off
      if (langs.length < 2) {
        return []
      }
      return subtitles.languages().map(language =>
        new QubitSubtitlesMenuItem(player, {
          language: language,
          selectable: true
        })
      )
    }
  })

  Component.registerComponent('SubtitlesButton', QubitSubtitlesButton)

  const QubitSubtitlesMenuItem = videojs.extend(MenuItem, {
    constructor: function(player, options) {
      this.language = options.language
      options.label = this.language || 'Unknown'
      options.selected = subtitles.currentLanguage() == this.language

      MenuItem.call(this, player, options)

      subtitles.on(subtitles.CHANGED, language => {
        Utils.log('QubitSubtitlesMenuItem subtitles.CHANGED to', language)
        this.selected(this.language == language)
        updateTracks(language)
      })
    },

    handleClick: function (event) {
      MenuItem.prototype.handleClick.call(this, event)
      const language = this.language
      subtitles.selectLanguage(language)
    }
  })

  Chromecast.on(Chromecast.STATUS_CHANGED, () => updateTracks(subtitles.currentLanguage()))

  function updateTracks(language) {
    Utils.log(`SubtitlesButton updateTracks(${language}) casting? ${Chromecast.isCasting()}`)
    const mode = Chromecast.isCasting() ? 'disabled' : 'showing'
    Utils.toArray(QB.vjs.textTracks()).forEach(track =>
      track.mode = ((track.language == language) ? mode : 'disabled'))
  }

  QubitSubtitlesButton.prototype.controlText_ = 'Subtitles'

  const subtitlesAvailable = QB.options.subtitles.length

  if (subtitlesAvailable == 0) {
    Utils.log('SubtitlesMenu.initializeSubtitles(): no subtitles, no subtitles button')
  } else {
    Utils.log(`SubtitlesMenu.initializeSubtitles(): ${subtitlesAvailable} languages, showing subtitles button`)
    Utils.removeAll(QB.video, 'track')
    QB.options.subtitles.forEach(subtitle => {
      Utils.log('SubtitlesMenu.initializeSubtitles() adding', JSON.stringify(subtitle))
      const track = document.createElement('track')
      track.setAttribute('src', subtitle.url)
      track.setAttribute('srclang', subtitle.lang)
      track.setAttribute('lang', subtitle.lang)
      track.setAttribute('label', subtitle.lang)
      track.setAttribute('type', 'subtitles')
      track.setAttribute('kind', 'subtitles')
      QB.video.appendChild(track)
    })
  }

  QB.Player.on(QB.Player.LOADED, () => {
    Utils.log('SubtitlesMenu.initializeSubtitles() QB.Player.LOADED')
    subtitles.initialize(QB.options.subtitleLanguage)
  })
}
