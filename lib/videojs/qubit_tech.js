import Utils from '../utils'

export default function (QB, videojs, QubitPlayer, Chromecast) {
  const Html5 = videojs.getComponent('Html5')

  function getVideo() {
    if (Chromecast.canCast()) {
      return Chromecast.getRemoteVideo()
    }

    return QB.video
  }

  const QubitTech = videojs.extend(Html5, {
    constructor: function (options, ready) {
      options.el = QB.video

      Html5.call(this, options, ready)

      this.castVolume_ = _.debounce(function (level, muted) {
        getVideo().volume = muted ? 0 : level
        getVideo().muted = muted
      }.bind(this), 200)

      this.castCurrentTime_ = _.debounce(function (time) {
        getVideo().currentTime = time
      }.bind(this), 200)

      this.player().on('playing', function () {
        Utils.log('QubitTech() player is playing')
        if (options.autoplay) {
          this.play()
        }
      }.bind(this))

      QubitPlayer.on(QubitPlayer.LOADED, function () {
        Utils.log('QubitTech() player is loaded')
        if (Chromecast.isCasting()) {
          this.player().trigger('playing')
        }
      }.bind(this))

      /**
       * This is a relatively small hack for youbora videojs plugin.
       * It's plugin expect videojs instance to have a tech available
       * and given tech must have one of the following properties:
       *
       *  "hls" when using videojs-contrib-hls
       *  "hls_" when using hls.js
       *  "shakaPlayer" when using Shaka Player
       *
       * So every QubitPlayer must have a name and an implementation to set
       * this up
       */
      this[QubitPlayer.name] = QubitPlayer.implementation
    },

    name: function () {
      return 'videojs-qubit-tech'
    },

    play: function () {
      Utils.log('QubitTech.play() casting?', Chromecast.isCasting())
      this.paused_ = false
      if (Chromecast.isCasting()) {
        getVideo().play()
        this.trigger('play')
      } else {
        Html5.prototype.play.call(this)
      }
    },

    pause: function () {
      Utils.log('QubitTech.pause() casting?', Chromecast.isCasting())
      this.paused_ = true
      if (Chromecast.isCasting()) {
        getVideo().pause()
        this.trigger('pause')
      } else {
        Html5.prototype.pause.call(this)
      }
    },

    paused: function () {
      return this.paused_
    },

    currentTime: function () {
      if (getVideo().currentTime == 0) {
        return this.currentTime_ || 0
      }
      return this.currentTime_ = getVideo().currentTime
    },

    setCurrentTime: function (time) {
      Utils.log(`QubitTech.setCurrentTime(${time}) casting?`, Chromecast.isCasting())
      if (Chromecast.isCasting()) {
        this.currentTime_ = time
        this.castCurrentTime_(this.currentTime_)
        this.trigger('timeupdate')
      } else {
        Html5.prototype.setCurrentTime.call(this, time)
      }
    },

    volume: function () {
      return this.volume_
    },

    setVolume: function (level, muted) {
      Utils.log(`QubitTech.setVolume(${level}, ${muted}) casting?`, Chromecast.isCasting())
      this.volume_ = level
      this.muted_ = muted
      if (Chromecast.isCasting()) {
        this.castVolume_(this.volume_, this.muted_)
        this.trigger('volumechange')
      } else {
        Html5.prototype.setVolume.call(this, level, muted)
      }
    },

    muted: function () {
      return this.muted_
    },

    setMuted: function (muted) {
      Utils.log(`QubitTech.setMuted(${muted}) casting?`, Chromecast.isCasting())
      this.muted_ = muted
      if (Chromecast.isCasting()) {
        this.castVolume_(this.volume_, this.muted_)
        this.trigger('volumechange')
      } else {
        Html5.prototype.setMuted.call(this, muted)
      }
    },

    duration: function () {
      Utils.log('QubitTech.duration() casting?', Chromecast.isCasting())
      if (Chromecast.isCasting()) {
        return getVideo().duration
      }

      return Html5.prototype.duration.call(this)
    },

    supportsFullScreen: function () {
      Utils.log('QubitTech.supportsFullScreen() casting?', Chromecast.isCasting())
      if (Chromecast.isCasting()) {
        return false
      }
      return Html5.prototype.supportsFullScreen.call(this)
    },

    enterFullScreen: function () {
      Utils.log('QubitTech.enterFullScreen() casting?', Chromecast.isCasting())
      if (!Chromecast.isCasting()) {
        return Html5.prototype.enterFullScreen.call(this)
      }
    }
  })

  /**
   * This tech is always supported.
   * (Not really, but is a proxy, we want this tech to be always available)
   */
  QubitTech.isSupported = () => true

  /**
   * This tech can play anything.
   * (Not really, but is a proxy, we want this tech to be always available)
   */
  QubitTech.canPlaySource = () => true

  /**
   * If this is false, manual `timeupdate` events will be triggred
   */
  QubitTech.featuresTimeupdateEvents = false

  /**
   * We must register this tech as a videojs Component to work.
   */
  const Component = videojs.getComponent('Component')
  Component.registerComponent('QubitTech', QubitTech)

  /**
   * We must register this tech as a videojs Tech to work.
   */
  const Tech = videojs.getComponent('Tech')
  Tech.registerTech('QubitTech', QubitTech)

  /**
   * videojs.options.techOrder is an array of names of
   * possible techs to load, overriding it to load only
   * this tech allow us to use this tech as a proxy to
   * the real player, using videojs just as a UI component
   */
  videojs.options.techOrder.unshift('QubitTech')
}
