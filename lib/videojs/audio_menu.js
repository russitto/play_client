import Utils from '../utils'

export default function (QB, videojs, audio) {
  const MenuItem = videojs.getComponent('MenuItem')
    , Component = videojs.getComponent('Component')
    , AudioTrackButton = videojs.getComponent('AudioTrackButton')

  const QubitAudioTrackButton = videojs.extend(AudioTrackButton, {
    constructor: function (player, options) {
      Utils.log('QubitAudioTrackButton()')
      options.label = options.lang
      AudioTrackButton.call(this, player, options)
    },

    createItems: function () {
      Utils.log('QubitAudioTrackButton.createItems(), languages:', audio.languages())
      const player = this.player()
      const languages = audio.languages()
      if (languages.length < 2) {
        return []
      }
      return languages.map(language =>
        new QubitAudioTrackMenuItem(player, {
          language: language,
          selectable: true
        })
      )
    }
  })

  Component.registerComponent('AudioTrackButton', QubitAudioTrackButton)

  const QubitAudioTrackMenuItem = videojs.extend(MenuItem, {
    constructor: function(player, options) {
      this.language = options.language
      Utils.log('QubitAudioTrackMenuItem(), language:', this.language)
      options.label = this.language || 'Unknown'
      options.selected = (audio.currentLanguage() == this.language)

      MenuItem.call(this, player, options)

      audio.on(audio.CHANGED, language => this.selected(this.language == language))
    },

    handleClick: function (event) {
      Utils.log('QubitAudioTrackMenuItem.handleClick(), language:', this.language)
      MenuItem.prototype.handleClick.call(this, event)
      audio.selectLanguage(this.language)
    }
  })

  QB.Player.on(QB.Player.LOADED, () => {
    Utils.log('AudioMenu.initializeAudio() QB.Player.LOADED')
    audio.initialize(QB.options.audioLanguage)
  })
}
