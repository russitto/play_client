import Utils from '../utils'
import Auth from '../auth'

export default { 
  initialize: function (QB) {
    const registerPlugin = videojs.registerPlugin || videojs.plugin
    const suggestedVideoEndcap = function(options) {
      const player = this
      let creditsBegin = options.creditsBegin
      let timerId
      let content
      let timeoutLoop

      // Process related or nextEpisode based in contentType 
      processByContentType(options)
      
      function processByContentType(options) {
        // Log when serie_chapters_tree return empty from api Play call to apiVOD content/detail 
        if ( options.contentType == 'Serie' && _.isEmpty(options.serieUid) ) {
          Utils.error('*** SERIE UID NOT FOUND *** RECOMM CANCELED ***')
          return true
        } 
        if (options.contentType != 'Serie') {
          return createScreenRelated(options)
        }
        return creditsBeginLoop()
      } 
      
      function creditsBeginLoop(){
        timeoutLoop = setTimeout(askForEndCredits, 15000)
        return timeoutLoop
      }

      function createScreenRelated(options) {
        relatedContent(options).then( data => {
          return createRelatedBase(options, data) 
        }).then( () => {
          if (options.contentType == 'Serie') {
            timerId = timer(content)
            showRelated()
          }
        })
      }
      
      function relatedContent(options) {
        if (!options) {
          throw new Error('related.relatedContent(): options can\'t be null or undefined')
        }
        
        return new Promise( (resolve, reject) => {
          const data = {
            operator: options.operator,
            uuid: options.uuid,
            fieldset: 'all',
            contentType: options.contentType
          }
          
          if ( options.contentType == 'Serie') {
            data.serieUid = options.serieUid
          }
          
          const headers = {
            Authorization: 'Bearer ' + Auth.getToken()
          }
          
          const url = options.api + '/recomm'
          Utils.post( url, headers, data)
            .then(JSON.parse)
            .then((data) => {
              if (data.recomm.length > 0) {
                return resolve(data)
              } else {
                return reject()
              }
            })
            .catch(reject)
        })
      }
    
      function timer(content) {
        clearTimeout(timeoutLoop)
        let seconds = 20
        const stringSec = player.localize(' seconds')
        const timerId = setInterval( function() {
          Utils.getElById('epTimer').innerHTML = seconds + stringSec
          seconds--
          if (seconds == 0) {
            hideRelated()
            clearInterval(timerId)           
            playContent(options, content)
          }
        },1000)
        return timerId
      }
      
      function createRelatedBase(options, data) {
        const _frag = document.createDocumentFragment()
        const _aside = Utils.createEl('aside', 'related-screen nodisplay', 'related_screen')
        asideEventListener(_aside) 
        
        const _relatedClose = Utils.createEl('div', 'related-close-container')
        const _closeRelatedButton = Utils.createEl('a', 'big-related-round-button', 'close')
        _closeRelatedButton.innerHTML = 'X'
        _relatedClose.appendChild(_closeRelatedButton)
        
        const _relatedHeader = Utils.createEl('div', 'related-header')
        _relatedHeader.innerHTML = player.localize('Continue watching')
        
        const _relatedScreen = createRelatedScreen(data)
        
        const _relatedFooter = Utils.createEl('a', 'related-footer') 
        _relatedFooter.innerHTML = player.localize('Return to home')
        _relatedFooter.href = options.homeCatalog
        _relatedFooter.target = '_self'

        _aside.appendChild(_relatedClose)
        _aside.appendChild(_relatedHeader)
        _aside.appendChild(_relatedScreen)
        _aside.appendChild(_relatedFooter)

        _frag.appendChild(_aside)
        player.el().appendChild(_frag)
        
        creditsBeginLoop()
      }
      
      function asideEventListener(_aside) {
        _aside.addEventListener('click', function(event) {
          
          if ( event.target.id == 'close') {
            if (options.contentType == 'Serie') {
              clearInterval(timerId)
            }
            Utils.hide(_aside, 'nodisplay')
            player.play()
          }
          
          if (event.target.id == 'posterImage1') {
            const _poster2 = Utils.getElById('posterImage0')
            const outline = _poster2.style.outline
            if ( outline != 'unset') {
              _poster2.style.outline = 'unset'
              toggleVisibility('detailsRight', event.target.id)
            }
            toggleVisibility('detailsLeft', event.target.id)
          }
          
          if (event.target.id == 'posterImage0') {
            const _poster1 = Utils.getElById('posterImage1')
            const outline = _poster1.style.outline
            if (outline != 'unset') {
              _poster1.style.outline = 'unset'
              toggleVisibility('detailsLeft', event.target.id)
            }
            toggleVisibility('detailsRight', event.target.id)
          }
        }, false)
      
      }
      
      function toggleVisibility(detailsId, posterId) {
        const _details = Utils.getElById(detailsId) 
        const _poster = Utils.getElById(posterId) 

        if (_details) {
          const visibility = _details.style.visibility
          if ( visibility == 'hidden') {
            _poster.style.outline = '1px solid #ff6d00'
            return _details.style.visibility = 'visible' 
          } else {
            _poster.style.outline = 'unset'
            return _details.style.visibility = 'hidden' 
          }
        }
      }

      function createEpisodeHeader(episode) {
        const _nextEpisodeTimer = Utils.createEl('span', 'episode-timer', 'epTimer')
        _nextEpisodeTimer.innerHTML = _.template(player.localize('<%=seconds%> seconds'))({seconds: 20})
        const _divEpisodeHeader = Utils.createEl('div', 'episode-header', 'epHeader')
        const _nextEpisodeTitle = Utils.createEl('span', 'episode-title', 'epTitle')
        _nextEpisodeTitle.innerHTML = _.template(player.localize('Episode <%=episode%> starts in '))({episode: episode})
        _nextEpisodeTitle.appendChild(_nextEpisodeTimer)
        _divEpisodeHeader.appendChild(_nextEpisodeTitle)
        
        return _divEpisodeHeader 
      }
      
      function createRelatedScreen(data) {
        if (options.contentType == 'Serie') {
          return createNextEpisode(data)
        }
        return createRelatedMovies(data) 
      }
      
      function createNextEpisode(data) {
        const episode = data.detail.Content.episode
        content = data.recomm 
        const _divEpHeader = createEpisodeHeader(episode, content)
        const _divNextEpisode = Utils.createEl('div', 'next-episode', 'epToPlay')
        const _divSeriePoster = Utils.createEl('div', 'serie-poster-container', 'episodePoster')
        const _img = Utils.createEl('img', 'serie-poster-img outline-active', 'posterImage')
        _img.src = data.detail.Content.metadata.img_large
        _img.addEventListener('click', () => playContent(options, content, timerId))
        
        _divNextEpisode.appendChild(_divEpHeader)
        _divSeriePoster.appendChild(_img)
        
        _divNextEpisode.appendChild(_divSeriePoster)
        
        return _divNextEpisode
      }
      
      function createRelatedMovies(data) {
        const sugs = data.recomm
        let  i = sugs.length - 1 > 1 ? 1 : sugs.length - 1
        
        const _divRelated = Utils.createEl('div', 'related-gallery', 'relatedMovies')
        const _divDetailsLeft = Utils.createEl('div', 'related-media-details related-margin-right', 'detailsLeft')
        const _h1TitleLeft = Utils.createEl('h1')
        _h1TitleLeft.innerHTML = sugs[1].title
        _divDetailsLeft.appendChild(_h1TitleLeft)
        // plot detail text
        const _h3PlotLeft = Utils.createEl('h3')
        _h3PlotLeft.innerHTML = sugs[1].short_summary
        _divDetailsLeft.appendChild(_h3PlotLeft)
        _divDetailsLeft.style.visibility = 'hidden' 
        // row with action buttons
        const _rowActionsLeft = Utils.createEl('div', 'related-row')
        let _addToListLeft = Utils.createEl('a', 'btn related-round-button inactive')
        // action play button 
        const _playButtonLeft = Utils.createEl('a', 'btn related-play-button')
        _playButtonLeft.addEventListener('click', () => playContent(options, sugs[1].uuid))
        _playButtonLeft.target = '_self'
        const _playIcon = Utils.createEl('i')
        _playIcon.innerHTML = player.localize('View')
        // action favorite button
        let _favIconLeft = Utils.createEl('i', 'not-fa', 'iconLeft')
        if ( sugs[1].favorite) {
          _favIconLeft.className = 'yes-fa'
          _addToListLeft.classList.remove('inactive')
          _addToListLeft.classList.add('active')
        }
        _addToListLeft.addEventListener('click', function() {
          // change icon and color
          _favIconLeft = toggleClassFav(_favIconLeft)
          _addToListLeft = toggleColorFavorite(_addToListLeft)
          // post to change favorite value 
          postFavorite(sugs[1], options)
        }, false)
        
        _playButtonLeft.appendChild(_playIcon)
        _addToListLeft.appendChild(_favIconLeft)
        _rowActionsLeft.appendChild(_playButtonLeft)
        _rowActionsLeft.appendChild(_addToListLeft)
        
        _divDetailsLeft.appendChild(_rowActionsLeft)
        _divRelated.appendChild(_divDetailsLeft)

        // construct the individual suggested content pieces
        for (; i >= 0; --i) {
          const _divPoster = Utils.createEl('div', 'related-poster-container', 'poster'+i)

          const _img = Utils.createEl('img', 'related-poster-img', 'posterImage'+i)

          _img.src = sugs[i].metadata.img_large
          _img.style.outline = 'unset'
          
          _divPoster.appendChild(_img)
          _divRelated.appendChild(_divPoster)

        }
       
        const _divDetailsRight = Utils.createEl('div', 'related-media-details related-margin-left', 'detailsRight')
        const _h1TitleRight = Utils.createEl('h1')
        _h1TitleRight.innerHTML = sugs[0].title

        _divDetailsRight.appendChild(_h1TitleRight)
        
        const _h3PlotRight = Utils.createEl('h3')
        _h3PlotRight.innerHTML = sugs[0].short_summary
        _divDetailsRight.appendChild(_h3PlotRight)
        _divDetailsRight.style.visibility = 'hidden' 
        
        //create row with action buttons
        const _rowActionsRight = Utils.createEl('div', 'related-row')
        let _addToListRight = Utils.createEl('a', 'btn related-round-button inactive')
        const _playButtonRight = Utils.createEl('a', 'btn related-play-button')
        _playButtonRight.addEventListener('click', () => playContent(options, sugs[0].uuid))
        _playButtonRight.target = '_self'
        const _playIconRight = Utils.createEl('i')
        _playIconRight.innerHTML = player.localize('View')
        let _favIconRight = Utils.createEl('i', 'not-fa', 'iconRight')
        
        if ( sugs[0].favorite) {
          _favIconRight.className = 'yes-fa'
          _addToListRight.classList.remove('inactive')
          _addToListRight.classList.add('active')
        }
        
        _addToListRight.addEventListener('click', function() {
          // change icon and color
          _favIconRight = toggleClassFav(_favIconRight)
          _addToListRight = toggleColorFavorite(_addToListRight)
          // post to change favorite value 
          postFavorite(sugs[0], options)
        }, false)
        
        _playButtonRight.appendChild(_playIconRight)
        _addToListRight.appendChild(_favIconRight)
        _rowActionsRight.appendChild(_playButtonRight)
        _rowActionsRight.appendChild(_addToListRight)
        
        _divDetailsRight.appendChild(_rowActionsRight)
        _divRelated.appendChild(_divDetailsRight)

        
        return _divRelated
      }     
      
      function toggleColorFavorite(el) {
        if ( el.classList.contains('inactive')) {
          el.classList.remove('inactive')
          el.classList.add('active')
          return el
        }
        el.classList.remove('active')
        el.classList.add('inactive')
        return el
      }
      
      function showRelated() {
        const relatedAside = Utils.getElByClass('related-screen')[0]
        Utils.show(relatedAside, 'nodisplay')
      }
      
      function hideRelated() {
        const relatedAside = Utils.getElByClass('related-screen')[0]
        Utils.hide(relatedAside, 'nodisplay')
      }
      
      function askForEndCredits() {
        const currentTime = QB.video.currentTime
        if (creditsBegin == 0) {
          creditsBegin = QB.video.duration - 20
        }
        
        // content is serie and credits begin
        if ( currentTime >= creditsBegin && options.contentType == 'Serie') {
          return createScreenRelated(options)
        }
        //content is Pelicula and credits Begin
        if (currentTime >= creditsBegin && options.contentType != 'Serie') {
          return showRelated()
        }
        // credits is not Began  
        return creditsBeginLoop()
      }
      
      function toggleClassFav(el) {
        if (el.className == 'yes-fa') {
          el.className = 'not-fa'
          return el
        }
        el.className = 'yes-fa'
        return el
      }
      
      function postFavorite(related, options) {
        return new Promise( (resolve, reject) => {
          const data = {
            operator: options.operator,
            uuid: related.uuid
          }

          const headers = {
            Authorization: 'Bearer ' + Auth.getToken()
          }
          
          const url = options.api + '/favorite'
          Utils.post( url, headers, data)
            .then(JSON.parse)
            .then((data) => {
              if (!data) {
                return reject()
              }
              return resolve(data)
            })
            .catch(reject)
         
        })
      } 
      // Show related on pause
      /*player.on('pause', function() {
        if (options.contentType != 'Serie') {
          showRelated()
        }
      })

      player.on('play', function() {
        if (options.contentType != 'Serie') {
          hideRelated()
        }
      })*/
    }

    function playContent(options, uuid, timerId) {
      if (options.contentType == 'Serie') {
        clearInterval(timerId)           
      }
      const headers = {
        Authorization: 'Bearer ' + Auth.getToken()
      }

      const data = {
        operator: options.operator,
        quality: options.quality,
        language: options.language,
        stream: options.stream,
        username: options.user,
        uuid: uuid
      }

      const url = options.api + '/content'
      showLoadingScreen()
      Utils.post(url, headers, data)
        .then(JSON.parse)
        .then(data => {
          if (data.embedUrl) {
            window.location.href = data.embedUrl
            window.location.reload(true)
          } else {
            //TODO: improve error handling
            Utils.error('no embed url on /content response')
            hideLoadingScreen()
          }
        })
        .catch(err => {
          //TODO: improve error handling
          Utils.error(err)
          hideLoadingScreen()
        })
    }

    function showLoadingScreen() {
      const loading = Utils.createEl('div', 'loading')

      const overlay = Utils.createEl('div', 'related-overlay', 'related_loading')
      overlay.appendChild(loading)

      const body = document.querySelector('body')
      body.appendChild(overlay)
    }

    function hideLoadingScreen() {
      const element = document.getElementById('related_loading')
      if (element) {
        const body = document.querySelector('body')
        body.removeChild(element)
      }
    }

    registerPlugin('suggestedVideoEndcap', suggestedVideoEndcap)
    QB.vjs.suggestedVideoEndcap(QB.options)
  }
}
