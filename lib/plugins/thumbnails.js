import Utils from '../utils'

export default {
  initialize: function (QB) {
    const Component = videojs.getComponent('Component')
      , GRID_CELLS_WIDTH = 10
      , GRID_CELLS_HEIGHT = 10
      , GRID_WIDTH = 2400           // pixels
      , GRID_HEIGHT = 1360          // pixels
      , TILE_WIDTH = GRID_WIDTH / GRID_CELLS_WIDTH
      , TILE_HEIGHT = GRID_HEIGHT / GRID_CELLS_HEIGHT
      , THUMBNAIL_INTERVAL = 240 / 101    // 101 images every 4 minutes (240 seconds)

    function toGridPoint(i) {
      return {
        x: i % GRID_CELLS_WIDTH,
        y: Math.floor(i / GRID_CELLS_WIDTH)
      }
    }

    function setImagePosition(image, index) {
      const point = toGridPoint(index)
        , top = TILE_HEIGHT * point.y
        , left = TILE_WIDTH * point.x

      image.style.top = '-' + (top + TILE_HEIGHT) + 'px'  // the "+ TILE_HEIGHT" if because of the loading image
      image.style.left = '-' + left + 'px'
    }

    function getId(url) {
      return 'id' + url.split('').filter(c => _.includes('0123456789', c)).join('')
    }

    const ThumbnailDisplay = videojs.extend(Component, {
      constructor: function (player, options) {
        Component.call(this, player, options)

        this.standalone = options.standalone
        this.urlTemplate = options.url
        this.index = 1

        if (this.standalone) {
          this.addClass('vjs-thumbnail-holder-standalone')
        }

        player.ready(() => this.on(player.controlBar.progressControl.el(), 'mousemove', _.throttle(this.update.bind(this), 25)))

        this.loading = document.createElement('div')
        this.loading.className = 'vjs-thumbnail-loading'
        this.el().appendChild(this.loading)
      },

      createEl: function () {
        return Component.prototype.createEl.call(this, 'div', {
          className: 'vjs-thumbnail-holder'
        })
      },

      url: function (index) {
        const imageIndex = 1 + Math.floor(index / (GRID_CELLS_WIDTH * GRID_CELLS_HEIGHT))
        return this.urlTemplate.replace('%s', ('000' + imageIndex).slice(-3))
      },

      update: function (event) {
        const player = this.player()
          , duration = player.duration()
          , thumbnailCount = duration / THUMBNAIL_INTERVAL

        if (!event || thumbnailCount == 0) {
          return
        }

        if (this.standalone && event) {
          const position = event.pageX - Utils.findElementPosition(this.el().parentNode).left
            , offset = this.el().getBoundingClientRect().width / 2
          this.el().style.left = (position - offset) + 'px'
        }

        const seekBar = _.get(player, 'controlBar.progressControl.seekBar')
          , mouseTime = seekBar.calculateDistance(event) * duration

        this.index = Math.floor(mouseTime / THUMBNAIL_INTERVAL)

        if (this.index != this.lastIndex) {
          this.setCurrentThumbnail(this.index)
          this.lastIndex = this.index
        }
      },

      setCurrentThumbnail: function (index) {
        const url = this.url(index)
          , id = getId(url)
          , image = this.getImage(this.el(), id)
          , isLoading = (image.getAttribute('data-loading') === 'true')

        if (this.currentThumbnailId) {
          document.getElementById(this.currentThumbnailId).style.display = 'none'
        }

        this.currentThumbnailId = image.id

        if (image.src != url) {
          image.src = url
        }

        if (isLoading) {
          image.style.display = 'none'
          this.loading.style.display = 'block'
        } else {
          image.style.display = 'block'
          this.loading.style.display = 'none'
          setImagePosition(image, index % (GRID_CELLS_WIDTH * GRID_CELLS_HEIGHT))
        }
      },

      getImage: function (element, id) {
        let image = document.getElementById(id)

        if (!image) {
          image = document.createElement('img')
          image.id = id
          image.className = 'vjs-thumbnail'
          image.setAttribute('data-loading', true)
          image.style.display = 'none'
          image.onload = this.imageLoaded.bind(this, image)
          element.appendChild(image)
        }

        return image
      },

      imageLoaded: function (image) {
        image.setAttribute('data-loading', false)
        image.onload = null
        if (this.currentThumbnailId == image.id) {
          image.style.display = 'block'
          this.loading.style.display = 'none'
        }
      }
    })

    Component.registerComponent('ThumbnailDisplay', ThumbnailDisplay)

    videojs.plugin('thumbnails', function (options) {
      let parent = this.controlBar.progressControl.seekBar
        , hasMouseTimeDisplay = _.get(options.vjs, 'controlBar.progressControl.seekBar.mouseTimeDisplay')

      if (!_.isBoolean(hasMouseTimeDisplay)) {
        hasMouseTimeDisplay = true    // videojs has mouse display by default
      }

      if (hasMouseTimeDisplay) {
        parent = parent.mouseTimeDisplay
      }

      parent.addChild('ThumbnailDisplay', {
        standalone: !hasMouseTimeDisplay,
        url: options.thumbs
      })
    })

    QB.vjs.thumbnails(QB.options)
  }
}
