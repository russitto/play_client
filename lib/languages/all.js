import es from './es'

const languages = {
  'es': es
  // add more languages here
}

export default {
  load: function () {
    Object.keys(languages)
      .forEach(language => videojs.addLanguage(language, languages[language]))
  }
}
