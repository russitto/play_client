/**
 * Dependencies, extra scripts to load to make the player work.
 *
 * @module lib/dependencies
 */

import Utils from './utils'
import Config from './config'

const scriptSuffix = (Config.debug ? '.js' : '.min.js')

const dependencies = [
  { /* promises */
    url: '//cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.0/es6-promise' + scriptSuffix,
    replace: window.Promise
  },

  { /* lzstring */
    url: '//cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string' + scriptSuffix,
    replace: window.LZString
  },

  { /* videojs */
    url: '//vjs.zencdn.net/5.20.1/video.min.js',   // video.js doesn't work, but video.min.js does
    replace: window.videojs
  },

  { /* lodash */
    url: '//cdn.jsdelivr.net/lodash/4.17.4/lodash' + scriptSuffix,
    replace: window._
  },

  { /* Object.assign (for IE11) */
    url: '//cdn.jsdelivr.net/npm/object-assign-polyfill@0.1.0/index' + scriptSuffix,
    replace: Object.assign
  }
]

/**
 * Load every dependency needed by the player
 *
 * @param {Function} done - A callback to be called when every dependency was loaded
 */
function load(done) {
  const head = document.querySelector('head')
  let remaining = dependencies.length

  function loaded(url) {
    remaining -= 1
    Utils.log('loaded', url)
    if (remaining == 0) {
      done()
    }
  }

  function error(url) {
    throw new Error('missing dependency: ' + url)
  }

  function load(dependency) {
    const script = document.createElement('script')

    function clean() {
      script.removeEventListener('load', onLoad)
      script.removeEventListener('error', onError)
    }

    function onLoad() {
      clean()
      loaded(dependency.url)
    }

    function onError() {
      clean()
      error(dependency.url)
    }

    if (typeof dependency.replace != 'undefined') {
      // dependency already loaded
      loaded(dependency.url)
    } else {
      script.addEventListener('load', onLoad)
      script.addEventListener('error', onError)

      script.src = dependency.url
      head.appendChild(script)
    }
  }

  for (let i = 0, length = dependencies.length; i < length; ++i) {
    load(dependencies[i])
  }
}

export default {
  load: load
}
