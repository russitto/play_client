import QB from '../lib/qb'
import HlsPlayer from './player'

function onDocumentLoad() {
  QB.initialize(HlsPlayer)
}

if (document.readyState === 'loaded') {
  onDocumentLoad()
} else {
  document.addEventListener('DOMContentLoaded', onDocumentLoad)
}
