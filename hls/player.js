/**
 * It uses {@link https://github.com/video-dev/hls.js|hls.js}. And adds <code>QB.hls</code> property.
 *
 * @namespace HlsPlayer
 * @implements {QubitPlayer}
 */

import Config from '../lib/config'
import Options from '../lib/options'
import Utils from '../lib/utils'
import Multilingual from '../lib/multilingual'

const CANT_PLAY_MEDIA = '__user_cant_play_media__'

function postForm(url, form) {
  return new Promise(function (resolve, reject) {
    const xhr = new XMLHttpRequest()

    function clean() {
      xhr.removeEventListener('error', onError)
      xhr.removeEventListener('abort', onError)
      xhr.removeEventListener('load', onLoad)
    }

    function onError(err) {
      Utils.log('HlsPlayer.postForm() ERROR:', err)
      clean()
      reject(new Error(err))
    }

    function onLoad() {
      Utils.log('HlsPlayer.postForm() Loaded', xhr)
      clean()

      const json = JSON.parse(xhr.responseText)

      /**
       * We need to check for exactly the value true, not a truthy value.
       * Because json.Data sometimes is {} or '', which are not true.
       *
       * Also, it appears that using status codes are overrated.
       */
      if (json.Success === true && json.Data === true) {
        resolve()
      } else {
        reject(new Error(CANT_PLAY_MEDIA))
      }
    }

    xhr.addEventListener('error', onError)
    xhr.addEventListener('abort', onError)
    xhr.addEventListener('load', onLoad)
    xhr.open('POST', url)
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    xhr.send(Utils.querystring(form))
  })
}

function authorize(options) {
  return postForm(Config.qubitClsService, {
    userid: options.userId,
    uuid: options.uuid
  })
}

let options = null

function onManifestParsedEvent(hls, options) {
  Utils.log('HlsPlayer: Hls.Events.MANIFEST_PARSED, time =', options.time)
  return authorize(options)
    .then(() => hls.startLoad(options.time || -1))
    .catch(error => {
      Utils.error(`Authorization Error: ${error}`)
      if (error.message === CANT_PLAY_MEDIA) {
        const url = options.link
        if (url == 'javascript:history.back()') {
          window.history.back()
        } else {
          const separator = url.indexOf('?') >= 0 ? '&' : '?'
          window.location = url + separator + 'status=PLAY_DENIED'
        }
      }
    })
}

function onErrorEvent(hls, event, error) {
  Utils.log('HlsPlayer: Hls.Events.ERROR:', error)

  return new Promise((resolve, reject) => {
    if (!error.fatal) {
      return reject(error)
    }

    switch (error.type) {
    case window.Hls.ErrorTypes.NETWORK_ERROR:
      Utils.log('Fatal network error, trying to recover')
      hls.startLoad()
      resolve()
      break
    case window.Hls.ErrorTypes.MEDIA_ERROR:
      Utils.log('Fatal media error, trying to recover')
      hls.recoverMediaError()
      resolve()
      break
    default:
      Utils.log('Can\'t recover from error')
      hls.destroy()
      reject()
    }
  })
}

function setImplementation(implementation) {
  Utils.log('HlsPlayer.setImplementation()')
  HlsPlayer.implementation = implementation
}

function initialize(QB) {
  Utils.log('HlsPlayer.initialize()')
  options = QB.options

  HlsPlayer.Audio = createAudio()
  HlsPlayer.Subtitles = createSubtitles()

  return new Promise((resolve, reject) => {
    return Utils.loadScript(Config.HLS.lib)
      .then(function () {
        if (!window.Hls) {
          throw new Error(`Can't use hls if window.Hls is not available`)
        }

        setImplementation(new window.Hls({autoStartLoad: false}))

        const hls = HlsPlayer.implementation

        hls.on(window.Hls.Events.MANIFEST_PARSED,
          () => onManifestParsedEvent(hls, options))

        hls.on(window.Hls.Events.ERROR,
          (event, error) => onErrorEvent(hls, event, error).catch(reject))

        hls.on(window.Hls.Events.MEDIA_ATTACHED,
          () => resolve())

        hls.attachMedia(QB.video)
      })
  })
}

function initializeYoubora(QB) {
  return Utils.loadScript(Config.HLS.youbora)
    .then(() => {
      const hls = HlsPlayer.implementation
      const youbora = new $YB.plugins.Hlsjs(hls, QB.options.youbora)

      hls.on(window.Hls.Events.ERROR, (event, error) => {
        if (event.fatal) {
          youbora.errorHandler(error.details)
        }
      })

      youbora.playHandler && youbora.playHandler()
      youbora.playerHandler && youbora.playerHandler()
    })
    .catch(Utils.error)
}

function createAudio() {
  let currentAudioLanguage = null
  const AudioBase = Multilingual('audio')
  return Object.create(AudioBase, {
    languages: {value: function () {
      Utils.log('HlsPlayer.Audio.languages()')
      return options.source.map(function (source) {
        return source.lang
      })
    }},

    selectLanguage: {value: function (language) {
      Utils.log('HlsPlayer.Audio.selectLanguage(', language, ')')
      if (currentAudioLanguage == language) {
        return
      }
      currentAudioLanguage = language

      const media = HlsPlayer.implementation.media
      if (media.currentTime) {
        options.time = media.currentTime
      }

      const source = Options.getSourceUrl(options, language)
      Utils.log('HlsPlayer.Audio.selectLanguage() source:', source)

      const hls = HlsPlayer.implementation
      hls.detachMedia()
      hls.loadSource(source)
      hls.attachMedia(media)

      AudioBase.selectLanguage.call(this, language)
    }}
  })
}

function createSubtitles() {
  const SubtitlesBase = Multilingual('subtitles')
  return Object.create(SubtitlesBase, {
    languages: {value: function () {
      Utils.log('HlsPlayer.Subtitles.languages()')
      return [SubtitlesBase.OFF].concat(options.subtitles.map(function (subtitle) {
        return subtitle.lang
      }))
    }}
  })
}

import Player from '../lib/player'

const HlsPlayer = Player({
  name: 'hls_',         /* needed for Youbora */
  implementation: null, /* needed for Youbora & Chromecast */
  setImplementation: setImplementation,
  enableChromecast: () => false,
  initialize: initialize,
  initializeYoubora: initializeYoubora,
  load: function () {},
  Audio: null,
  Subtitles: null
})

export default HlsPlayer
